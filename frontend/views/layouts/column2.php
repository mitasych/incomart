<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
	<div class="span-5 last" style="float:none;">
		<div id="sidebar">
		<?php
			$this->beginWidget('zii.widgets.CPortlet', array(
// 				'title'=>'Operations',
				'title'=>false,
			));
// 			$this->widget('zii.widgets.CMenu', array(
// 				'items'=>$this->menu,
// 				'htmlOptions'=>array('class'=>'operations'),
// 			));
			$this->widget('bootstrap.widgets.TbButtonGroup', array(
					'buttons'=>$this->menu,
					/* 'htmlOptions' => array('class'=>'btn-group-vertical'), */
			));
			$this->endWidget();
			
		?>
		
		
		</div><!-- sidebar -->
	</div>

	<div class="span-19">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
</div>
<?php $this->endContent(); ?>