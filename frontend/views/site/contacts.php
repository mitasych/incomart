<?php
$this->breadcrumbs=array(
	'Contacts'
);
?>

<?php if(Yii::app()->user->hasFlash('success')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
<?php endif; ?>

<h1>Contacts</h1>


<span style="font-size: small;">mitasych@gmail.com<br><br>тел. +38 (050) 4932803<br></span>

<?php if(1):?>
<h2>You can send us a request</h2>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'contacts-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>45)); ?>
	
	<?php echo $form->textFieldRow($model, 'subject', array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>500)); ?>

	<?php echo $form->textAreaRow($model,'content',array('class'=>'span5', 'rows'=>5)); ?>
	
	<?php echo $form->checkBoxRow($model, 'copyMe'); ?>
	
	<?php if ($model->requireCaptcha): ?>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo CHtml::activeTextField($model, 'verifyCode'); ?>
	<?php endif; ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Send',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<?php endif; ?>