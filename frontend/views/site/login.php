<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = 'Login';
$this->breadcrumbs = array(
	'Login',
);
?>

<?php if(Yii::app()->user->hasFlash('success')):
        echo Yii::app()->user->getFlash('success'); 
endif; ?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'login-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
)); ?>
	<?php echo $form->label($model, 'username');?>
	<?php echo $form->textField($model, 'username'); ?>
	<?php echo $form->error($model, 'username');?>
	<br/>
	<?php echo $form->label($model, 'password');?>
	<?php echo $form->passwordField($model, 'password'); ?>
	<?php echo $form->label($model, 'password');?>
	<?php echo CHtml::link(Yii::t('main','Reset Password'), array('/user/resetpassword'),array()) ?> <br/><br/>
	
	<?php echo $form->checkBox($model, 'rememberMe',array('class' => 'fleft','style'=>'margin-right:10px;')); ?>
	<?php echo $form->label($model, 'Remember Me',array('class' => 'fleft'));?>
	<div class="clear"></div>
	
	<?php if ($model->requireCaptcha): ?>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo CHtml::activeTextField($model, 'verifyCode'); ?>
	<?php endif; ?>
	<div class="actions">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'top-sidebar')); ?>
	
	<div style="height: 10px"></div>
<?php $this->endWidget();?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'sidebar-two')); ?>
	
	
<?php $this->endWidget();?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'calendar-sidebar')); ?>
	
<?php $this->endWidget();?>