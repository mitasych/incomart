<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = 'Login';
$this->breadcrumbs = array(
	'Login',
);
?>
<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'login-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
)); ?>
	<?php echo $form->hiddenField($model, 'role');?>

	<?php echo $form->label($model, 'username');?>
	<?php echo $form->textField($model, 'username'); ?>
	<?php echo $form->error($model, 'username');?>
	<br/>
	<?php echo $form->label($model, 'password');?>
	<?php echo $form->passwordField($model, 'password'); ?>
	<?php echo $form->error($model, 'password');?>
	<br/>
	<?php echo $form->label($model, 'password_repeat');?>
	<?php echo $form->passwordField($model, 'password_repeat'); ?>
	<?php echo $form->error($model, 'password_repeat');?>
	<br/>
	<?php echo $form->label($model, 'email');?>
	<?php echo $form->textField($model, 'email'); ?>
	<?php echo $form->error($model, 'email');?>
	<br/>
	
	<?php if ($model->requireCaptcha): ?>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo CHtml::activeTextField($model, 'verifyCode'); ?>
	<?php endif; ?>
	<div class="actions">
		<?php echo CHtml::submitButton('Signup'); ?>
	</div>

<?php $this->endWidget(); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$tariff,
	'attributes'=>array(
		'name',
		array(
				'name'=>'cost_camera_per_month',
				'value'=>isset($tariff->cost_camera_per_month) ? $model->cost_camera_per_month.' tg/month per camera' : '0 tg/month per camera',
				),
		//'activkey',
		'number_cameras',
		'another_users_access',
		'local_archive',
		'clode_archive',
	),
)); ?>