<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'camera-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>256)); ?>

	<?php // echo $form->textFieldRow($model,'manufacturer_id',array('class'=>'span5')); ?>
	<?php 
// 		echo $form->dropDownListRow(
// 				$model, 
// 				'manufacturer_id', 
// 				CamerasManufacturer::listItems(), 
// 				array(
// 						'empty'=>'Select',
// 						'ajax'=>
// 						array(
// 								'type'=>'POST', 
// 								'url'=>CController::createUrl('camera/loadmodels'), 
// 								'update'=>'#'.CHtml::activeId($model,'model_id'),
// 						)
// 				)
// 			); 
	?>
	
	<?php 
// 		echo $form->dropDownListRow(
// 				$model,
// 				'model_id', 
// 				array(),
// 				array(
// 						'empty'=>'Select',
// 						'ajax'=>
// 						array(
// 								'type'=>'POST', 
// 								'url'=>CController::createUrl('camera/loadCameraUrl'), 
// 								'success'=>'js:function(data)
// 									{
// 										$("#'.CHtml::activeId($model,'url').'").val(data);
// 									}'
// 						)
// 				)
// 			); 
		?>

	<?php // echo $form->textFieldRow($model,'model_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'IP_address',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'login',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'port',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'url',array('class'=>'span5','maxlength'=>256)); ?>

	
	
	<?php // echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
