<?php
$this->breadcrumbs=array(
	'Cameras',
);

$this->menu=array(
	array('label'=>'Create Camera','url'=>array('create')),
	array('label'=>'Manage Camera','url'=>array('admin')),
);
?>

<h1>Cameras</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
