<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = 'Reset Password';
$this->breadcrumbs = array(
	'Reset Password',
);
?>
<h1>Reset Password</h1>

<p>Please enter your username or e-mail address:</p>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'reset-pass-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
)); ?>

	<?php echo $form->label($model, 'username');?>
	<?php echo $form->textField($model, 'username'); ?>
	<?php echo $form->error($model, 'username');?>
	<br/>
	<?php $this->widget('CCaptcha'); ?>
	<?php echo CHtml::activeTextField($model, 'verifyCode'); ?>
	<div class="actions">
		<?php echo CHtml::submitButton('Reset Password'); ?>
	</div>

<?php $this->endWidget(); ?>