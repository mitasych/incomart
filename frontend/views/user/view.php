<?php
/* @var $this UserController */
/* @var $model User */
if(Yii::app()->user->hasFlash('success')){
	echo Yii::app()->user->getFlash('success');
}

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php $avatar = empty($model->profile->photo) ? 'no-avatar.jpg' : $model->profile->photo; ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
// 		'id',
		'username',
		array(
				'label'=>'First Name',
				'type'=>'raw',
				'value'=>$model->profile->first_name,
			),
		array(
				'label'=>'Last Name',
				'type'=>'raw',
				'value'=>$model->profile->last_name,
			),
		array(
				'label'=>'Photo',
				'type'=>'image',
				'value'=>'/image/uploaded/avatarthumb/'.$avatar,
			),
		array(
				'label'=>'City',
				'type'=>'raw',
				'value'=>$model->profile->city,
			),
		array(
				'label'=>'ICQ',
				'type'=>'raw',
				'value'=>$model->profile->icq ? $model->profile->icq : '',
			),
		array(
				'label'=>'Skype',
				'type'=>'raw',
				'value'=>$model->profile->skype,
			),
		array(
				'label'=>'About me',
				'type'=>'raw',
				'value'=>$model->profile->about,
			),
// 		'password',
		'email',
		/* 'activkey',
		'create_time',
		'lastvisit_at', */
// 		'superuser',
		'status',
// 		'role_id',
		'login_attempts',
	),
)); ?>
