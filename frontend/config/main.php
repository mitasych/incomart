<?php
/**
 * main.php
 *
 * This file holds frontend configuration settings.
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 5:48 PM
 */
$frontendConfigDir = dirname(__FILE__);

$root = $frontendConfigDir . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..';

$params = require_once($frontendConfigDir . DIRECTORY_SEPARATOR . 'params.php');

// Setup some default path aliases. These alias may vary from projects.
Yii::setPathOfAlias('root', $root);
Yii::setPathOfAlias('common', $root . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('frontend', $root . DIRECTORY_SEPARATOR . 'frontend');
Yii::setPathOfAlias('www', $root. DIRECTORY_SEPARATOR . 'frontend' . DIRECTORY_SEPARATOR . 'www');

$mainLocalFile = $frontendConfigDir . DIRECTORY_SEPARATOR . 'main-local.php';
$mainLocalConfiguration = file_exists($mainLocalFile)? require($mainLocalFile): array();

$mainEnvFile = $frontendConfigDir . DIRECTORY_SEPARATOR . 'main-env.php';
$mainEnvConfiguration = file_exists($mainEnvFile) ? require($mainEnvFile) : array();

return CMap::mergeArray(
	array(
		'name' => 'Видеосервис',
		// @see http://www.yiiframework.com/doc/api/1.1/CApplication#basePath-detail
		'basePath' => 'frontend',
		//'homeUrl' => 'http://new-details.dev/',
		// set parameters
		'params' => array_merge($params, array('type_app'=>'front')),
		// preload components required before running applications
		// @see http://www.yiiframework.com/doc/api/1.1/CModule#preload-detail
		'preload' => array('bootstrap','log'),
		// @see http://www.yiiframework.com/doc/api/1.1/CApplication#language-detail
		'sourceLanguage' => 'en',
		'language' => 'ru',
		// setup format 
// 		'format' => array(
// 				'dateFormat' => 'd MMMM Y',
// 				'timeFormat' => 'H:i',
// 				'datetimeFormat' => ' H:i d.m.yy',
// 				'numberFormat' => array('decimals' => null, 'decimalSeparator' => '.', 'thousandSeparator' => ' '),
// 				'booleanFormat' => array('Нет','Да')
// 		),
		// uncomment if a theme is used
 		'theme' => 'incom',
		// setup import paths aliases
		// @see http://www.yiiframework.com/doc/api/1.1/YiiBase#import-detail
		'import' => array(
			'common.components.*',
			'common.extensions.*',
			'common.extensions.epayKKB.*',
			'common.models.*',
			'common.lib.PHPSecLib.Net.*',
			'common.lib.PHPSecLib.Math.*',
			'common.lib.PHPSecLib.Crypt.*',
			// uncomment if behaviors are required
			// you can also import a specific one
			/* 'common.extensions.behaviors.*', */
			// uncomment if validators on common folder are required
			/* 'common.extensions.validators.*', */
			'application.components.*',
			'application.controllers.*',
			'application.models.*'
		),
		/* uncomment and set if required */
		// @see http://www.yiiframework.com/doc/api/1.1/CModule#setModules-detail
		'modules' => array(
			'gii' => array(
				'class' => 'system.gii.GiiModule',
				'password' => '333',
				'ipFilters'=>array('*'),
				'generatorPaths' => array(
					'bootstrap.gii'
				)
			)
		),
		'components' => array(
			'session' => array(
	            'class' => 'CDbHttpSession',
	            'timeout' => 1800, 
	            'connectionID'=>'db',
	            'autoCreateSessionTable' => false,
	            'sessionTableName' =>'user_session' ,
		        //'savePath' => $root.DIRECTORY_SEPARATOR.'sessions',
				'gCProbability'=>100,
	        ),
			'user' => array(
					'allowAutoLogin'=>true,
					'class'=>'WebUser',
			),
			'bootstrap' => array(
				'class' => 'common.extensions.bootstrap.components.Bootstrap',
				'responsiveCss' => true,
			),
			'errorHandler' => array(
				// @see http://www.yiiframework.com/doc/api/1.1/CErrorHandler#errorAction-detail
				'errorAction'=>'site/error'
			),
			'db' => array(
				'connectionString' => $params['db.connectionString'],
				'username' => $params['db.username'],
				'password' => $params['db.password'],
				'schemaCachingDuration' => YII_DEBUG ? 0 : 86400000, // 1000 days
				'enableParamLogging' => YII_DEBUG,
				'charset' => 'utf8'
			),
			'urlManager' => array(
				'urlFormat' => 'path',
				'showScriptName' => false,
// 				'urlSuffix' => '.html',
				'rules' => $params['url.rules']
			),
			'authManager'=>array(
					'class'=>'CDbAuthManager',
					'connectionID'=>'db',
					'defaultRoles' => array('guest'),
			),
			'image' => $params['image'],
			'mail' => array(
					'class' => 'YiiMail',
// 					'transportType' => 'smtp',
					'transportType' => 'php',
// 					'transportOptions' => array(
// 							'host'=>'smtp.gmail.com',
// 							'username'=>'*****@*****',
// 							'password'=>'****',
// 							'port'=>'465',
// 							'encryption'=>'ssl'
// 					),
					'viewPath' => 'application.views.mail',
					'logging' => false,
					'dryRun' => false
			),
			'log' => array(
					'class' => 'CLogRouter',
					'routes' => array(
							'db' => array(
									'class' => 'CWebLogRoute',
									'categories' => 'system.db.CDbCommand',
									'showInFireBug' => true //Показывать в FireBug или внизу каждой страницы
							)
					),
			),
			/* make sure you have your cache set correctly before uncommenting */
			/* 'cache' => $params['cache.core'], */
			/* 'contentCache' => $params['cache.content'] */
		),
	),
	CMap::mergeArray($mainEnvConfiguration, $mainLocalConfiguration)
);