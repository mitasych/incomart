<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/columnCabinet1';

	/**
	 * @return array action filters
	 */
	// 	public function filters()
	// 	{
	// 		return array(
	// 			'rights',
	// // 			'accessControl', // perform access control for CRUD operations
	// 		);
	// 	}

	public function actions() {
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha' => array(
						'class' => 'CCaptchaAction',
						'testLimit' => 1,
						'backColor' => 0xFFFFFF,
				),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array('allow',  // allow all users to perform 'index' and 'view' actions
						'actions'=>array('index','view'),
						'users'=>array('*'),
				),
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>array('create','update'),
						'users'=>array('@'),
				),
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('admin','delete'),
						'users'=>array('admin'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->render('view',array(
				'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = UserProfile::model()->findByPk($id);

		$old_photo = $model->photo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserProfile']))
		{
			$model->attributes=$_POST['UserProfile'];
			if (!empty($_FILES['UserProfile']['name']['photo'])) {

				$imageName = uniqid(substr(md5($_FILES['UserProfile']['name']['photo']),0,8));
					
				$imageOb = CUploadedFile::getInstance($model,'photo');
					
				$model->photo = $imageName.'.'.$imageOb->extensionName;
					
				// 				if($model->save()){
				$imageOb->saveAs(Yii::app()->params['imgUploadOrigin'].$model->photo);

				$image = Yii::app()->image->load(Yii::app()->params['imgUploadOrigin'].$model->photo);

				$wm = NULL;
					
				// 					// 				if($_POST['ImagePost']['watermark'] == 1){
				// 					// 					$wm = 1;
				// 					// 				}

				$this->imageResize($image, $model->photo, $wm);

				// 					// 				$this->redirect(array('view','id'=>$model->id));
				// 					$this->redirect(Yii::app()->request->getUrlReferrer());
				// 				}
			}
			elseif (!empty($old_photo)){
				$model->photo = $old_photo;
			}
			if($model->save()){
				$this->redirect(array('view','id'=>$model->user_id));
			}
		}

		$this->render('update',array(
				'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function imageResize($image, $name, $wm = NULL){

		$imageSizes = Yii::app()->params['imgSize'];

		//$img_watermark = Yii::app()->image->load(Yii::app()->params['imgUploadLarge'].'watermark_nikcenter.png');

		if ($image->width >= $image->height) {
			$image->resize($imageSizes['originWidth'], $imageSizes['originHeight']);
		}
		else{
			$image->resize($imageSizes['originHeight'], $imageSizes['originWidth']);
		}

		if (isset($wm)){
			$image->watermark($img_watermark, 20, ($image->height - 40), 100);
		}
		$image->save();
		$image->resize($imageSizes['mediumWidth'], $imageSizes['mediumHeight']);
		$image->save(Yii::app()->params['avatarUpload'].$name);
		$image->resize($imageSizes['thumbWidth'], $imageSizes['thumbHeight'], Image::AUTO);
		$image->save(Yii::app()->params['avatarUploadThumb'].$name);

		unlink($image->file);

	}

	public function actionActivate()
	{
		$user = $this->loadModel($_GET['user_id']);
		if ($_GET['key'] == $user->activkey) {
			$user->status = 1;
			$user->update(array('status'));
			$auth = Yii::app()->authManager;
			$auth->assign('authorized', $user->id);
				
			$profile = new UserProfile;
			$profile->user_id = $user->id;
			$profile->save();
				
			$modelAccount = new UserAccount;
			$modelAccount->user_id = user()->id;
			$modelAccount->currency_id = 1;
			$modelAccount->save();
				
			Yii::app()->getUser()->setFlash('success',
					Yii::t('core', 'Congratulate! Your account is active!')
			);
			$this->redirect('/site/login');
		}
		else{
			die('error');
		}
	}

	public function actionResetPassword()
	{
		$model = new ResetPassForm;

		$er = false;

		if (isset($_POST['ResetPassForm'])) {
				
			$model->attributes = $_POST['ResetPassForm'];
				
			if ($model->validate(array('username', 'verifyCode'))){

				$user = $model->user;
				$user->password_reset = 1;

				$new_pass = $user->generatePass();

				$user->password = $new_pass;

				if($user->save()) {
					$text = '<p>Your new password is <i>'.
							$new_pass.
							'</i></p>';
						
					$message = new YiiMailMessage;
					$message->setBody($text, 'text/html');
					$message->subject = 'Reset Password';
					$message->addTo($user->email);
					$message->from = '*****@********';
					if (Yii::app()->mail->send($message)) {
						Yii::app()->getUser()->setFlash('success',
								Yii::t('core',
										'Your password was reset successfully, check your mailbox, please.')
						);
						$this->redirect(Yii::app()->homeUrl);
					}
				}

			}
			else{
				$er = true;
			}
		}

		$this->render('pass_reset', array(
				'model' => $model,
				'er' => $er
		));
	}

	public function actionUpdatePassword()
	{
		$model = new UpdatePassForm;

		$er = false;

		if (isset($_POST['UpdatePassForm'])) {
				
			$model->attributes = $_POST['UpdatePassForm'];
				
				
			if ($model->validate()){
				// 				echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($model->new_password); echo'</pre>';die;
				$user = user()->model;
				$user->password_reset = 1;

				$user->password = $model->new_password;

				if($user->save()) {
					$text = '<p>Your new password is <i>'.
							$model->new_password.
							'</i></p>';
						
					$message = new YiiMailMessage;
					$message->setBody($text, 'text/html');
					$message->subject = 'Update Password';
					$message->addTo($user->email);
					$message->from = '*******@*********';
					if (Yii::app()->mail->send($message)) {
						Yii::app()->getUser()->setFlash('success',
								Yii::t('core',
										'Your password was reset successfully.')
						);
						$this->redirect('/user/'.$user->id);
					}
				}

			}
			else{
				$er = true;
			}
		}

		$this->render('pass_update', array(
				'model' => $model,
				'er' => $er
		));
	}

	public function actionCabinet()
	{
		$model=new Camera('search');
		$model->unsetAttributes();  // clear any default values
		$model->user_id = user()->id;
		if(isset($_GET['Camera']))
			$model->attributes=$_GET['Camera'];

		$this->render('cabinet', array(
				'model'=>$model,
		));
	}

	public function actionServices()
	{

		$userTariff = 'You have not any tariff! Why???';

		if ($this->currentUser->tariff) {
			$userTariff = $this->currentUser->tariff->tariff;
		}

		$this->render('services', array(
		// 				'model'=>$model,
				'tariff'=>$userTariff,
		));
	}

	public function actionAccount()
	{
		$modelAccount = new UserAccount;

		$modelTransaction = new UserAccountTransaction;

		if(Yii::app()->session->get('new_cam')){
				
		}

		if (is_null($this->currentUser->account)) {
			// 			if(isset($_POST['UserAccount'])){
			// 				$modelAccount->attributes=$_POST['UserAccount'];
			$modelAccount->user_id = user()->id;
			$modelAccount->currency_id = 1;
			if($modelAccount->save())
				$this->refresh();
			// 			}
				
			// 			$this->render('account_new', array('model'=>$modelAccount));
		}
		else{
			$modelAccount = $modelAccount->findByAttributes(array('user_id'=>user()->id));
				
			$sessionCamUpdate = null;
			if (Yii::app()->session->get('new_cam')) {
				$sessionCamUpdate = Yii::app()->session->get('new_cam');
			}
			elseif(Yii::app()->session->get('update_cam')){
				$sessionCamUpdate = Yii::app()->session->get('update_cam');
			}
				
			if(!is_null($sessionCamUpdate) && $modelAccount->balance < $sessionCamUpdate['cost_per_day']){
				$this->redirect(array('refillAccount', 'acc_id'=>$modelAccount->id));
			}

			$transactions = new UserAccountTransaction('search');
			$transactions->unsetAttributes();
			$transactions->account_id = $modelAccount->id;
			if(isset($_GET['UserAccountTransaction'])){
				$transactions->attributes=$_GET['UserAccountTransaction'];
				$transactions->account_id = $modelAccount->id;
			}
				
			$this->render('account', array(
					'model'=>$modelAccount,
					'transactions'=>$transactions
			));
		}

	}

	public function actionRefillAccount()
	{

		$modeTransaction = new UserAccountTransaction;

		if (isset($_REQUEST['acc_id'])) {
			$modelAccount = UserAccount::model()->findByPk(Yii::app()->request->getQuery('acc_id'));
				
			$sessionCamUpdate = null;
			if (Yii::app()->session->get('new_cam')) {
				$sessionCamUpdate = Yii::app()->session->get('new_cam');
			}
			elseif(Yii::app()->session->get('update_cam')){
				$sessionCamUpdate = Yii::app()->session->get('update_cam');
			}
				
			if(!is_null($sessionCamUpdate) && $modelAccount->balance < $sessionCamUpdate['cost_per_day']){
				$modeTransaction->amount = $sessionCamUpdate['cost_per_day'];
			}
				
			if (!is_null($modelAccount)) {
				if(isset($_POST['UserAccountTransaction']))	{
					$modeTransaction->attributes=$_POST['UserAccountTransaction'];
					$modeTransaction->account_id = $modelAccount->id;
					$modeTransaction->description = 'Пополнение счёта в системе';
					$modeTransaction->type_transaction_id = 1;
					//if ($modeTransaction->payment_type == UserAccountTransaction::PAY_DIRECTLY) {
						$modeTransaction->status = UserAccountTransaction::STATUS_WAIT;
					//}
						
					if($modeTransaction->save()){
						if ($modeTransaction->payment_type == UserAccountTransaction::PAY_DIRECTLY
								&& $modeTransaction->status == UserAccountTransaction::STATUS_WAIT) {
								
							$adminLink = CHtml::link(Yii::app()->createAbsoluteUrl('/admin/userAccountTransaction', array('id' => $modeTransaction->id)),
									Yii::app()->createAbsoluteUrl('/admin/userAccountTransaction', array('id' => $modeTransaction->id)));
								
							$adminText = 'Подтвердите прямой платёж '.$adminLink;
								
							$message = new YiiMailMessage;
							$message->setBody($adminText,'text/html');
							$message->subject = 'New payment';
							$message->addTo(Settings::val('paymaster'));
							$message->from = '****@*****';
							if (Yii::app()->mail->send($message)) {
								if (!is_null($sessionCamUpdate)) {
									user()->setFlash('wait_and_enable_camera',
											Yii::t('messages',
													'You can activate the camera after your payment will be confirmed.'));
									$this->redirect(array('cameraAdd'));
								}
								$this->redirect('account');
							}
						}
						elseif ($modeTransaction->payment_type == UserAccountTransaction::PAY_EPAY){
							$payOrder = new UserOrder;
							
							$payOrder->transaction_id = $modeTransaction->id;
							$payOrder->user_id = user()->id;
							$payOrder->amount = $modeTransaction->amount;
							
							if ($payOrder->save()) {
								$kkb_proc = new KKBproc;
								
								$appendix = '<document><item number="1" name="Пополнение счёта в системе VIDOnline" quantity="1" amount="'.$payOrder->amount.'"/></document>';
								
								$order=array(
											'content64'=>$kkb_proc->process_request($payOrder->id,$payOrder->currency, $payOrder->amount),
											'email'=>$payOrder->user->email,
											'appendix'=>base64_encode($appendix),
											'amount'=>$payOrder->amount,
										);
								return $this->render('confirm_pay', array(
										'model'=>$order));
							}
							
						}
						$modelAccount->balance = $modelAccount->balance + $modeTransaction->amount;
						$modelAccount->save();
						if (Yii::app()->session->get('new_cam')) {
							$this->redirect(array('cameraAdd'));
						}
						if (Yii::app()->session->get('update_cam')) {
							$this->redirect(array('cabinet'));
						}
						$this->redirect(array('account'));
					}
				}
			}
			else{
				throw new CHttpException(404,'The requested account does not exist.');
			}
		}
		else{
			throw new CHttpException(404,'The requested account does not exist.');
		}

		$this->render('refill', array(
				'model'=>$modeTransaction,
		));
	}

	public function actionCameraAdd()
	{
		$model = new Camera;

		$user = $this->loadModel(user()->id);
		
		if ($user->countSelfCameras  >= $user->tariff->tariff->number_cameras) {
			user()->setFlash('error', Yii::t('core', 'Your cameras limit is exceeded.'));
			$this->redirect(array('/cabinet/index'));
		}

		$userTariff = UserTariff::model()->findByAttributes(array('user_id'=>user()->id, 'active'=>'1'))->tariff;

		if (Yii::app()->session->get('new_cam')) {
				
			$model->attributes=Yii::app()->session->get('new_cam');
				
			if($model->save()){

				Yii::app()->session->remove('new_cam');

				if (user()->hasFlash('wait_and_enable_camera')) {
					user()->setFlash('wait_and_enable_camera', user()->getFlash('wait_and_enable_camera'));
				}
				$this->redirect(array('/cabinet/index'));
			}
			else {

				echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($model->errors); echo'</pre>';
			}
				
		}

		if(isset($_POST['Camera']))
		{
			$model->attributes=$_POST['Camera'];
			$model->user_id = user()->id;
			
			/* testing and processing of payment options */
			if (!is_null($user->account)) {
				if (!UserAccount::checkBalance(user()->id, $model->cost_per_day)) {
					if (Yii::app()->session->get('new_cam')) {
						Yii::app()->session->remove('new_cam');
					}
					$_POST['Camera']['user_id'] = user()->id;
					$_POST['Camera']['active'] = 0;
					Yii::app()->session['new_cam'] = $_POST['Camera'];
					Yii::app()->getUser()->setFlash('no_balance',
							Yii::t('core', 'You do not have enough funds in your account.'));
					$this->redirect('/user/account');
				}
				else{
					$account = UserAccount::model()->findByAttributes(array('user_id'=>user()->id));
						
					$transaction = new UserAccountTransaction;
					$transaction->amount = $model->cost_per_day;
					$transaction->type_transaction_id = 2;
					$transaction->description = 'Оплата дневного тарифа по камере '.$model->name.'('.$model->machine_name.')';
					$transaction->account_id = $account->id;
					$transaction->payment_type = UserAccountTransaction::PAY_INNER;
					$transaction->status = UserAccountTransaction::STATUS_SUCCESS;
						
					if ($transaction->save()) {
						$model->last_payment_time = $transaction->create_time;
					}
					else{
						echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($transaction->errors); echo'</pre>';die;
					}
				}
			}
			else{
				Yii::app()->session['new_cam'] = $_POST['Camera'];
				Yii::app()->getUser()->setFlash('no_account',
						Yii::t('core', 'You must open an account before add the camera.'));

				$this->redirect('/user/account');

			}
				
			if($model->save()){
				$this->redirect(array('/cabinet/index'));
			}
			else {
				echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($model->errors); echo'</pre>';die;
			}
		}

		$this->render('camera_add',array(
				'model'=>$model,
				'tarif'=>$userTariff,
		));
	}

	public function actionChangeTarif()
	{
		$model = new UserTariff;

		$isTarif = UserTariff::model()->findByAttributes(array('user_id'=>user()->id, 'active'=>'1'));

		if(isset($_POST['UserTariff']))
		{
			$datetime = date('Y-m-d H:i:s');
				
			$model->attributes=$_POST['UserTariff'];
			$model->user_id = user()->id;
			$model->create_time = $datetime;
			$model->active = 1;
				
			$oldModel = UserTariff::model()->findByAttributes(array('user_id'=>user()->id, 'active'=>1), 'create_time <> :datetime', array(':datetime' => $datetime));
			if($model->save()){
				$oldModel->active = 0;
				if ($oldModel->save()) {
					$this->redirect(array('services'));
				}
			}
		}

		$this->render('change_tarif',array(
				'model'=>$model,
				'isTarif'=>$isTarif->tariff_id
		));
	}

	protected function getCurrentUser()
	{
		return $this->loadModel(user()->id);
	}

	public function actionAutocompleteNames()
	{
		if (Yii::app()->request->isAjaxRequest && isset($_GET['term'])) {
			$models = User::model()->loadNames($_GET['term']);
			$result = array();
			foreach ($models as $m)
				$result[] = array(
						'value' => $m->username,
						'id' => $m->id,
				);

			echo CJSON::encode($result);
		}
	}
	
	public function actionPayResponse()
	{
// 		die('111');
		$kkb_proc = new KKBproc;
		$result = 0;
		if(isset($_POST["response"])){
			$response = $_POST["response"];
				
			$result = $kkb_proc->process_response(stripslashes($response));
			//foreach ($result as $key => $value) {echo $key." = ".$value."<br>";}
			if (is_array($result)){
				if (in_array("ERROR",$result)){
					
					$order = UserOrder::model()->findByPk($result['RESPONSE_ORDER_ID']);
					$order->error = 1;
					$order->error_type = $result["ERROR_TYPE"];
					$order->error_code = $result["ERROR_CODE"];
					$order->customer_name = $result['CUSTOMER_NAME'];
					$order->save();
					
				}
				if (in_array("DOCUMENT",$result)){
					$order = UserOrder::model()->findByPk($result['ORDER_ORDER_ID']);
					$user = User::model()->findByPk($order->user_id);

					if (!is_null($user->sess_id)) {
						$session = Yii::app()->session->readSession($user->sess_id);
						session_decode($session);
						if(isset(Yii::app()->session['new_cam'])){
							$cam = Yii::app()->session['new_cam'];
							if ($result['ORDER_AMOUNT'] >= $cam['cost_per_day']) {
								$new_cam = new Camera;
								$new_cam->attributes = $cam;
								$new_cam->active = 1;
								$new_cam->save();
								
								Yii::app()->session->remove('new_cam');
								Yii::app()->session->writeSession($user->sess_id, session_encode());
							}
						}
					}
					$order->response_code = $result['PAYMENT_RESPONSE_CODE'];
					$order->customer_name = $result['CUSTOMER_NAME'];
					$order->save();
					
				}
			}
			else {
				echo "System error".$result;
			}
		}
		
		$this->render('/site/pay_response');
	}
	
	public function actionTickets()
	{
		$model=new Ticket('search');
		$model->unsetAttributes();  // clear any default values
		$model->user_id = user()->id;
		if(isset($_GET['Ticket']))
			$model->attributes=$_GET['Ticket'];
	
		$this->render('ticket/admin',array(
				'model'=>$model,
		));
	}
	
	public function actionCreateTicket()
	{
		$model=new Ticket;
	
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
	
		if(isset($_POST['Ticket']))
		{
			$model->attributes=$_POST['Ticket'];
			$model->user_id = user()->id;
			if($model->save())
				$this->redirect(array('viewTicket','idTicket'=>$model->id));
		}
	
		$this->render('ticket/create',array(
				'model'=>$model,
		));
	}
	
	public function actionCreateReview()
	{
		$model=new Review;
		
		if (user()->checkAccess('admin')) {
			$model->status = Review::STATUS_ACTIVE;
		} else {
			$model->status = Review::STATUS_INACTIVE;
		}
		
		$model->user_id = user()->id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Review']))
		{
			$model->attributes=$_POST['Review'];
			if($model->save()) {
				if ($model->status == Review::STATUS_INACTIVE) {
					user()->setFlash('success', Yii::t('messages',
					'<strong>Thanks!</strong> Your comment will appear after moderation service administrator.'));
//					'<strong>Спасибо!</strong> Ваш комментарий появится после модерации администратором сервиса.'
				}
				$this->redirect(array('/cabinet'));
			}
		}

		$this->render('review/create',array(
			'model'=>$model,
		));
	}
	
	public function actionViewTicket()
	{
		if (isset($_GET['idTicket'])) {
			$model=Ticket::model()->findByPk($_GET['idTicket']);
			
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
			
			$messages=new TicketMessage('search');
			$messages->unsetAttributes();
			$messages->ticket_id = $model->id;
			
			$this->render('ticket/view',array(
					'model'=>$model,
					'messages'=>$messages
			));
		}
		else{
			throw new CHttpException(404,'The requested page does not exist.');
		}
		
		
	}
	
	public function actionCreateTicketMessage()
	{
		$model=new TicketMessage;
	
		if (!isset($_REQUEST['ticket_id'])) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
	
		$model->ticket_id = $_REQUEST['ticket_id'];
		$ticket = Ticket::model()->findByPk($_REQUEST['ticket_id']);
	
		$quest_text = NULL;
		if (isset($_REQUEST['ask_id']) && !empty($_REQUEST['ask_id'])) {
			$model->ask_id = $_REQUEST['ask_id'];
			$quest = TicketMessage::model()->findByPk($_REQUEST['ask_id']);
			$quest_text = $quest->message;
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
	
		if(isset($_POST['TicketMessage']))
		{
			$model->attributes=$_POST['TicketMessage'];
			$model->user_id = user()->id;
			$model->type = 'customer';
			if($model->save())
				$this->redirect(array('/user/viewTicket','id'=>$model->ticket_id));
		}
	
		$this->render('ticket/createMessage',array(
				'model'=>$model,
				'ticket_text'=>$ticket->description,
				'quest_text'=>$quest_text
		));
	}
	
}
