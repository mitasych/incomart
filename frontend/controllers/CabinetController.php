<?php

class CabinetController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/columnCabinet2';

	/**
	 * @return array action filters
	 */
// 	public function filters()
// 	{
// 		return array(
// 			'rights',
// // 			'accessControl', // perform access control for CRUD operations
// 		);
// 	}
	
	public function actions() {
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha' => array(
						'class' => 'CCaptchaAction',
						'testLimit' => 1,
						'backColor' => 0xFFFFFF,
				),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionIndex()
	{
		
		$user = User::model()->findByPk(user()->id);
// 		echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($user->availableCameras); echo'</pre>';die;
		$model=new Camera('search');
		$model->unsetAttributes();  // clear any default values
		$model->user_id = user()->id;
		
		if(isset($_GET['Camera'])){
			$model->attributes=$_GET['Camera'];
		}
		
		$criteria=new CDbCriteria;
		$criteria->addInCondition('id', $user->availableCameras);
		$avCameras = new CActiveDataProvider('Camera', array(
			'criteria'=>$criteria,
		));
		
		$this->render('home', array(
				'model'=>$model,
				'avCameras'=>$avCameras
		));
	}
	
	public function actionCameraAdd()
	{
		$model = new Camera;
		
		if(isset($_POST['Camera']))
		{
			$model->attributes=$_POST['Camera'];
			
			if($model->save()){
				$this->redirect(array('/user/cabinet'));
			}
			else {
				echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($model->errors); echo'</pre>';die;
			}
		}
		
		$this->render('camera_add',array(
				'model'=>$model,
		));
	}
	
	public function actionArchiveExport()
	{
		$this->render('archive_export'
// 				,array(
// 						'model'=>$model,
// 					)
				);
	}
	
	public function actionArchive()
	{
		if(!isset($_GET['name']) || is_null(Camera::model()->findByAttributes(array('machine_name'=>$_GET['name'])))){
			throw new CHttpException(404,'The requested page does not exist.');
		}
		
		$model = new ArchiveForm;
		
		$model->name = $_GET['name'];
		$model->date = date('Y-m-d');
		
// 		$date = date('Y/m/d');
		
// 		$name = $_GET['name'];
		
// 		$ch = curl_init('http://178.88.68.174:8080/flussonic/api/dvr_status/'.$date.'/'.$name);
		
// 		curl_setopt($ch, CURLOPT_USERPWD, "test:test");
		
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
// 		$res = array();
		
// 		$decode = CJSON::decode(curl_exec($ch));
// 		if (is_array($decode)){
// 			$res = $decode;
// 		}
		
// 		curl_close($ch);
		
		$this->render('archive'
				,array(
						'model'=>$model,
// 						'name'=>$name
					)
				);
	}
	
	public function actionAjaxArchive()
	{
		$model = new ArchiveForm;
		
		if(!Yii::app()->request->isAjaxRequest){
			throw new CHttpException(404,'The requested page does not exist.');
		}
		
		$getLink = NULL;
		
		if(isset($_POST['ArchiveForm']))	{
			$model->attributes=$_POST['ArchiveForm'];
			
			if($model->validate()){
				$output = 1;
			}
			else {
				$output = 2;
			}
			
			$ch = curl_init('http://178.88.68.174:8080/'.$model->name.'/index-'.$model->startTime.'-'.$model->lengthSeconds.'.m3u8');
			
			curl_setopt($ch, CURLOPT_USERPWD, "test:test");
	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			curl_setopt($ch,CURLOPT_NOBODY,true);
			
			curl_setopt($ch,CURLOPT_HEADER,true);
			
			curl_exec($ch);
			
			$code = curl_getinfo ($ch , CURLINFO_HTTP_CODE);
			
			curl_close($ch);
			
			if($code == 200 && $model->getFile == 1){
				$camera = Camera::model()->findByAttributes(array('machine_name'=>$model->name));
				if($camera->active == 1 && $camera->archive == 1){
					$getLink = 'http://178.88.68.174:8080/'.$model->name.'/archive-'.$model->startTime.'-'.$model->lengthSeconds.'.mp4';
				}
				else{
					$getLink = 1;
				}
			}
	
		}
		
		$this->renderPartial('ajaxArchive'
				,array(
						'model'=>$model,
						'output' => $output,
						'code' => $code,
						'getLink' => $getLink
				)
		);
	}
	
// 	public function actionArchive()
// 	{
// 		$date = date('Y/m/d');
		
// 		$name = $_GET['name'];
		
// 		$ch = curl_init('http://178.88.68.174:8080/flussonic/api/dvr_status/'.$date.'/'.$name);
		
// 		curl_setopt($ch, CURLOPT_USERPWD, "test:test");
		
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
// 		$res = array();
		
// 		$decode = CJSON::decode(curl_exec($ch));
// 		if (is_array($decode)){
// 			$res = $decode;
// 		}
		
// 		curl_close($ch);
		
// 		$this->renderPartial('archive'
// 				,array(
// 						'data'=>$res,
// 						'name'=>$name
// 					)
// 				);
// 	}
	
	public function actionPaytest()
	{
		$kkb_proc = new KKBproc;
		$order_id = 302;			
		$currency_id = "398"; 		
		$amount = 100;			
		$content = $kkb_proc->process_request($order_id,$currency_id,$amount);
		
		$this->render('testpay', array('content'=>$content));
	}
	
	public function actionArchiveTime()
	{
// 		$date = date('Y/m/d');
		$date = str_replace('-', '/', $_GET['date']);
		
		$name = $_GET['name'];

		$ch = curl_init('http://178.88.68.174:8080/flussonic/api/dvr_status/'.$date.'/'.$name);

		curl_setopt($ch, CURLOPT_USERPWD, "test:test");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$res = array();

		$decode = CJSON::decode(curl_exec($ch));
		if (is_array($decode)){
			$res = $decode;
		}

		curl_close($ch);
		foreach($res as $id => &$item){
			list($year, $month, $day, $hour, $minute) = explode('/', $item['path']);
// 			unset($item['segments']);
// 			$item['hour'] = $hour;
// 			$item['minute'] = $minute;
			$item = $hour.'_'.$minute;
		}
		$res = array_flip($res);
		//echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($res); echo'</pre>';die;
		$this->renderPartial('_time_grid'
				,array(
						'data'=>$res,
						'name'=>$name
				)
		);
	}
	
}
