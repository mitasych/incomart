<?php

class CameraController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/columnCabinet1';

	/**
	 * @return array action filters
	 */
// 	public function filters()
// 	{
// 		return array(
// 			'accessControl', // perform access control for CRUD operations
// 		);
// 	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
// 	public function actionCreate()
// 	{
// 		$model=new Camera;

// 		// Uncomment the following line if AJAX validation is needed
// 		// $this->performAjaxValidation($model);

// 		if(isset($_POST['Camera']))
// 		{
// 			$model->attributes=$_POST['Camera'];
// 			if($model->save())
// 				$this->redirect(array('/user/cabinet'));
// 		}

// 		$this->render('create',array(
// 			'model'=>$model,
// 		));
// 	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$userTariff = UserTariff::model()->findByAttributes(array('user_id'=>user()->id, 'active'=>'1'))->tariff;
		
		$bitrates = $model->user->tariff->tariff->simpleBitrates;
		
// 		$name = $model->name;
		$oldAttrs = $model->attributes;
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Camera']))
		{
			$model->attributes=$_POST['Camera'];
			$model->name = $oldAttrs['name'];
			$model->machine_name = $oldAttrs['machine_name'];
			
			if ($model->active == 1 && (($model->paydate || is_null($model->last_payment_time)) || $model->attributeChanged('cost_per_day')) ) {
				if (!UserAccount::checkBalance(user()->id, $model->cost_per_day)) {
					if (Yii::app()->session->get('update_cam')) {
						Yii::app()->session->remove('update_cam');
					}
					$_POST['Camera']['active'] = 0;
					Yii::app()->session['update_cam'] = $_POST['Camera'];
					Yii::app()->getUser()->setFlash('no_balance',
							Yii::t('core', 'You do not have enough funds in your account.'));
					$this->redirect('/user/account');
				}
				else {
					$account = UserAccount::model()->findByAttributes(array('user_id'=>user()->id));
					
					$transaction = new UserAccountTransaction;
					$transaction->amount = $model->cost_per_day;
					$transaction->type_transaction_id = 2;
					$transaction->description = 'Оплата дневного тарифа по камере '.$model->name.'('.$model->machine_name.')';
					$transaction->account_id = $account->id;
					$transaction->payment_type = UserAccountTransaction::PAY_INNER;
					$transaction->status = UserAccountTransaction::STATUS_SUCCESS;
					
					if ($transaction->save()) {
						$model->last_payment_time = $transaction->create_time;
					}
					else{
						echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($transaction->errors); echo'</pre>';die;
					}
					
				}
			}
			if ($model->save()) {
				$this->redirect(array('/cabinet/index'));
			}
			
		}

		$this->render('update',array(
			'model'=>$model,
			'bitrates' => $bitrates,
			'tarif'=>$userTariff,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			
				$model = $this->loadModel($id);
				$ssh = new Net_SSH2(Settings::val('vsHost'));
				
				if (!$ssh->login(Settings::val('vsRoot'), Settings::val('vsRootPass'))) {
					exit('Login Failed');
				}
				else {
					if ($ssh->exec('test -f '.Settings::val('vsDirectory').$model->name.'.conf && echo 1') == 1) {
						$ssh->exec('rm '.Settings::val('vsDirectory').$model->name.'.conf');
						$ssh->exec('/etc/init.d/flussonic reload');
					}
					$model->delete();
				}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Camera');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Camera('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Camera']))
			$model->attributes=$_GET['Camera'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Camera::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='camera-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionLoadmodels()
	{
		$data = CamerasModel::model()->findAll(
				array('condition'=>"manufacturer_id='".$_POST['Camera']['manufacturer_id']."'")
		);
		$data=CHtml::listData($data,'id','name');
		
		echo CHtml::tag('option', array('value'=>''),'Select',true);
		
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
		}
	}
	
	public function actionLoadCameraUrl()
	{
		$url = '';
		$htmlResolution = '';
		$htmlFrameRates = '';
		
		$camera = CamerasModel::model()->findByPk($_POST['Camera']['model_id']);
		
		if ($camera !== null) {
			$url = $camera->url;
			///
			$resolutions = CameraResolution::model()->findAllByAttributes(array('id'=>explode(',', $camera->resolutions_ids)));
			$dataResolutions=CHtml::listData($resolutions,'id','name');
			$htmlResolution .= CHtml::tag('option',	array('value'=>''),'Select',true);
			
			foreach ($dataResolutions as $val=>$name){
				$htmlResolution .= CHtml::tag('option',
						array('value'=>$val),CHtml::encode($name),true);
			}
			///
			$frame_rates = CameraFrameRate::model()->findAllByAttributes(array('id'=>explode(',', $camera->frame_rate_ids)));
			$dataFrameRates=CHtml::listData($frame_rates,'id','frame_rate');
			$htmlFrameRates .= CHtml::tag('option',	array('value'=>''),'Select',true);
			
			foreach ($dataFrameRates as $val=>$name){
				$htmlFrameRates .= CHtml::tag('option',
						array('value'=>$val),CHtml::encode($name),true);
			}
		}
		
		echo CJSON::encode(array('url'=>$url, 'resolutions'=>$htmlResolution, 'frame_rates'=>$htmlFrameRates));
	}
	
	public function actionCalcMinuteSpace()
	{
		$data = array();
		if (isset($_POST['resolution_id']) && 
				isset($_POST['frame_rate_id']) && 
				!empty($_POST['resolution_id']) && 
				!empty($_POST['frame_rate_id']) &&
				isset($_POST['bitrate_id']) &&
				!empty($_POST['bitrate_id']) &&
				isset($_POST['archive']) &&
				isset($_POST['archive_days']) &&
				!empty($_POST['archive_days'])				
			) {
			$modelResolution = CameraResolution::model()->findByPk($_POST['resolution_id']);
			$modelFrameRate = CameraFrameRate::model()->findByPk($_POST['frame_rate_id']);
			$modelBitrate = VideoBitrate::model()->findByPk($_POST['bitrate_id']);
			
			$tarif = UserTariff::model()->findByAttributes(array('user_id'=>user()->id, 'active'=>'1'));
			
			$archive_cost = 0;
			
			
	
			//$space = (((($modelResolution->height_dimension * $modelResolution->width_dimension) * 24) / 8) / 1024) * $modelFrameRate->frame_rate * 60;
			$space_per_day = ($modelBitrate->value * 60 * 60 * 24) * $_POST['archive_days'];
			
			$space_per_day_mb = (($modelBitrate->value / 1024 / 8) * 60 * 60 * 24);
			$data['space_kb']=$space_per_day;
			$data['space_mb']=round($space_per_day_mb, 1);
			$data['space_mb_all']=round(($space_per_day_mb * $_POST['archive_days']), 1);
			if ($_POST['archive'] == 1) {
				$archive_cost = $tarif->tariff->archive_cost_per_mb * $data['space_mb'];
			}
			
			$data['cost_per_day']=$modelResolution->getRelTarifByTarifId($tarif->tariff_id)+
									$modelFrameRate->getRelTarifByTarifId($tarif->tariff_id)+
									$modelBitrate->getRelTarifByTarifId($tarif->tariff_id)+
									$archive_cost;
			
			$data['cost_all']=($modelResolution->getRelTarifByTarifId($tarif->tariff_id)*$_POST['archive_days'])+
									($modelFrameRate->getRelTarifByTarifId($tarif->tariff_id)*$_POST['archive_days'])+
									($modelBitrate->getRelTarifByTarifId($tarif->tariff_id)*$_POST['archive_days'])+
									($archive_cost*$_POST['archive_days']);
		}
		
		echo CJSON::encode($data);
	}
	
	public function actionCalcSpace()
	{
		$data = array();
		if (isset($_POST['per_min_kb']) && isset($_POST['days_number']) && !empty($_POST['per_min_kb']) && !empty($_POST['days_number'])) {

			$data['kb'] = $_POST['per_min_kb'] * 60 * 24 * $_POST['days_number'];
			$data['mb'] = round(($data['kb'] / 1024), 1);
			$data['gb'] = round(($data['kb'] / 1048576), 1);
			
		}
		
		echo CJSON::encode($data);
	}
}
