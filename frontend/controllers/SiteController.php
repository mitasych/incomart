<?php
/**
 * SiteController.php
 *
 * @author: NikoSmart <support@nikosmart.com>
 */
class SiteController extends Controller {
	public $layout='//layouts/column1';
	
// 	public function filters()
// 	{
// 		return array(
// // 				'accessControl',
// 				'rights'
// 		);
// 	}

	public function accessRules()
	{
		return array(
			// not logged in users should be able to login and view captcha images as well as errors
			array('allow', 'actions' => array('index', 'captcha', 'login', 'error',
					 'KK', 'signup', 'selectMonth', 'search')),
			// logged in users can do whatever they want to
			array('allow', 'users' => array('@')),
			// not logged in users can't do anything except above
			array('deny'),
		);
	}
	/**
	 * Declares class-based actions.
	 * @return array
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'testLimit' => 1,
				'backColor' => 0xFFFFFF,
			),
		);
	}

	/* open on startup */
	public function actionIndex()
	{
		if (isset($_GET)) {
			if (isset($_GET['day'])) {
				$this->redirect('/post?'.http_build_query($_GET));
			}
		}
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	/* public function actionLogin() {
		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login', array('model' => $model));
	} */

	public function actionLogin()
	{
		$model = new LoginForm();
	
		$er = false;
		if (isset($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];
			
			if ($model->validate(array('username', 'password', 'verifyCode')) && $model->login()){
				user()->model->login_attempts = 0;
				user()->model->sess_id = Yii::app()->session->sessionID;
				user()->model->save();
				
				$this->redirect('/cabinet');
// 				$this->redirect(user()->returnUrl);
			}
			else{
				$er = true;
			}
		}
	
		$sent = r()->getParam('sent', 0);
		$this->render('login', array(
				'model' => $model,
				'sent' => $sent,
				'er' => $er
		));
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() 
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionSignup()
	{
		
		$auth=Yii::app()->authManager;
		
		$user = new User(User::SCENARIO_SIGNUP);

		
		if (isset($_GET['tariff']) && in_array($_GET['tariff'], Tarif::listItensByMachine())) {
			$tarifMachineName = $_GET['tariff'];
		}
		else {
			$tarifMachineName = 'home';
		}
		
		$tarif = Tarif::model()->findByAttributes(array('machine_name'=>$tarifMachineName));
		
		if(isset($_POST['User'])) {
			$user->attributes = $_POST['User'];
			
			$user->activkey = $user->generateActiveCode();
		
		
			if($user->save()) {
				
				$auth->assign('customer', $user->id);
				
				// UserTariff
				$userTariff = new UserTariff();
				$userTariff->user_id = $user->id;
				$userTariff->tariff_id = $tarif->id;
				
				$userTariff->save();
				//
				
				$text = '<p>You can activate your account now. Follow this link '.
				CHtml::link(Yii::app()->createAbsoluteUrl('user/activate/', array('id' => $user->id, 'key' => $user->activkey)), Yii::app()->createAbsoluteUrl('user/activate/', array('id' => $user->id, 'key' => $user->activkey))).
				'</p>';
				
				$message = new YiiMailMessage;
				$message->setBody($text,'text/html');
				$message->subject = 'Welcome';
				$message->addTo($user->email);
				$message->from = 'inf***o@*****';
				if (Yii::app()->mail->send($message)) {
					Yii::app()->getUser()->setFlash('success',
							Yii::t('core', 
									'To complete the registration you must follow the link, that we sent to your mailbox')
					);
					$this->redirect(Yii::app()->homeUrl);
				}
			}
		}
		
		
		// Вывести форму
		$this->render('signup', array('model'=>$user, 'tariff'=>$tarif));
	}
	
	public function actionReclame()
	{
		$model = new OrderBannerForm();
		
		if (isset($_POST['OrderBannerForm'])) {
			$model->attributes = $_POST['OrderBannerForm'];

			if ($model->validate(array('email', 'verifyCode'))){
				
				$service = '';
				
				$subject = 'Banner Advertising';
				
				if (!empty($model->service)) {
					$service_model = BannerServices::model()->findByPk($model->service);
					$service = '<h3>Banner Service #'.$model->service.' <i>"'.$service_model->name.'"</i></h3>';
					$subject .= ' service '.$service_model->name;
				}
				
				$text = $service.$model->content;
				
				$message = new YiiMailMessage;
				$message->setBody($text,'text/html');
				$message->subject = $subject;
				$message->addTo('******@******.com');
				if ($model->copyMe == 1) {
					$message->addCc($model->email);
				}
				$message->from = 'support@nikosmart.com';
				if (Yii::app()->mail->send($message)) {
					Yii::app()->getUser()->setFlash('success',
							Yii::t('core',
									'Your message has been sent successfully')
					);
				}
			}
		}
			
		$dataProvider = new CActiveDataProvider('BannerServices');
		
		$this->render('reclame', array('dataProvider'=>$dataProvider, 'model'=>$model));
		
	}
	
	public function actionContacts()
	{
		$model = new ContactForm();
// 		phpinfo();
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];

			if ($model->validate(array('email', 'verifyCode'))){
				$headers= "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=utf-8\r\n";
				$headers .= "From: {$model->email}\r\nReply-To: {$model->email}\r\n";
				if ($model->copyMe == 1) {
					$headers .= 'Cc:'.($model->email).'\r\n';
				}
				if (mail('*****@*****.com', $model->subject, $model->content)) {
					Yii::app()->getUser()->setFlash('success',
							Yii::t('core',
									'Your message has been sent successfully')
					);
				}
// 				$message = new YiiMailMessage;
// 				$message->setBody($model->content,'text/html');
// 				$message->subject = $model->subject;
// 				$message->addTo('*****@******.com');
// 				if ($model->copyMe == 1) {
// 					$message->addCc($model->email);
// 				}
// 				else{
// 					$message->setReplyTo($model->email);
// 				}
// 				$message->from = 'support@nikosmart.com';
// 				if (Yii::app()->mail->send($message)) {
// 					Yii::app()->getUser()->setFlash('success',
// 							Yii::t('core',
// 									'Your message has been sent successfully')
// 					);
// 				}
			}
		}
			
		$this->render('contacts', array('model'=>$model));
		
	}
	
	public function actionTariffs($minTariffId=1, $maxTariffId=3)
	{
		for ($id = $minTariffId; $id <= $maxTariffId; $id++) {
			$arrID[] = $id;
		}

		$model = Tarif::model()->findAllByPk($arrID);
		if($model === null)
			throw new CHttpException(404,'Tariff not exist.');
		
		$tar_params = array();
		
		foreach ($model as $item) {
			$tar_params['name'][$item->id] = $item->name;
			$tar_params['machine_name'][$item->id] = $item->machine_name;
			$tar_params['cost_camera_per_month'][$item->id] = $item->cost_camera_per_month;
			$tar_params['number_cameras'][$item->id] = $item->number_cameras;
			$tar_params['safe_mode'][$item->id] = $item->safe_mode;
			$tar_params['local_archive'][$item->id] = $item->local_archive;
			$tar_params['embed_mode'][$item->id] = $item->embed_mode;
			$tar_params['clode_archive'][$item->id] = $item->clode_archive;
			$tar_params['AVI_export'][$item->id] = $item->AVI_export;
			$tar_params['another_users_access'][$item->id] = $item->another_users_access;
			$tar_params['online_broadcasting'][$item->id] = $item->online_broadcasting;
			$tar_params['extended_support'][$item->id] = $item->extended_support;
			$tar_params['payment_settlement_account'][$item->id] = $item->payment_settlement_account;
			$tar_params['free_period'][$item->id] = $item->free_period;
		}
		
		$this->render('tariffs', array(
				'model'=>$tar_params,
		));
		
	}
	
	public function actionPayResponse()
	{
		$kkb_proc = new KKBproc;
		$result = 0;
		if(isset($_POST["response"])){
			$response = $_POST["response"];
			
			$result = $kkb_proc->process_response(stripslashes($response));
			//foreach ($result as $key => $value) {echo $key." = ".$value."<br>";}
			if (is_array($result)){
				if (in_array("ERROR",$result)){
					if ($result["ERROR_TYPE"]=="ERROR"){
						echo "System error:".$result["ERROR"];
					} 
					elseif ($result["ERROR_TYPE"]=="system"){
						echo "Bank system error > Code: '".$result["ERROR_CODE"]."' Text: '".$result["ERROR_CHARDATA"]."' Time: '".$result["ERROR_TIME"]."' Order_ID: '".$result["RESPONSE_ORDER_ID"]."'";
					}
					elseif ($result["ERROR_TYPE"]=="auth"){
						echo "Bank system user autentication error > Code: '".$result["ERROR_CODE"]."' Text: '".$result["ERROR_CHARDATA"]."' Time: '".$result["ERROR_TIME"]."' Order_ID: '".$result["RESPONSE_ORDER_ID"]."'";
					}
				}
				if (in_array("DOCUMENT",$result)){
					$order = UserOrder::model()->findByPk($result['ORDER_ORDER_ID']);
					$order->response_code = $result['PAYMENT_RESPONSE_CODE'];
					$order->customer_name = $result['CUSTOMER_NAME'];
					$order->save();
					
					echo "Result DATA: <BR>";
					foreach ($result as $key => $value) {
						echo "Postlink Result: ".$key." = ".$value."<br>";
					}
				}
			} 
			else { 
				echo "System error".$result;
			}
		}
		
		$this->render('pay_response');
	}
	
	public function actionAbout() 
	{
		$this->render('about');
	}
	
	public function actionReviews()
	{
		$reviews = new CActiveDataProvider('Review');
		
		$this->render('reviews', array('reviews'=>$reviews));
	}
	
}