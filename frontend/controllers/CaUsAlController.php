<?php

class CaUsAlController extends Controller
{
	public $layout='//layouts/columnCabinet1';
	
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('CameraUserAllowed');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCreate()
	{
	    $model=new CameraUserAllowed;

	    if(isset($_POST['ajax']) && $_POST['ajax']==='client-account-create-form')
	    {
	        echo CActiveForm::validate($model);
	        Yii::app()->end();
	    }

	    if(isset($_POST['CameraUserAllowed']))
	    {
	        $model->attributes=$_POST['CameraUserAllowed'];
	        if($model->validate())
	        {
				$this->saveModel($model);
				$this->redirect(array('view','id_camera'=>$model->id_camera, 'id_user'=>$model->id_user));
	        }
	    }
	    $this->render('create',array('model'=>$model));
	} 
	
	public function actionDelete($id_camera, $id_user)
	{
		if(Yii::app()->request->isPostRequest)
		{
			try
			{
				// we only allow deletion via POST request
				$this->loadModel($id_camera, $id_user)->delete();
			}
			catch(Exception $e) 
			{
				$this->showError($e);
			}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function actionUpdate($id_camera, $id_user)
	{
		$model=$this->loadModel($id_camera, $id_user);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CameraUserAllowed']))
		{
			$model->attributes=$_POST['CameraUserAllowed'];
			$this->saveModel($model);
			$this->redirect(array('view',
	                    'id_camera'=>$model->id_camera, 'id_user'=>$model->id_user));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionAdmin()
	{
		$model=new CameraUserAllowed('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CameraUserAllowed']))
			$model->attributes=$_GET['CameraUserAllowed'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionView($id_camera, $id_user)
	{		
		$model=$this->loadModel($id_camera, $id_user);
		$this->render('view',array('model'=>$model));
	}
	
	public function loadModel($id_camera, $id_user)
	{
		$model=CameraUserAllowed::model()->findByPk(array('id_camera'=>$id_camera, 'id_user'=>$id_user));
		if($model==null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function saveModel($model)
	{
		try
		{
			$model->save();
		}
		catch(Exception $e)
		{
			$this->showError($e);
		}		
	}

	function showError(Exception $e)
	{
		if($e->getCode()==23000)
			$message = "This operation is not permitted due to an existing foreign key reference.";
		else
			$message = "Invalid operation.";
		throw new CHttpException($e->getCode(), $message);
	}	

	public function actionAddAllowedUser()
	{
		$model=new CameraUserAllowed;
		
// 		if(isset($_POST['ajax']) && $_POST['ajax']==='client-account-create-form')
// 		{
// 			echo CActiveForm::validate($model);
// 			Yii::app()->end();
// 		}
		
		if (isset($_GET['id_camera'])) {
			$model->id_camera = $_GET['id_camera'];
		}
			$owner = User::model()->findByPk(user()->id);

		if ($owner->countAllowedUsers >= $owner->tariff->tariff->another_users_access) {
			echo CJSON::encode(array('error'=>1, 'html'=>Yii::t('core', 'Your limit for users is exceeded.')));
			return;
		}
		
		if(isset($_POST['CameraUserAllowed']))
		{
			$model->attributes=$_POST['CameraUserAllowed'];
			
			if ($model->validate(array('username'))) {
// 				echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($model->username); echo'</pre>';die;
				$user = User::model()->findByAttributes(array('username'=>$model->username));
				$model->id_user = $user->id;
			}
			
			if($model->save())
			{
				$html = $this->renderPartial('_ajax_view',array('model'=>$model), true, true);
				echo CJSON::encode(array('error'=>0, 'html'=>$html));
				return;
			}
			else{
				$html = $this->renderPartial('_ajax_form',array('model'=>$model), true, true);
				echo CJSON::encode(array('error'=>1, 'html'=>$html));
				return;
			}
		}
		$html = $this->renderPartial('_ajax_form',array('model'=>$model), true, true);
		echo CJSON::encode(array('error'=>0, 'html'=>$html));
		return;
	}
	
	public function actionRmAllowedUser()
	{
		if (isset($_GET['id_user']) && isset($_GET['id_camera'])) {
			try
			{
				$this->loadModel($_GET['id_camera'], $_GET['id_user'])->delete();
			}
			catch(Exception $e) 
			{
				$this->showError($e);
			}
		}
	}
}