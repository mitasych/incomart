<?php
/**
 * LoginForm.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 8:37 PM
 */

class ResetPassForm extends CFormModel {

	// maximum number of login attempts before display captcha
	const MAX_LOGIN_ATTEMPTS = 3;

	public $username;
	public $verifyCode;
	private $_user = null;
	
	/**
	 * Model rules
	 * @return array
	 */
	public function rules() {
		return array(
			array('username', 'required'),
			array('username', 'isuser'),
			array('verifyCode', 'validateCaptcha'),
		);
	}

	/**
	 * Returns attribute labels
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'username' => Yii::t('labels', 'Username or e-mail'),
			'verifyCode' => Yii::t('labels', 'verifyCode'),
		);
	}


	public function isuser($attribute, $params)
	{
		if (!$this->user) {
			$this->addError('username', Yii::t('errors', 'The user with this username or email does not exist.'));;
		}
	}
	
	/**
	 * Validates captcha code
	 * @param $attribute
	 * @param $params
	 */
	public function validateCaptcha($attribute, $params) 
	{
		CValidator::createValidator('captcha', $this, $attribute, $params)->validate($this);
	}


	/**
	 * Returns
	 * @return null
	 */
	public function getUser() 
	{
		if ($this->_user === null) {
			$attribute = strpos($this->username, '@') ? 'email' : 'username';
			$this->_user = User::model()->find(array('condition' => $attribute . '=:loginname', 'params' => array(':loginname' => $this->username)));
		}
		return $this->_user;
	}


}
