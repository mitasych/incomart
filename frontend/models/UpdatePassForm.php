<?php
/**
 * LoginForm.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 8:37 PM
 */

class UpdatePassForm extends CFormModel {

	public $old_password;
	public $new_password;
	public $new_password_repeat;
	public $verifyCode;
	private $_user = null;
	
	/**
	 * Model rules
	 * @return array
	 */
	public function rules() {
		return array(
			array('old_password, new_password, new_password_repeat', 'required'),
			array('old_password', 'isUserPassword'),
			array('new_password, new_password_repeat', 'length', 'min'=>6, 'max'=>30),
			array('new_password', 'compare', 'compareAttribute'=>'new_password_repeat'),
			array('verifyCode', 'validateCaptcha'),
		);
	}

	/**
	 * Returns attribute labels
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'old_password' => Yii::t('labels', 'Your old password'),
			'new_password' => Yii::t('labels', 'New password'),
			'new_password_repeat' => Yii::t('labels', 'New password repeat'),
			'verifyCode' => Yii::t('labels', 'verifyCode'),
		);
	}

	public function isUserPassword($attribute,$params)
	{
		if (user()) {
			if(user()->model->password!==crypt($this->$attribute,user()->model->password)){
				$this->addError($attribute, 'Your old password is not correct.');
			}
		}
		else{
			$this->addError($attribute, 'You are not logged in.');
		}
	}
	
	/**
	 * Validates captcha code
	 * @param $attribute
	 * @param $params
	 */
	public function validateCaptcha($attribute, $params) 
	{
		CValidator::createValidator('captcha', $this, $attribute, $params)->validate($this);
	}

}
