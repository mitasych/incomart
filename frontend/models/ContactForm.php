<?php
/**
 * OrderBannerForm.php
 *
 * @author: Aleksandr Mitasov <mitas@nikosmart.com>
 * 
 */

class ContactForm extends CFormModel {

	// maximum number of login attempts before display captcha
	public $name;
	public $subject;
	public $email;
	public $content;
	public $copyMe;
	public $verifyCode;

	/**
	 * Model rules
	 * @return array
	 */
	public function rules() {
		return array(
			array('name, content, email', 'required'),
			array('name', 'length', 'max' => 45),
			array('copyMe', 'numerical', 'integerOnly'=>true),
			array('content', 'length', 'max' => 1500, 'min' => 4),
			array('verifyCode', 'validateCaptcha'),
			array('email', 'email'),
			array('email', 'length', 'max' => 125),
		);
	}

	/**
	 * Returns attribute labels
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'name' => Yii::t('labels', 'Your name'),
			'subject' => Yii::t('labels', 'Subject'),
			'email' => Yii::t('labels', 'E-Mail'),
			'content' => Yii::t('labels', 'Content'),
			'copyMe' => Yii::t('labels', 'Send me a copy'),
			'verifyCode' => Yii::t('labels', 'verifyCode'),
		);
	}
	
	public function getUser() {
		if ($this->_user === null) {
			$attribute = strpos($this->username, '@') ? 'email' : 'username';
			$this->_user = User::model()->find(array('condition' => $attribute . '=:loginname', 'params' => array(':loginname' => $this->username)));
		}
		return $this->_user;
	}


	/**
	 * Validates captcha code
	 * @param $attribute
	 * @param $params
	 */
	public function validateCaptcha($attribute, $params) {
		if ($this->getRequireCaptcha())
			CValidator::createValidator('captcha', $this, $attribute, $params)->validate($this);
	}
	
	
	
	public function getRequireCaptcha() {
		return user() !== null;
	}

}
