<?php
/**
 * OrderBannerForm.php
 *
 * @author: Aleksandr Mitasov <mitas@nikosmart.com>
 * 
 */

class ArchiveForm extends CFormModel {

	// maximum number of login attempts before display captcha
	public $name;
	public $date;
	public $startHour;
	public $startMinute;
	public $endHour;
	public $endMinute;
	public $startDate;
	public $getFile;
	
	public $startTime;

	/**
	 * Model rules
	 * @return array
	 */
	public function rules() 
	{
		return array(
			array('name, date, startHour, startMinute, endHour, endMinute, startDate', 'required'),
			array('name', 'length', 'max' => 150),
			array('date', 'date', 'format'=>'yyyy-MM-dd'),
			array('getFile', 'boolean'),
			array('startHour, startMinute, endHour, endMinute', 'length', 'is'=>2),
			array('startDate', 'date', 'format'=>'yyyy-MM-dd hh:mm', 'timestampAttribute'=>'startTime'),
		);
	}

	/**
	 * Returns attribute labels
	 * @return array
	 */
	public function attributeLabels() 
	{
		return array(
			'name' => Yii::t('labels', 'Cam name'),
			'date' => Yii::t('labels', 'Archive date'),
			'startHour' => Yii::t('labels', 'Start hour'),
			'startMinute' => Yii::t('labels', 'Start minute'),
			'endHour' => Yii::t('labels', 'End hour'),
			'endMinute' => Yii::t('labels', 'End minute'),
			'getFile' => Yii::t('labels', 'get file'),
		);
	}
	
	protected function beforeValidate()
	{
		$this->startDate = $this->date.' '.$this->startHour.':'.$this->startMinute;
		
		return parent::beforeValidate();
	}
	
	public static function listHours()
	{
		$hours = range(0, 23);
		
		foreach ($hours as $key => &$hour){
			if (strlen($hour) == 1) {
				$hour = '0'.$hour;
			}
		}
		
		return array_combine($hours, $hours);
	}
	
	public static function listMinutes()
	{
		$mitutes = range(0, 50, 10);
		
		foreach ($mitutes as &$mitute){
			if (strlen($mitute) == 1) {
				$mitute = '0'.$mitute;
			}
		}

		return array_combine($mitutes, $mitutes);
	}
	
	public function getLengthSeconds()
	{
		$start = strtotime($this->date.' '.$this->startHour.':'.$this->startMinute);
		$end = strtotime($this->date.' '.$this->endHour.':'.$this->endMinute);
		
		return $end - $start;
	}
	
}
