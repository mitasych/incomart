<?php
/**
 * AccountNewForm.php
 *
 * @author: Aleksandr Mitasov <mitas@nikosmart.com>
 * 
 */

class AccountNewForm extends CFormModel {

	// maximum number of login attempts before display captcha
	public $currency;
	public $user_id;

	/**
	 * Model rules
	 * @return array
	 */
	public function rules() {
		return array(
			array('currency, user_id', 'required'),
			array('currency, user_id', 'numerical', 'integerOnly'=>true),
			array('user_id', 'exists', 'id', 'User'),
		);
	}

	/**
	 * Returns attribute labels
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'currency' => Yii::t('labels', 'Currency'),
		);
	}

}
