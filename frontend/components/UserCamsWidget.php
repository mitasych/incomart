<?php
class UserCamsWidget extends CWidget
{
	public $page = '_partner_news';
	
	public function run()
	{
		$model=new Camera('search');
		$model->unsetAttributes();  // clear any default values
		$model->user_id = user()->id;
		
		$this->render('user_cameras', array(
				'model'=>$model,
		));
	}
}
?>