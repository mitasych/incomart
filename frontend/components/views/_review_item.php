<div class="reviews_item">
	<div class="reviews_item_photo">
		<img src="<?php echo url('/image/uploaded/avatarthumb/'.$data->user->profile->photo); ?>" alt="" style="border-radius: 50%">
	</div>
	<div class="reviews_item_info">
		<div><?php echo $data->user->profile->first_name." ".$data->user->profile->last_name;?></div>
		<div><?php echo $data->user->profile->about;?></div>
		<div><?php echo $data->user->profile->country." ".$data->user->profile->city;?></div><!-- site -->
	</div>
	<div class="clear"></div>
		<div class="reviews_item_content">
				<?php echo count(explode(" ", $data->text)) > $this->wordCount ? 
					implode(" ", array_slice(explode(" ", $data->text, $this->wordCount), 0, $this->wordCount-1)).'...' :
					$data->text;
			?>
		</div>
</div><!-- .reviews_item -->