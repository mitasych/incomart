<?php 
// echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($model->search()->data); echo'</pre>';die;
if (empty($model->search()->data)) {
	Yii::app()->user->setFlash('success', '<strong>Здесь</strong> появятся ваши камеры.');
	$this->widget('bootstrap.widgets.TbAlert', array(
			'block'=>true, // display a larger alert block?
			'fade'=>true, // use transitions?
			'closeText'=>'×', // close link text - if set to false, no close link is displayed
			'alerts'=>array( // configurations per alert type
					'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'), // success, info, warning, error or danger
			),
	));
}
else{
	
	$this->widget('bootstrap.widgets.TbGridView',array(
			'id'=>'camera-grid',
			'dataProvider'=>$model->search(),
// 			'filter'=>$model,
			'template'=>'{items}{pager}',
			'htmlOptions'=>array('style'=>'display: none;'),
			'columns'=>array(
					'name',
					'IP_address',
// 					'login',
					array(
							'class'=>'bootstrap.widgets.TbButtonColumn',
// 							'template'=>'{view}{delete}',
							'template'=>'{update}{delete}',
							'buttons'=>array
							(
									'view' => array
									(
											'url'=>'Yii::app()->createUrl("camera/view", array("id"=>$data->id))',
									),
									'delete' => array
									(
											'url'=>'Yii::app()->createUrl("camera/delete", array("id"=>$data->id))',
									),
									'update' => array
									(
											'url'=>'Yii::app()->createUrl("camera/update", array("id"=>$data->id))',
									),
							),
					),
			),
	)); 
}

?>

