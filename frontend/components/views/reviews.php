<div class="block_reviews">
	<div class="block_reviews_left">
		<div class="reviews_header"><span>Система видеонаблюдения on-line</span></div>
			<p>
				Мы гордимся тем, что cегодня наш сервис — это самый популярный в Казахстане облачный сервис 
				видеонаблюдения через Интернет.
			</p>
			<p>
				Наш сервис — это высочайшие стандарты качества, надежности и безопасности. Мы обеспечиваем тысячи 
				просмотров камер для людей из разных мест. Мы помогаем тысячам компаний увидеть, как работает 
				их бизнес, и сделать его лучше. Облачное видеонаблюдение — это будущее, которое доступно уже сегодня.
			</p>
			<p>
				Мы - компания, которая благодаря большому количеству пользователей может позволить себе предложить 
				самые низкие тарифы при самом высоком качестве и надежности!
			</p>
			<p>
				У вас есть вопросы?<br>
				Вы можете написать нам <a href="#" class="link_green">здесь</a>, и мы ответим вам в самое короткое время.
			</p>
	</div>
	<div class="block_reviews_right">
		<div class="reviews_header"><span>Отзывы о нас</span></div>
			<?php
				$this->widget('bootstrap.widgets.TbListView',array(
					'dataProvider'=>$dataProvider,
					'itemView'=>$this->page,
					'template'=>'{items}',
				));
			?>
			
			
<!-- 			<div class="reviews_item">
				<div class="reviews_item_photo">
					<img src="<?php //echo Yii::app()->theme->baseUrl; ?>/img/reviews_item_photo1.png" alt="">
				</div>
				<div class="reviews_item_info">
					<div>Ирина Морозова</div>
					<div>Специалист по SEO-продвижению</div>
					<div>i-morozova.ru</div>
				</div>
				<div class="clear"></div>
					<div class="reviews_item_content">
						“Видео хорошо работает при подключении через wi-fi. В сотовых сетях все зависит от качества Интернета у оператора...”
					</div>
			</div><!-- .reviews_item -->
<!--		<div class="reviews_item">
				<div class="reviews_item_photo">
					<img src="<?php //echo Yii::app()->theme->baseUrl; ?>/img/reviews_item_photo2.png" alt="">
				</div>
				<div class="reviews_item_info">
					<div>Артур Сотников</div>
					<div>Заказчик этой страницы</div>
					<div>24ip.kz</div>
				</div>
				<div class="clear"></div>
				<div class="reviews_item_content">
					“Очень перспективный и интересный проект, который появился относительно недавно...”
				</div>
			</div> <!-- .reviews_item -->
			
			
		<?php echo CHtml::link(Yii::t('main','Смотреть все отзывы→'), array('/reviews'),array('class' => 'link_green reviews_all')) ?>
	</div><!-- .block_reviews_right -->
</div><!-- .block_reviews -->