<?php
class ReviewWidget extends CWidget
{
	public $page = '_review_item';
	public $wordCount = 25;
	
	public function run()
	{
		$dataProvider = new CActiveDataProvider('Review', array(
				'criteria'=>array(
						'condition'=>'status='.Review::STATUS_ACTIVE,
						'order'=>'create_time DESC',
						'limit'=>2,
				),
				'pagination'=>false,
		));
		
		$this->render('reviews', array(
				'dataProvider'=>$dataProvider,
		));
	}
}
?>