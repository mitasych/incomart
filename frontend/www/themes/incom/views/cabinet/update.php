<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = 'Update Profile';
$this->breadcrumbs = array(
	'Update Profile',
);
?>
<h1>Update Profile</h1>

<p>Please fill out the following form with your login credentials:</p>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'update-form',
// 	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	'htmlOptions'=>array(
			'enctype' => 'multipart/form-data',
	),
)); ?>

	<div class="avatar">
		<?php $avatar = empty($model->photo) ? 'no-avatar.jpg' : $model->photo; ?>
		<?php echo CHtml::image('/image/uploaded/avatarthumb/'.$avatar)?>
	</div>
	
	<?php echo $form->label($model, 'photo');?>
	<?php echo $form->fileField($model, 'photo'); ?>
	<?php echo $form->error($model, 'photo');?>
	<br/>
	<?php echo $form->label($model, 'first_name');?>
	<?php echo $form->textField($model, 'first_name'); ?>
	<?php echo $form->error($model, 'first_name');?>
	<br/>
	<?php echo $form->label($model, 'last_name');?>
	<?php echo $form->textField($model, 'last_name'); ?>
	<?php echo $form->error($model, 'last_name');?>
	<br/>
	<?php echo $form->label($model, 'email');?>
	<?php echo $form->textField($model, 'email'); ?>
	<?php echo $form->error($model, 'email');?>
	<br/>
	<?php echo $form->label($model, 'city');?>
	<?php echo $form->textField($model, 'city'); ?>
	<?php echo $form->error($model, 'city');?>
	<br/>
	<?php echo $form->label($model, 'icq');?>
	<?php echo $form->textField($model, 'icq'); ?>
	<?php echo $form->error($model, 'icq');?>
	<br/>
	<?php echo $form->label($model, 'skype');?>
	<?php echo $form->textField($model, 'skype'); ?>
	<?php echo $form->error($model, 'skype');?>
	<br/>
	<?php echo $form->label($model, 'about');?>
	<?php echo $form->textArea($model, 'about'); ?>
	<?php echo $form->error($model, 'about');?>
	<br/>
	<div class="actions">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>

<?php $this->endWidget(); ?>