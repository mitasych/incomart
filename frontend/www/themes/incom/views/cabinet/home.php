<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Cabinet','url'=>array('cabinet')),
	array('label'=>'Add Camera','url'=>array('cameraAdd')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('camera-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script type="text/javascript" src="http://178.88.68.174:8080/flu/js/swfobject.js"></script>
<script type="text/javascript">
	
	function getVideo(name, type){

		$('#myModal').css('height', '580px');
		$('#myModal .modal-body').css('height', '490px');
		$('#myModal .modal-body').empty();
		$('#myModal').modal('toggle');
		$('#myModal .modal-body').append('<div id="videoplayer"></div>');

		var server = 'http://178.88.68.174:8080/';
		var postfix = '';
		var player = 'http://178.88.68.174:8080/flu/StrobeMediaPlayback.swf';

		var flashvars = {};

		if(type == 'hls'){
			flashvars = {
					  src : server+name+'/index.m3u8',
					  autoPlay: true
				};
		}
		else if(type == 'hds'){
			flashvars = {
					  src : server+name+'/manifest.f4m',
					  autoPlay: true
				};
		}
		else if(type == 'rtmp'){
			player = 'http://178.88.68.174:8080/flu/jwplayer.swf';
			flashvars = {
					file : name, 
					streamer : 'rtmp://178.88.68.174:1935/static', 
					'rtmp.tunneling' : false, 
					autostart : true
				};
		}
		
		var paramObj = {allowScriptAccess : "always", allowFullScreen : "true", allowNetworking : "all"};
		swfobject.embedSWF(player, "videoplayer", 640, 480, "10.3", false,
		    flashvars, paramObj, {name: "player"});
	}
	
	function getCode(name, type){

		var text = [];
		text['hls'] = '<object type="application/x-shockwave-flash" name="player" '+
					'data="http://178.88.68.174:8080/flu/StrobeMediaPlayback.swf" width="640" height="480" id="videoplayer" '+
					'style="visibility: visible;"><param name="allowScriptAccess" value="always"><param name="allowFullScreen" '+
					'value="true"><param name="allowNetworking" value="all"><param name="flashvars" value="src=http://178.88.68.174:8080/'+
					name+'/index.m3u8&autoPlay=true"></object>';
		text['rtmp'] = '<object type="application/x-shockwave-flash" name="player" data="http://178.88.68.174:8080/flu/jwplayer.swf" '+
					'width="640" height="480" id="videoplayer" style="visibility: visible;"><param name="allowScriptAccess" '+
					'value="always"><param name="allowFullScreen" value="true"><param name="allowNetworking" value="all">'+
					'<param name="flashvars" value="file='+name+
					'&amp;streamer=rtmp://178.88.68.174:1935/static&amp;rtmp.tunneling=false&amp;autostart=true"></object>';

		text['hds'] = '<object type="application/x-shockwave-flash" name="player" '+
						'data="http://178.88.68.174:8080/flu/StrobeMediaPlayback.swf" width="640" height="480" id="videoplayer" '+
						'style="visibility: visible;"><param name="allowScriptAccess" value="always"><param name="allowFullScreen" '+
						'value="true"><param name="allowNetworking" value="all"><param name="flashvars" '+
						'value="src=http://178.88.68.174:8080/'+name+'/manifest.f4m&amp;autoPlay=true"></object>'

		
		if (confirm("Администрация сервиса не несёт ответственности за трансляцию видео с других сайтов")) {
			$('#myModal').css('height', '230px');
			$('#myModal .modal-body').css('height', '130px');
			$('#myModal .modal-body').empty();
			
			$('#myModal').modal('toggle');
			
			$('#myModal .modal-body').append('<textarea style="width: 630px; height: 100px;">'+text[type]+'</textarea>');
		} else {
			return false;
		}
						
	}

 
</script>

<?php if(Yii::app()->user->hasFlash('wait_and_enable_camera')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('wait_and_enable_camera'); ?>
        </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
	        'block'=>true, 
	        'fade'=>true, 
	        'closeText'=>'&times;', 
	        'alerts'=>array( 
	            'error'=>array('block'=>true, 'fade'=>true), 
	        ),
	    )); ?>

<div class="main">
	<div class="content">
		<h2>Собственные камеры</h2>
		
		<?php /// CAMERAS ?>
		<?php $this->widget('bootstrap.widgets.TbGridView',array(
			'id'=>'camera-grid-view',
			'dataProvider'=>$model->search(),
// 			'filter'=>$model,
			'columns'=>array(
// 				'id',
				'name',
// 				'manufacturer_id',
// 				'model_id',
				'active',
				'IP_address',
				array(
						'header'=>Yii::t('main','HTML Code'),
						'type'=>'raw',
						'value'=>'
							$this->grid->controller->widget(\'bootstrap.widgets.TbButtonGroup\', array(
											\'buttons\' => array(
														array(
															\'label\'=>\'HDS\',
														    \'type\'=>\'danger\', 
															\'htmlOptions\'=>array(
																\'title\'=>$data->machine_name,
																\'onclick\'=>\'getCode(this.title, "hds")\',
															)
														),
														array(
															\'label\'=>\'RTMP\',
														    \'type\'=>\'success\', 
															\'htmlOptions\'=>array(
																\'title\'=>$data->machine_name,
																\'onclick\'=>\'getCode(this.title, "rtmp")\',
															)
														),
														array(
															\'label\'=>\'HLS\',
															\'htmlOptions\'=>array(
																\'title\'=>$data->machine_name,
																\'onclick\'=>\'getCode(this.title, "hls")\',
															)
														),
													),
													\'buttonType\'=>\'button\',
													\'size\'=>\'mini\', 
												),true
											);
						
						'
						),
				array(
						'header'=>Yii::t('main','View'),
						'type'=>'raw',
						'value'=>'\'<a href="http://178.88.68.174:8080/\'.$data->machine_name.\'/index.m3u8" data-toggle="modal" data-target="#myModal" onclick="getVideo(this.href);">view</a>\'',
						'value'=>'
							$this->grid->controller->widget(\'bootstrap.widgets.TbButtonGroup\', array(
											\'buttons\' => array(
														array(
															\'label\'=>\'HDS\',
															\'type\'=>\'danger\',
															\'htmlOptions\'=>array(
																	\'title\'=>$data->machine_name,
																	\'onclick\'=>\'getVideo(this.title, "hds")\',
																)
														),
														array(
															\'label\'=>\'RTMP\',
															\'type\'=>\'success\',
															\'htmlOptions\'=>array(
																	\'title\'=>$data->machine_name,
																	\'onclick\'=>\'getVideo(this.title, "rtmp")\',
																)
														),
														array(
															\'label\'=>\'HLS\',
															\'htmlOptions\'=>array(
																	\'title\'=>$data->machine_name,
																	\'onclick\'=>\'getVideo(this.title, "hls")\',
																)
														),
													),
													\'buttonType\'=>\'button\',
													\'size\'=>\'mini\',
												),true
											);
						
						'
						),
				array(
						'header'=>Yii::t('main','Archive'),
						'type'=>'raw',
						//'value'=>'\'<a href="http://178.88.68.174:8080/\'.$data->machine_name.\'/index.m3u8" data-toggle="modal" data-target="#myModal" id="\'.$data->machine_name.\'" onclick="getArchive(this.id);">Просмотр</a>\''
						'value'=>'\'<a href="/cabinet/archive/\'.$data->machine_name.\'">Просмотр</a>\''
						),
// 				'login',
				/*
				'password',
				'port',
				'url',
				'user_id',
				'create_time',
				
				
				array(
					'class'=>'bootstrap.widgets.TbButtonColumn',
				),
				*/
				
			),
		)); ?>
		
		<h2>Доступные камеры</h2>
		
		<?php 
			$this->widget('bootstrap.widgets.TbGridView',array(
				'id'=>'camera-available-grid-view',
				'dataProvider'=>$avCameras,
				'columns'=>array(
					'name',
					'active',
					'IP_address',
					array(
							'header'=>Yii::t('main','HTML Code'),
							'type'=>'raw',
							'value'=>'
							$this->grid->controller->widget(\'bootstrap.widgets.TbButtonGroup\', array(
								\'buttons\' => array(
								array(
										\'label\'=>\'HDS\',
										\'type\'=>\'danger\',
										\'htmlOptions\'=>array(
												\'title\'=>$data->machine_name,
												\'onclick\'=>\'getCode(this.title, "hds")\',
										)
								),
								array(
										\'label\'=>\'RTMP\',
										\'type\'=>\'success\',
										\'htmlOptions\'=>array(
												\'title\'=>$data->machine_name,
												\'onclick\'=>\'getCode(this.title, "rtmp")\',
										)
								),
								array(
										\'label\'=>\'HLS\',
										\'htmlOptions\'=>array(
												\'title\'=>$data->machine_name,
												\'onclick\'=>\'getCode(this.title, "hls")\',
										)
								),
						),
								\'buttonType\'=>\'button\',
								\'size\'=>\'mini\',
					),true
					);
					
							'
					),
					array(
							'header'=>Yii::t('main','View'),
							'type'=>'raw',
							'value'=>'\'<a href="http://178.88.68.174:8080/\'.$data->machine_name.\'/index.m3u8" data-toggle="modal" data-target="#myModal" onclick="getVideo(this.href);">view</a>\'',
							'value'=>'
							$this->grid->controller->widget(\'bootstrap.widgets.TbButtonGroup\', array(
									\'buttons\' => array(
											array(
													\'label\'=>\'HDS\',
													\'type\'=>\'danger\',
													\'htmlOptions\'=>array(
															\'title\'=>$data->machine_name,
															\'onclick\'=>\'getVideo(this.title, "hds")\',
													)
											),
											array(
													\'label\'=>\'RTMP\',
													\'type\'=>\'success\',
													\'htmlOptions\'=>array(
															\'title\'=>$data->machine_name,
															\'onclick\'=>\'getVideo(this.title, "rtmp")\',
													)
											),
													array(
													\'label\'=>\'HLS\',
													\'htmlOptions\'=>array(
															\'title\'=>$data->machine_name,
															\'onclick\'=>\'getVideo(this.title, "hls")\',
													)
											),
									),
									\'buttonType\'=>\'button\',
									\'size\'=>\'mini\',
							),true
					);
					
							'
					),
					array(
						'header'=>Yii::t('main','Archive'),
						'type'=>'raw',
// 						'value'=>'\'<a href="http://178.88.68.174:8080/\'.$data->machine_name.\'/index.m3u8" data-toggle="modal" data-target="#myModal" id="\'.$data->machine_name.\'" onclick="getArchive(this.id);">Просмотр</a>\''
						'value'=>'\'<a href="/cabinet/archive/\'.$data->machine_name.\'">Просмотр</a>\''
						),
				),
			)); 
		?>
		
		<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'myModal', 'htmlOptions'=>array('style'=>'width: 670px; height: 580px'))); ?>
 
		<div class="modal-header">
		    <a class="close" data-dismiss="modal">&times;</a>
		    <h4><?php echo Yii::t('main','Your Camera');?></h4>
		</div>
		 
		<div class="modal-body" style="width: 640px; height: 490px; max-height: 490px">
		</div>
		 
		<?php $this->endWidget(); ?>
		
		
		
		<?php /// END CAMERAS ?>
		<div class="alert alert-info">
		  	Вам осталось только подключить к VIDOnline свои камеры наблюдения. Это займет всего пару минут.
		</div>
		<p>Остались вопросы? Загляните в наш <a href="#" target="_blank">Центр Помощи</a>!<br>И не забудьте попробовать продвинутые функции VIDOnline. Например…</p>
		<div class="b-feature-list">
			<div class="b-feature">
		  		<i class="icon-globe"></i>
		  		<span class="b-text">Хранение архива в&nbsp;облаке VIDOnline</span>
		  	</div>
		  	<div class="b-feature">
		  		<i class="icon-wrench"></i>
		  		<span class="b-text">Передачу прав на камеры</span>
		  	</div>
	  	</div>
	  	
	  	
	  	<!-- *************************************************************************** -->
	  	<hr />
	  	<!-- *************************************************************************** -->
	  	
	  	<div class="alert alert-block">
			<?php 
			$this->widget('bootstrap.widgets.TbButton', array(
					'buttonType' => 'link',
					'url'=>'/user/changeTarif',
					'type'=>'primary',
					'htmlOptions' => array('style'=>'float:right; margin:20px 50px 0 0; text-decoration: none;'),
					'label'=>'Сменить тарифный план'/* Yii::t('main','Search') */,
				)); 
			?>
			<div class="clear"></div>
		</div>
		<div class="alert alert-info">
		  	Хотите получить архивные видеозаписи с камер и сохранить их на своем компьютере, 
		  	отправить друзьям или даже загрузить их на YouTube?
		</div>
		<dl class="b-help-actions">
			<dt>1</dt>
			<dd>
				<p>Подключите вашу камеру</a><br>
			<dt>2</dt>
			<dd>
				<p>Включите архивирование вашего видео<br>
			</dd>
			<dt>3</dt>
			<dd>
				<p>Сохраните созданный архив на вашем компьютере<br>
				<?php 				
				$this->widget('bootstrap.widgets.TbButton', array(
						'buttonType' => 'link',
						'url'=>'/user/cameraAdd',
						'type'=>'primary',
						'htmlOptions' => array('style'=>'margin-top:20px; text-decoration: none;'),
						'label'=>'Добавить камеру'/* Yii::t('main','Search') */,
					)); ?>
			</dd>
	  	</dl>
	</div><!-- .content -->
</div>
				
<script type="text/javascript">

	var isMouseDown = false;
	
	$('.modal-body').mousedown(function(event){
	    isMouseDown = true;
	    if(!$(event.target).is('#view_arch') && !$(event.target).is('#download_arch'))
	    {
	    	$('.data_minute').each(function(){
				$(this).removeClass('to-show');
			});
	    }
	    
	});
	$('.modal-body').mouseup(function(){
	    isMouseDown = false;
	});

	function checkDiv(id){
		
		if(isMouseDown==true){
			console.log(id);
			//console.log(isMouseDown);
			var lastToshowInd = $('.data_minute.to-show').index();
			var thisInd = $('#'+id.toString()).index()
			if (lastToshowInd < thisInd){
				$('.data_minute:gt('+lastToshowInd+'):lt('+thisInd+')').each(function(){
					$(this).addClass('to-show');
				});
			}
			else if(lastToshowInd > thisInd){
				$('.data_minute:lt('+lastToshowInd+'):gt('+thisInd+')').each(function(){
					$(this).addClass('to-show');
				});
			}
			$('#'+id.toString()).addClass('to-show');
		}
	}

	function getArchive(name){

		//$('#myModal').css('height', '580px');
		//$('#myModal .modal-body').css('height', '490px');
		$('#myModal .modal-body').empty();

		$.ajax({
			'url':'/<?php echo $this->id?>/archive/'+name,
			'cache':false,
			'success':function(html){
				$("#myModal .modal-body").html(html);
			}
		});
		
	}

	function runArchive(name){
		var start = $('.to-show:first').attr('id');
		var length = $('.to-show:last').attr('id') - start +60;

		console.log(start);
		console.log(length);

	//	var url = 'http://178.88.68.174:8080/'+name+'/archive/'+start+'/'+length+'/index.m3u8';
		var url = 'http://178.88.68.174:8080/'+name+'/archive/'+start+'/'+length+'/manifest.f4m';

		console.log(url);
		
		$('#myModal').css('height', '580px');
		$('#myModal .modal-body').css('height', '490px');
		$('#myModal .modal-body').empty();
		$('#myModal .modal-body').append('<div id="videoplayer"></div>')
		
		var flashvars = {
				  src : url,
				  autoPlay: true
				 };
		var paramObj = {allowScriptAccess : "always", allowFullScreen : "true", allowNetworking : "all"};
		swfobject.embedSWF("http://178.88.68.174:8080/flu/StrobeMediaPlayback.swf", "videoplayer", 640, 480, "10.3", false,
		    flashvars, paramObj, {name: "player"});
	}
	
	function loadArchive(name){
		var start = $('.to-show:first').attr('id');
		var length = $('.to-show:last').attr('id') - start +60;

		console.log(start);
		console.log(length);

		var url = 'http://178.88.68.174:8080/'+name+'/archive-'+start+'-'+length+'.mp4';

		console.log(url);

		window.location = url;
		
	}
</script>
