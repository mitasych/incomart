<div id="data_container" style="width: 640px; display: table;">

<?php foreach($data as $minute): ?>
	
	<div class="data_minute" id="<?php echo $minute['timestamp']?>" onmousemove="checkDiv(this.id);"><?php echo date('H:i', $minute['timestamp']); ?></div>

<?php endforeach;?>
</div>

<?php echo CHtml::button(Yii::t('main','View archive'), array('id'=>'view_arch', 'onclick' => 'runArchive("'.$name.'");')); ?>
<?php echo CHtml::button(Yii::t('main','Download archive'), array('id'=>'download_arch', 'onclick' => 'loadArchive("'.$name.'");')); ?>