<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Cabinet','url'=>array('cabinet')),
	array('label'=>'Add Camera','url'=>array('cameraAdd')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('camera-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="main">
	<div class="content">
	  	
	  	<div class="alert alert-block">
	  		<strong>Для ваших камер услуга доступна на другом тарифном плане</strong>
	  		<!-- <div class="clear"></div> -->
	  		<br /><br />
	  		<p>Услуга «Экспорт архива» недоступна для ваших камер на тарифном плане «Бесплатный». <br>
	  		Чтобы воспользоваться этой услугой, смените ваш тарифный план, например, на «Домашний»</p>
			<?php 
			$this->widget('bootstrap.widgets.TbButton', array(
					'buttonType' => 'submit',
					'type'=>'primary',
					'htmlOptions' => array('style'=>'float:right; margin:20px 50px 0 0;'),
					'label'=>'Сменить тарифный план'/* Yii::t('main','Search') */,
				)); 
			?>
			<div class="clear"></div>
		</div>
		<div class="alert alert-info">
		  	Хотите получить архивные видеозаписи с камер и сохранить их на своем компьютере, 
		  	отправить друзьям или даже загрузить их на YouTube?
		</div>
		<dl class="b-help-actions">
			<dt>1</dt>
			<dd>
				<p>Загрузите программу <a href="#" target="_blank">Incomart</a><br>
				<small>Incomart доступен для Windows, Mac OS X и Linux.</small></p></dd>
			<dt>2</dt>
			<dd>
				<p>Установите Incomart Server на компьютер<br>
				<small>Просто запустите загруженный файл и следуйте инструкциям мастера установки.</small></p>
			</dd>
			<dt>3</dt>
			<dd>
				<p>Нажмите на кнопку «Обновить»<br>
				<small>…и ваши камеры появятся на этой странице.</small></p>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType' => 'submit',
					'type'=>'primary',
					'htmlOptions' => array('style'=>'margin-top:20px;'),
					'label'=>'Обновить'/* Yii::t('main','Search') */,
				)); ?>
			</dd>
	  	</dl>
	</div><!-- .content -->
</div>
