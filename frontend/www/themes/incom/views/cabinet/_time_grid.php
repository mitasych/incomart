<style>
	table#time-grid-table td, table#time-grid-table th {
		min-width: 18px;
		height: 30px;
		padding: 1px;
		text-align: center;
	}
	.td-empty{
		background-color: red;
	}
	.td-fully{
		background-color: green;
	}
	.div-minute{
		width: 100%;
		height: 3px;
	}
</style>
<table id="time-grid-table">

<?php for($j = 0; $j <= 60; $j+=10):?>
	<tr>
	<?php for($i = 0; $i <= 24 ; $i++):?>
		<?php if($i === 0 && $j > 0):?>
			<th><?php echo ($j-10).'-'.$j; ?></th>
		<?php elseif($i > 0 && $j === 0): ?>  
			<th><?php echo $i-1; ?></th>
		<?php elseif($i === 0 && $j === 0): ?>
			<td></td>
		<?php else: ?>
			<td id="<?php echo $i;?>_<?php echo $j;?>">
				<?php for($x = $j-10; $x < $j; $x++):?>
					<?php 
						$xx = $x;
						$ii = $i;
						
						if(strlen($xx)<2){
							$xx = '0'.$xx;
						}
						if(strlen($ii)<2){
							$ii = '0'.$ii;
						}
					?>
					<div id="<?php echo $ii;?>_<?php echo $xx;?>"  class="div-minute <?php echo isset($data[$ii.'_'.$xx]) ? 'td-fully' : 'td-empty'?>"></div>
				<?php endfor;?>
			</td>
		<?php endif; ?>
	<?php endfor; ?>
	</tr>
<?php endfor; ?>

</table>