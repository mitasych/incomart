<div class="main">
	<div class="content">

	<?php 
// 		$this->widget(
// 		    'bootstrap.widgets.TbBox',
// 		    array(
// 		        'title' => 'Воспроизведение',
// 		        'headerIcon' => 'icon-film',
// 		        'content' => '<h2>Выберите дату и время</h2>',
// 		    	'htmlOptions' => array('class' => 'archive_view')
// 		    )
// 		);
	?>
	
	<?php $box = $this->beginWidget(
	    'bootstrap.widgets.TbBox',
	    array(
	    	'id' => 'play-box',
	        'title' => 'Воспроизведение',
	        'headerIcon' => 'icon-film',
	        'htmlOptions' => array('class' => 'archive_view')
	    )
	);?>
		<h2 style="margin:0 0 16px 0;">Выберите дату и время</h2>
		<div id="_time_grid"></div>
		<?php // $this->renderPartial('_time_grid');?>
	<?php $this->endWidget(); ?>
	
	<?php $box = $this->beginWidget(
	    'bootstrap.widgets.TbBox',
	    array(
	        'title' => 'Календарь',
	        'headerIcon' => 'icon-calendar',
	        'htmlOptions' => array('class' => 'select_archive_period')
	    )
	);?>
	
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'archive-form',
			'enableAjaxValidation'=>true,
			'htmlOptions'=>array(
					'style'=>'margin:0; padding: 0;'
				),
		)); ?>
		
			<?php echo $form->hiddenField($model,'name'); ?>
		
			<?php
				$this->widget('zii.widgets.jui.CJuiDatePicker',array(
					'model'=>$model, //Model object
					'attribute'=>'date',
					//'name' => 'date_view',
					'flat' => true,
					'htmlOptions'=>array(
						//'value'=>$model->public_time = $model->isNewRecord ? date('Y-m-d H:i:s') : $model->public_time,
						'class'=>'unit-fluid',
					),
					'options'=>array(
						"dateFormat"=>'yy-mm-dd',
						'onSelect'=>'js: function(selectedDate) {
										var name = "'.$model->name.'";
										getVideoArchiveTime(name, $(this).val());
										console.log($(this).val());
										$("#ArchiveForm_date").val(selectedDate);
								}'
					), 
					'language' => 'ru',
					) 
				);
			?>
			
			<div class="">
				<?php echo $form->dropDownList($model,'startHour', ArchiveForm::listHours(), array('options'=>array((date('H')-1)=>array('selected'=>true)), 'class'=>'col_52')); ?>
				:
				<?php echo $form->dropDownList($model,'startMinute', ArchiveForm::listMinutes(), array('class'=>'col_52')); ?>
				-
				<?php echo $form->dropDownList($model,'endHour', ArchiveForm::listHours(), array('options'=>array((date('H')-0)=>array('selected'=>true)), 'class'=>'col_52')); ?>
				:
				<?php echo $form->dropDownList($model,'endMinute', ArchiveForm::listMinutes(), array('class'=>'col_52')); ?>
				
			</div>
			
			<div class="end-archive-form">
				получить файл 
				<?php echo $form->checkBox($model, 'getFile');?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
				    'label'=>'Готово',
				    'buttonType'=>'button',
				    'type'=>'info', 
				    'size'=>'large', 
					'htmlOptions'=>array('onclick'=>'getVideoArchive($(this));'),
				)); ?>
			</div>
		<?php $this->endWidget(); ?>
		
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				    'label'=>'Сброс',
				    'buttonType'=>'button',
				    'type'=>'warning', 
				    'size'=>'mini', 
					'htmlOptions'=>array('onclick'=>'getVideoArchiveTime("'.$model->name.'", "'.date('Y-m-d').'");'),
				)); ?>
	
	<?php $this->endWidget(); ?>

	</div>

</div>

<script type="text/javascript">

	$(document).ready(function(){
		console.log($('#ArchiveForm_date').val());
		getVideoArchiveTime('<?php echo $model->name; ?>', $('#ArchiveForm_date').val());

		
	});
				
	function getVideoArchive(el){
		
		$.ajax({
			'type':'POST',
			//'dataType':'json',
			'url':'<?php echo CController::createUrl('/cabinet/ajaxArchive'); ?>',
			'data':el.parents('form').serialize(),
			'success':function(html)
				{
					$(".archive_view .bootstrap-widget-content").html(html);
				},
			'error':function(xhr, ajaxOptions, thrownError){
					console.log(thrownError);
					console.log(xhr.responseText);
				}
		});
	}
	
	function getVideoArchiveTime(name, date){
		$(".archive_view #_time_grid").css('height', '225px');
		$(".archive_view #_time_grid").html('<div id="loadingDiv"></div>');
		$.ajax({
			'type':'GET',
			//'dataType':'json',
			'url':'<?php echo CController::createUrl('/cabinet/archiveTime'); ?>',
			'data':'name='+name+'&date='+date,
			'success':function(html)
				{
					$(".archive_view #_time_grid").html(html);
				},
			'error':function(xhr, ajaxOptions, thrownError){
					console.log(thrownError);
					console.log(xhr.responseText);
				}
		});
	}

	
	
</script>

<script>
	$('.ui-datepicker-current-day').click(function(){
		alert('xxx');
		console.log($('#ArchiveForm_date').val());
	});
</script>