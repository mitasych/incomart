<?php
$this->breadcrumbs=array(
	'Cameras'=>array('admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Camera'),'url'=>array('user/cameraAdd')),
	array('label'=>Yii::t('main','Update Camera'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Camera'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Cameras'),'url'=>array('/cabinet')),
);
?>

<h1><?php echo Yii::t('main','View Camera: "'); echo $model->name.'"'; ?></h1>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
// 		array(
// 				'name'=>'manufacturer_id',
// 				'value'=>$model->manufacturer->name
// 		),
// 		array(
// 				'name'=>'model_id',
// 				'value'=>$model->model->name
// 		),
		array(
				'name'=>'frame_rate_id',
				'value'=>$model->frameRate->frame_rate
		),
		array(
				'name'=>'resolution_id',
				'value'=>$model->resolution->name
		),
		'IP_address',
		'login',
		'password',
		'port',
		'url',
		'user_id',
		'create_time',
		array(
				'label'=>Yii::t('labels','Allowed Users'),
				'value'=>$model->allowedUsers,
			)
	),
)); ?>
<script type="text/javascript" src="http://178.88.68.174:8080/flu/js/swfobject.js"></script>
<div id="videoplayer" style="width: 640px; height: 480px"></div>
<script type="text/javascript">
 var flashvars = {
		  src : "http://178.88.68.174:8080/<?php echo $model->name; ?>/index.m3u8",
		  autoPlay: true
		 };
var paramObj = {allowScriptAccess : "always", allowFullScreen : "true", allowNetworking : "all"};
swfobject.embedSWF("http://178.88.68.174:8080/flu/StrobeMediaPlayback.swf", "videoplayer", 640, 480, "10.3", false,
    flashvars, paramObj, {name: "player"});
</script>


