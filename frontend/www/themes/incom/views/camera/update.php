<?php
$this->breadcrumbs=array(
	Yii::t('main','Cameras')=>array('admin'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Camera'),'url'=>array('user/cameraAdd')),
	array('label'=>Yii::t('main','View Camera'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Cameras'),'url'=>array('/cabinet')),
);
?>

<h1><?php echo Yii::t('main','Update Camera: "'); echo $model->name.'"'; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model, 'bitrates' => $bitrates, 'tarif'=>$tarif)); ?>

<div class="clear"></div>

<style>
.is_us_name, .is_us_endtime{
	float: left;
	padding: 2px 5px;
}

.is_us_name{width: 100px}
.is_us_endtime{width: 144px}

#isUs_titles{
	font-weight: bold;
}

.isset_user{
	background-color: #f5f5f5;
	border-bottom: 1px solid #e5e5e5;
	padding: 2px 0;
	width: 300px;
}

.new-user-form{
	padding: 5px 10px 0;
	border: 1px solid #e5e5e5;
	background-color: #f5f5f5;
	width: 216px;
	margin: 10px 0;
}

#addAllowUser{
	margin: 10px 0;
}

</style>

<h4>Пользователи с доступом</h4>

<div class="isset-allowed-users">
			<div class="isset_user" id="isUs_titles">
				<div class="is_us_name">Имя</div>
				<div class="is_us_endtime">Окончание доступа</div>
				<div class="is_us_delete">&nbsp;</div>
			</div>
			<div class="clear"></div>
	<?php if(!empty($model->users)): ?>
		<?php foreach ($model->users as $user): ?>
			<div class="isset_user" id="isUs_<?php echo $user->id_user; ?>">
				<div class="is_us_name"><?php echo $user->user->username; ?></div>
				<div class="is_us_endtime"><?php echo $user->end_time; ?></div>
				<div class="is_us_delete">
				<?php 
					$this->widget('bootstrap.widgets.TbButton', array(
						'buttonType'=>'button',
						'type'=>'danger',
						'size'=>'mini',
						'icon'=>'remove white',
						'label'=>'',
						'htmlOptions'=>array('class'=>'remAllowUse', 'onClick'=>'removeAlUs('.$user->id_user.')'),
					)); 
				?>
				</div>
			</div>
			<div class="clear"></div>
		<?php endforeach;?>
	<?php endif; ?>
	
</div>

<div class="clear"></div>

<?php $this->widget('bootstrap.widgets.TbButton', array(
	'buttonType'=>'button',
	'type'=>'success',
	'label'=>'Добавить пользователя',
	'htmlOptions'=>array('id'=>'addAllowUser'),
)); ?>
<div class="clear"></div>

<div class="allowed-users">
</div>

<div class="clear"></div>

<script type="text/javascript">
	$('#addAllowUser').click(function(){
		var div_ind = $('.new-user-form').length+1;
		//if($('.new-user-form').length>0){
			$('.allowed-users').append('<div class="new-user-form" id="index_'+div_ind+'"></div>')
		//}
		
		$.ajax({
			'type':'GET',
			'dataType':'json',
			'url':'<?php echo CController::createUrl('CaUsAl/addAllowedUser'); ?>',
			'data':{id_camera : <?php echo $model->id?>, 
				},
			'success':function(json)
			{
				$(".new-user-form#index_"+div_ind).html(json.html);
				$(".new-user-form#index_"+div_ind+" #CameraUserAllowed_username").attr('id', 'CameraUserAllowed_username'+div_ind);
				
			},
		});
	});

	function removeAlUs(id){
		$.ajax({
			'type':'GET',
			//'dataType':'json',
			'url':'<?php echo CController::createUrl('CaUsAl/rmAllowedUser'); ?>',
			'data':{id_camera : <?php echo $model->id?>, 
					id_user : id,
				},
			'success':function(html)
			{
				$("#isUs_"+id).remove();
	 		},
		});
	}

	$('.addAlUs').live('click', function(){

		var parentDiv = $(this).parents('.new-user-form');
		
		$.ajax({
			'type':'POST',
			'dataType':'json',
			'url':'<?php echo CController::createUrl('/CaUsAl/addAllowedUser'); ?>',
			'data':$(this).parents('form').serialize(),
			'success':function(json)
				{
					if(json.error == 1){
						parentDiv.html(json.html);
					}
					else{
						$(".isset-allowed-users").append(json.html);
						parentDiv.remove();
					}
				},
			'error':function(xhr, ajaxOptions, thrownError){
					console.log(thrownError);
					console.log(xhr.responseText);
				}
		});
		
		//console.log($(this).parents('form').serialize());
	});

</script>