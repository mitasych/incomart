<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'camera-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('main','Fields with ') ?><span class="required">*</span>
	<?php echo Yii::t('main',' are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>256)); ?>

	<?php // echo $form->textFieldRow($model,'manufacturer_id',array('class'=>'span5')); ?>
		
	<?php 
		echo $form->dropDownListRow(
			$model,
			'frame_rate_id', 
			CameraFrameRate::listItems(),
			array(
// 				'empty'=>'Select',
				'onchange'=>'getCost();'
			)
		); 
	?>

	<?php 
		echo $form->dropDownListRow(
			$model,
			'resolution_id', 
			CameraResolution::listItems(),
			array(
// 				'empty'=>'Select',
				'onchange'=>'getCost();'
			)
		); 
	?>
	
	<?php 
		echo $form->dropDownListRow(
			$model,
			'bitrate_id', 
			VideoBitrate::listItems(),
			array(
// 				'empty'=>'Select',
				'onchange'=>'getCost();'
			)
		); 
	?>
	
	<?php // echo CHtml::dropDownList('bitrate', '', $bitrates)?>
	
	<?php  echo $form->textFieldRow($model,'cost_per_day',array('class'=>'span1','maxlength'=>45, 'readonly'=>'readonly')); ?>

	<?php // echo $form->textFieldRow($model,'model_id',array('class'=>'span5')); ?>
	
	<?php // echo $form->textFieldRow($model,'disk_space_per_day',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->hiddenField($model,'disk_space_per_day',array('class'=>'span5','maxlength'=>45)); ?>
	
	<?php echo CHtml::hiddenField('per_min_kb', '', array('id'=>'per_min_kb')); ?>
	
	<?php echo $form->toggleButtonRow($model, 'archive', array('onchange'=>'toggleDays($(this).attr("checked"));getCost();')); ?>
	
	<?php 
		$days_display = ' display: none; ';
		if($model->archive == 1){
			$days_display = '';
		}
	
	?>
	
	<div id="wrapper_days_number" style="<?php echo $days_display; ?>">
	<?php 
		$days_number = array();
		foreach (range(1, $tarif->archive_term) as $num){
			$days_number[$num] = $num;
		}
	?>
		<?php 
			echo $form->dropDownListRow(
				$model,
				'archive_days_number', 
				$days_number,
				array(
// 					'empty'=>'Select',
 					'onchange'=>'getCost();'
				)
			); 
		?>
	
		<?php echo $form->textFieldRow($model,'disk_space',array('readonly'=>'readonly')); ?>
	
	</div>
	
	
	<?php echo CHtml::label('Стоимость всего', 'cost_all'); ?>
	<?php  echo CHtml::textField('cost_all', '', array('id'=>'cost_all', 'readonly'=>'readonly')); ?>
	
	
	<?php echo $form->textFieldRow($model,'IP_address',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'login',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'port',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'url',array('class'=>'span5','maxlength'=>256)); ?>
	
	<?php // if (!$model->isNewRecord):?>
		<?php // echo $form->textFieldRow($model,'allowedUsers',array('class'=>'span5')); ?>
	<?php // endif; ?>

	<?php // echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>
	
	<?php echo $form->toggleButtonRow($model, 'active'); ?>
	
	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? Yii::t('main','Create') : Yii::t('main','Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	function toggleDays(val){
		if(val==undefined){
			$('#Camera_archive_days_number').val('');
			$('#Camera_disk_space').val('');
			$('#wrapper_days_number').hide();
		}
		else if(val=="checked"){
			$('#wrapper_days_number').show();
		}
	}

	function getCost(){

		$.ajax({
			'type':'POST',
			'dataType':'json',
			'url':'<?php echo CController::createUrl('camera/calcMinuteSpace'); ?>',
			'data':{frame_rate_id : $("#Camera_frame_rate_id").val(), 
					resolution_id : $("#Camera_resolution_id").val(),
					bitrate_id : $("#Camera_bitrate_id").val(),
					archive : $("#Camera_archive").attr('checked') == 'checked' ? 1 : 0,
					archive_days : $("#Camera_archive_days_number").val()
				},
			'success':function(data)
			{
				//console.log(data);
				$("#disk_space_per_day").val(data.space_mb);
				$("#Camera_disk_space").val(data.space_mb_all);
				$("#per_min_kb").val(data.space_kb);
				$("#Camera_cost_per_day").val(data.cost_per_day);
				$("#cost_all").val(data.cost_all);
			},
		});
		
	}

	$(document).ready(function(){
		getCost();
	});

</script>