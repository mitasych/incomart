<?php
/**
 * index.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 8:30 PM
 */
?>
    <div class="block_slider">
    <?php 
		$this->widget('bootstrap.widgets.TbCarouselTab', array(
				'displayPrevAndNext' => false,
				'tabs'=>array(
						array(),
						array(),
						array(),
						array(),
						array(),
						array(),
						),
				'items'=>array(
						array('image'=>Yii::app()->theme->baseUrl.'/img/slides/1.jpg', 
								//'label'=>Yii::app()->theme->baseUrl, 
								'label'=>'Ваше имущество всегда под присмотром',
								'caption'=>' ',
// 								'caption'=>'Ваше имущество всегда под присмотром',
								'captionOptions'=>array('class'=>'item_caption', 'style'=>'top: 120px'),
								),
						array('image'=>Yii::app()->theme->baseUrl.'/img/slides/2.jpg', 
								//'label'=>Yii::app()->theme->baseUrl, 
								'label'=>'Контролируй бизнес в любой точке планеты',
								'caption'=>' ',
								'captionOptions'=>array('class'=>'item_caption'),
								),
						array('image'=>Yii::app()->theme->baseUrl.'/img/slides/3.jpg', 
								//'label'=>Yii::app()->theme->baseUrl, 
								'label'=>'Вы знаете, что делают ваши дети когда вас нет рядом',
								'caption'=>' ',
								'captionOptions'=>array('class'=>'item_caption', 'style'=>'top: 180px'),
								),
						array('image'=>Yii::app()->theme->baseUrl.'/img/slides/4.jpg', 
								//'label'=>Yii::app()->theme->baseUrl, 
								'label'=>'Удаленная забота о пожилых родителях',
								'caption'=>' ',
								'captionOptions'=>array('class'=>'item_caption', 'style'=>'top: 200px'),
								),
						array('image'=>Yii::app()->theme->baseUrl.'/img/slides/5.jpg', 
								//'label'=>Yii::app()->theme->baseUrl, 
								'label'=>'Не пропустите этапы развития вашего ребенка',
								'caption'=>' ',
								'captionOptions'=>array('class'=>'item_caption'),
								),
						array('image'=>Yii::app()->theme->baseUrl.'/img/slides/6.jpg', 
								//'label'=>Yii::app()->theme->baseUrl, 
								'label'=>'Организация дистанционного обучения',
								'caption'=>' ',
								'captionOptions'=>array('class'=>'item_caption'),
								),
						),
				'htmlOptions'=>array('id'=>'frontSlider'),
		));
                        	
	?>
    </div>
    <div class="slogan">
    	<span>
    	Получите доступ ко всем своим камерам наблюдения,<br />
		даже если они находятся на разных континентах.
        </span>
    </div>
    
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Имущество', 'url'=>array('#'), 'itemOptions'=>array('class'=>'goods active')),
				array('label'=>'Пожилые родители', 'url'=>array('#'), 'itemOptions'=>array('class'=>'parents')),
				array('label'=>'Бизнес', 'url'=>array('#'), 'itemOptions'=>array('class'=>'business')),
				array('label'=>'Детский сад', 'url'=>array('#'), 'itemOptions'=>array('class'=>'kindergarten')),
				array('label'=>'Образование', 'url'=>array('#'), 'itemOptions'=>array('class'=>'education')),
				array('label'=>'Дети', 'url'=>array('#'), 'itemOptions'=>array('class'=>'children')),
			),
		)); ?>
	</div><!-- mainmenu -->
    <div class="clear" style="height:35px;"></div>
	<div class="block_info">
    	<div class="block_info_left">
        	<div class="block_header">Возможности VIDEOnline</div>
        	<div class="block_info_left_text">
                <p>
				Услуга VIDEOnline предоставляет возможность 
				удаленного онлайн просмотра видео с Ваших камер 
				неограниченному количеству пользователей
                </p>
                <p>
                Для использования услуги достаточно иметь одну
                камеру и доступ в интернет. При наличии нескольких 
                камер Вы имеете возможность подключить
                к нашей услуге одну, несколько или все камеры на
                Ваш выбор.
                </p>
                <p>
                Трансляция видео и запись в архиве ведется онлайн, 
                просмотр видео доступен пользователю в
                любом месте, при условии наличия доступа в
                Интернет.
                </p>
                <!-- <a href="#">Cмотреть видеопрезентацию</a> -->
                <div class="clear" style="height:20px;"></div>
                <div class="block_info_left_video"></div>
            </div>
        </div><!-- .block_info_left -->
        <?php $this->renderPartial('index_tabs/_goods')?>
        <?php $this->renderPartial('index_tabs/_parents')?>
        <?php $this->renderPartial('index_tabs/_business')?>
        <?php $this->renderPartial('index_tabs/_kindergarten')?>
        <?php $this->renderPartial('index_tabs/_education')?>
        <?php $this->renderPartial('index_tabs/_children')?>
    </div><!-- .block_info -->
    <script type="text/javascript">
		$('#mainmenu a').click(function(){
			$('#mainmenu li').each(function(){
				$(this).removeClass('active');
			});
			$('.block_info_right').each(function(){
				$(this).css('display', 'none');
			});
			$('.block_info_right.'+$(this).parent().attr('class')).css('display', 'block');
			$(this).parent().addClass('active');

			return false;
		});
	</script>
    <div class="clear" style="height:35px;"></div>
    <div class="block_help">
        <div class="block_header" style="text-align: center;">Услуга VIDEOnline - как это работает?</div>
        <div class="map">
            <div class="message mes1">
                <div class="message_top">
                    <div>Наши специалисты установят камеры туда, куда Вы хотите, например, домой</div>
                </div>
                <div class="message_bottom"></div>
            </div>
            <div class="message mes2">
                <div class="message_top">
                    <div>... или в офис.</div>
                </div>
                <div class="message_bottom"></div>
            </div>
            <div class="message mes3">
                <div class="message_top">
                    <div>Просматривайте изображение с камер откуда угодно!</div>
                </div>
                <div class="message_bottom"></div>
            </div>
        </div><!-- .map -->
        <div class="slogan2">
            <div class="slogan2_content">
                <div class="slogan2_h">Все очень просто!</div>
                <!-- <div class="slogan2_txt">
                    Наши специалисты установят камеры туда, куда вы хотите, после чего
                    вы сможете смотреть, что происходит перед камерами из любой точки
                    земного шара!
                </div> -->
                <div class="slogan2_txt">Наши специалисты установят камеры в тех местах, где вы пожелаете и при 
                    дополнительном подключении услуги VIDEOnline, Вы будете иметь возможность 
                    смотреть, что происходит перед Вашими камерами из любой точки земного шара!</div>
                <a href="#">Подать заявку</a>
            </div>
        </div>
    </div><!-- .block_help -->
    <div class="clear"></div>
    <div class="block_header" style="text-align: center;">Возможности VIDEOnline</div>
    <div class="block_baners">
    	<div class="baners_item1">
        	<div class="baners_txt">
		        Вы можете просматривать видеоизображения онлайн в любое время, с любой точки, 
		        имеющей доступ в интернет.
        	</div>
        </div>
        <div class="baners_item2">
        	<div class="baners_txt">
		        Возможность раздать закрытый доступ к просмотру видеоизображения родственникам 
		        или друзьям без потери качества видео.
        	</div>
        </div>
        <div class="baners_item3">
        	<div class="baners_txt">
                Своим клиентам мы предлагаем хранение видеоархива на нашем сервере - в случае утери ваших данных 
                вы можете сохранить свой видеоархив на наших серверах.
			</div>
        </div>
        <div class="baners_item4">
        	<div class="baners_txt">
		        Ваша безопасность - наша первостепенная задача. 
		        Мы прикладываем максимальные усилия для обеспечения безопасности передачи данных.
            </div>
        </div>
        <div class="baners_item5">
        	<div class="baners_txt">
		        Для подключения услуги VIDEOnline Вам потребуется только наличие видеокамеры и доступ в Интернет.
            </div>
        </div>
    </div><!-- .block_baners -->
    <div class="block_info2">
        <div class="block_info2_left">
        	<div class="block_info2_left_content">
            	<div class="block_info2_h">Установка видеонаблюдения</div>
                <div class="block_info2_txt">
                    Наши опытные специалисты грамотно подберут необходимое оборудование и интегрируют с различными 
                    охран ными системами безопасности.
                </div>
                <a href="/about">Читать далее →</a>
            </div>
        </div>
        <div class="block_info2_right">
			<div class="block_info2_right_content">
            <div class="block_info2_h">Каталог оборудования</div>
	            <div class="clear"></div>
                <div class="block_info2_txt">
					В разделе «<a href="#">Каталог</a>» мы подобрали для Вас лучшие, сертифицированные и 
					отвечающие всем мировым стандартам модели оборудования для видеонаблюдения с 
					оптимальным соотношением цена/качество.
                </div>
                <div class="clear"></div>
                <div class="message mes2">
                    <div class="message_top">
                        <a href="#">Перейти в каталог</a>
                    </div>
                    <div class="message_bottom"></div>
                </div>
            </div>
        </div>
    </div><!-- .block_info2 -->
    
    <div class="clear"></div>