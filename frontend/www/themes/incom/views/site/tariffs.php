<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]--><head>
	<meta charset="utf-8">
             <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   
    <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
		
        <link rel="stylesheet" href="/themes/incom/css/normalize.css">
        <!-- <link rel="stylesheet" href="/themes/incom/css/main_trf.css"> -->
        <script src="js/vendor/jquery-1.9.1.min.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
	<div class="content">
	    	<div class="block_plans">
	            <!--<div class="block_plans_bg">-->
	            <div class="plans_slogan">
	            	<span>Выбрать план и зарегистрироваться за минуту. Нет скрытых платежей. Отме-<br>нить подключение можно в любое время.</span>
	            </div><!--  .plans_slogan-->
				<div class="plans_item">
					<span>Подключаемых камер</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Защищенный режим</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Локальный видеоархив</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Встраивание видео в сайт</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Облачный видеоархив</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Экспорт архива в файл AVI</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Передача доступа к камере</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Онлайн-трансляция</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Поддержка расширенная</span>
	            </div><!-- .plans_item -->
				<div class="plans_item">
					<span>Оплата на расчетный счет</span>
	            </div><!-- .plans_item -->
	            
	            <div class="tariff_home">
	            	<div class="tariff_home_top"><span>Самый популярный</span></div>
	                <div class="tariff_header">
	                	<span>Домашний</span>
	                    <span><?php echo $model['cost_camera_per_month'][2];?> тг/месяц за камеру</span>
	                </div>
	                <div class="tariff_item">
	                	<span><?php echo $model['number_cameras'][2];?></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['safe_mode'][2];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['local_archive'][2]?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['embed_mode'][2];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['clode_archive'][2];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['AVI_export'][2];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item">
	                	<span>до <?php echo $model['another_users_access'][2];?> пользователей</span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['online_broadcasting'][2];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['extended_support'][2];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['payment_settlement_account'][2];?>">
	                	<span></span>
	                </div>
					<div class="tariff_footer">
					<div class="tariff_footer_price"> <?php echo $model['cost_camera_per_month'][2];?></div>
					<?php echo CHtml::link('Купить', array('/site/signup', 'tariff'=>$model['machine_name'][2]), array('class'=>'tariff_footer_button'))?>
	                </div>
	            </div><!-- .tariff_home -->
	            <div class="tariff_business">
	                <div class="tariff_header">
	                	<span>Бизнес</span>
	                    <span><?php echo $model['cost_camera_per_month'][3];?> тг/месяц за камеру</span>
	                </div>
	                <div class="tariff_item">
	                	<span><?php echo $model['number_cameras'][3];?></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['safe_mode'][3];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['local_archive'][3];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['embed_mode'][3];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['clode_archive'][3];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['AVI_export'][3];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item">
	                	<span>до <?php echo $model['another_users_access'][3];?> пользователей</span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['online_broadcasting'][3];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['extended_support'][3];?>">
	                	<span></span>
	                </div>
	                <div class="tariff_item check_<?php echo $model['payment_settlement_account'][3];?>">
	                	<span></span>
	                </div>
					<div class="tariff_footer">
						<div class="tariff_footer_price"> <?php echo $model['cost_camera_per_month'][3];?></div>
						<?php echo CHtml::link('Купить', array('/site/signup', 'tariff'=>$model['machine_name'][3]), array('class'=>'tariff_footer_button'))?>
	                </div>
	            </div><!--  .tariff_business-->
	        </div><!-- .block_plans -->
			<?php $this->widget('ReviewWidget');?>
	    </div>
    </body>
</html>
