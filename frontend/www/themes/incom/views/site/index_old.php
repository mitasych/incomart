<?php
/**
 * index.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 8:30 PM
 */
?>
<div id="headline">
	<?php if(Yii::app()->user->hasFlash('success')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
	<?php endif; ?>
	<div id="headline-inner">
		<h2 class="graphic home"> Простое подключение. Просматривайте видео где угодно – <!-- <em> -->24/365<!-- </em> -->.</h2>
		<div>
			<?php 
			$this->widget('application.extensions.piecemaker.PieceMaker', array(
			
				'contents'=>array(			
					array(
	// 					'image', 'image/gallery/Fly Agaric Fly Hawaii.jpg', 'Fly Agaric Fly Hawaii',
							'image' /*type*/,
							'image/gallery/Fly Agaric Fly Hawaii.jpg' /*url*/,
							'Fly Agaric Fly Hawaii' /*title*/,
							'<p><em>Photo Source:<em>&nbsp;DeviantArt</p>' . PHP_EOL /*info*/,
	// 						'http://www.yiiframework.com/extension/piecemakerwidget/' /*link*/,
	// 						'_page' /*link-target*/,
					),
	
					array(
						'image', 
						'image/gallery/Ford Mustang.jpg', 
						'Ford Mustang',
						'<p>Photo Source:&nbsp;DeviantArt</p>'
					),
	
					array(
						'image', 
						'image/gallery/Little Mountain.jpg', 
						'Little Mountain',
						'<p>Photo Source:&nbsp;DeviantArt</p>'
					),
	
				),
	
	// 			'transitions'=>array(
	// 				array(9, 1.2, 'easeInOutBack', 0.1, 300, 30),
	// 				array(15, 3, 'easeInOutElastic', 0.03, 200, 10),
	// 				array(5, 1.3, 'easeInOutCubic', 0.1, 500, 50),
	// 				array(9, 1.25, 'easeInOutBack', 0.1, 900, 5),
	// 			),
				
				'settings'=>array(
					
				),
				
			)); ?>
			<?php if(0):?>
				<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/screenshots.jpg" alt="">
			<?php endif; ?>
		</div>
	</div>
</div>
<div id="signup">
	<div id="signup-inner">
		<div class="signup-conteiner">
			<div class="plans">От 1500 тенге/месяц. <a href="site/tariffs">Посмотреть все тарифы &amp; цены</a></div>
			<div class="signup-buttons">
				<a href="#" class="button2">Бесплатная 30-дневная пробная версия</a>
				&nbsp;
				<a href="#" class="button">Обзорный тур</a>
			</div>
		</div>
	</div>
</div>
<!-- 
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span2">
    </div>
    <div class="span10">
    </div>
  </div>
</div>
-->
<div id="showcase">
	<div id="showcase-inner">
		<div class="box1 fleft">
			<div class="feature" >
				<h3>Ваш бизнес под контролем</h3>
				<p>
					<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bisness.jpg" alt="" width="78" height="58"></a>
					Ваш бизнес всегда под контролем. Наблюдение 24 часа.
				</p>
			</div>
			<div class="feature">
				<h3>Для автомашины</h3>
				<p>
					<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/imagesavt.jpg" alt="" width="78" height="58"></a>
					Обезопасить себя от неприятностей и автовандалов.
				</p>
			</div>
		</div>
		<div class="box1 fleft">
			<div class="feature">
				<h3>Отчеты в личном кабинете</h3>
				<p>
					<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/report.jpg" alt="" width="78" height="58"></a>
					Контроль используемого трафика.
				</p>
			</div>
			<div class="feature" style="margin-right:0;">
				<h3>Архив в облаке</h3>
				<p>
					<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/zip.jpg" alt="" width="78" height="58"></a>
					Храните архивное видео не только на своём компьютере.
				</p>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<div id="main">
	<div class="main_content">
		<p class="lead">
			Получите доступ ко всем своим камерам наблюдения, даже если они находятся на разных континентах.
		</p>
		<div class="leftcol">
			<h3 class="alt">Зачем это?</h3>
			<p>
				Очень важно в любой момент узнать, чем заняты дети, как чувствуют себя пожилые родители, 
				какая ситуация в квартире и на даче во время вашего отсутствия. Зарегистрируйте учетную 
				запись и начните создание собственной системы видеонаблюдения для квартиры и личного пользования.
				Мы предлагаем различные варианты установки. Используйте наиболее подходящий.
			</p>
            <h3 class="alt">Партнерская программа</h3>
			<p>
				У вас есть своя клиентская база, но нет услуги видеонаблюдения? Наша партнерская программа 
				станет вашим преимуществом перед конкурентами.Не откладывайте на завтра то, что можно 
				заработать сегодня. Простая процедура регистрации. 
				<a href="#">Заполнить заявку партнера.</a>
			</p>
			<p>
				<a href="#" class="button2">Бесплатная 30-дневная пробная версия</a>
			</p>
		</div><!-- .leftcol -->
		<div class="rightcol">
			<div class="group">
				<h3 class="alt">Отзывы наших пользователей:</h3>
				<h4>TOO HomeGrup.</h4>
				<p>
					<a href="#">
						<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homegrup.jpg" alt="" class="left">
					</a>
						“Видео хорошо работает при подключении через wi-fi. В сотовых сетях все зависит 
						от качества Интернета у оператора....”
					<br />
					<a href="#">Читать далее</a>
				</p>
				<h4>Максим Галкин</h4>
				<p>
					<a href="#">
						<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/cs-thumb_ngen.jpg" alt="" class="left">
					</a>
						“Очень перспективный и интересный проект, который появился относительно недавно. 
						Про некоторые продукты говорят, что они "просто работают".”
						<br />
						<a href="#">Читать далее
					</a>
				</p>
				<p>
					<a href="#">Читать все отзывы</a>
				</p>

			</div><!-- .group -->
		</div><!-- .rightcol -->
	</div><!-- .main_content -->
	<div class="sidebar">
		<div class="feature">
			<h3>Безопасность</h3>
			<p>
				<a href="#">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/feature_notes.gif" alt="" width="78" height="58">
				</a>
				Все данные, включая видео, передаются в зашифрованном виде.
			</p>
		</div>
		<div class="clearfix"></div>
		<div class="feature">
			<h3>Установка видеонаблюдения</h3>
			<p>
				<a href="#">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/ustanovka.png" alt="" width="78" height="58">
				</a>
				Наши опытные специалисты грамотно подберут необходимое оборудование и интегрируют 
				с различными охранными системами безопасности.
			</p>
		</div>
		<div class="clearfix"></div>
		<div class="feature" style="margin-right:0;">
			<h3>Каталог оборудования</h3>
			<p>
				<a href="#">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/feature_basecamp.gif" alt="" width="78" height="58">
				</a>
				В разделе «Каталог» мы подобрали для Вас лучшие модели оборудования для видеонаблюдения 
				с оптимальным соотношением цена/качество. Вся продукция, представленная в каталоге, 
				высокого качества, сертифицирована и отвечает всем мировым стандартам. 
				<a href="#">Перейти в каталог</a>
			</p>
		</div>
	</div><!-- .sidebar -->
	<div class="clearfix"></div>
</div><!-- .main -->
