<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = Yii::t('main','Login');
$this->breadcrumbs = array(
	Yii::t('main','Login'),
);
?>

<?php if(Yii::app()->user->hasFlash('success')):
        echo Yii::app()->user->getFlash('success'); 
endif; ?>
<div class="contacts_content">
<h1><?php echo Yii::t('main','Login') ?></h1>

<p><?php echo Yii::t('main','Please fill out the following form with your login credentials:')?></p>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'login-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
)); ?>
	<?php echo $form->label($model, 'username');?>
	<?php echo $form->textField($model, 'username'); ?>
	<?php echo $form->error($model, 'username');?>
	<br/>
	<?php echo $form->label($model, 'password');?>
	<?php echo $form->passwordField($model, 'password'); ?>
	<?php echo $form->error($model, 'password');?><br/>
	<?php echo CHtml::link(Yii::t('main','Reset Password'), array('/user/resetpassword'),array()) ?> <br/><br/>
	
	<?php echo $form->checkBox($model, 'rememberMe',array('class' => 'fleft','style'=>'margin-right:10px;')); ?>
	<?php echo $form->label($model, 'rememberMe',array('class' => 'fleft'));?>
	<div class="clear"></div>
	
	<?php if ($model->requireCaptcha): ?>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo CHtml::activeTextField($model, 'verifyCode'); ?>
	<?php endif; ?>
	<div class="actions">
		<?php echo CHtml::submitButton(Yii::t('main','Login')); ?>
	</div>

<?php $this->endWidget(); ?>
<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'top-sidebar')); ?>
	
	<div style="height: 10px"></div>
<?php $this->endWidget();?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'sidebar-two')); ?>
	
	
<?php $this->endWidget();?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'calendar-sidebar')); ?>
	
<?php $this->endWidget();?>
</div>