<div class="block_info_right business" style="display:none;">
        	<span class="block_header">Что вы получаете, подключив услугу VIDEOnline</span>
            <div class="block_info_right_item1">
            	<div class="item_img"></div>
                 Получите качественный сервис для контроля производственного цикла.
            </div>
            <span class="clear"></span>
            <div class="block_info_right_item2">
            	<div class="item_img"></div>
	             Проведите анализ посещаемости вашего торгового зала.
            </div>
            <span class="clear"></span>
            <div class="block_info_right_item3">
            	<div class="item_img"></div>
	             Защитите свои склады от недобросовестных сотрудников
            </div>
            <span class="clear"></span>
            <div class="block_info_right_item4">
            	<div class="item_img"></div>
				Вы в любое время будете иметь доступ к просмотру в реальном времени о происходящем на вашей кухне, 
                в зале для посетителей, на кассе и этим вы получаете возможность полноценно самостоятельно 
                отслеживать и устранять недостатки в работе вашего заведения. Специфика ресторанного бизнеса 
                требует тщательного контроля, что прямо влияет на посещаемость и популярность заведения.           
            </div>
        </div><!-- .block_info_right -->