<div class="block_info_right children" style="display:none;">
        	<span class="block_header">Что вы получаете, подключив услугу VIDEOnline</span>
            <div class="block_info_right_item1">
            	<div class="item_img"></div>
				Наша услуга позволит наблюдать чем занят ребенок, пока вас нет дома, учит ли уроки, не привел ли посторонних людей в дом.
            </div>
            <span class="clear"></span>
            <div class="block_info_right_item2">
            	<div class="item_img"></div>
	             Если в доме есть няня, домработница вы можете наблюдать за безопасностью 
                 Ваших малышей и всегда сможете контролировать работу нанятого домашнего персонала.
            </div>
        </div><!-- .block_info_right -->