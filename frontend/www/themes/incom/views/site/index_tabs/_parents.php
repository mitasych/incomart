<div class="block_info_right parents" style="display:none;">
        	<span class="block_header">Что вы получаете, подключив услугу VIDEOnline</span>
            <div class="block_info_right_item1">
            	<!--<div class="item_img"></div>-->
                 Установив камеры видеонаблюдения и подключив услугу онлайн просмотра, вы решаете две задачи:
            </div>
            <span class="clear"></span>
            <div class="block_info_right_item2">
            	<div class="item_img"></div>
	             - обеспечивается круглосуточное наблюдение за недвижимым имуществом родителей;
            </div>
            <span class="clear"></span>
            <div class="block_info_right_item3">
            	<div class="item_img"></div>
	             - даже если вы не имеете возможность находиться рядом со своими пожилыми родителями, вы удаленно всегда 
                 можете просмотреть происходящее и, в случае необходимости помощи, мгновенно отреагировать. 
                 Ваше спокойствие и безопасность ваших родителей это одна и важнейших жизненных ценностей.
            </div>
        </div><!-- .block_info_right -->