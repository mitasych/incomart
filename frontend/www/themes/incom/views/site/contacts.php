<?php
$this->breadcrumbs=array(
	Yii::t('main','Contacts')
);
?>

<?php if(Yii::app()->user->hasFlash('success')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
<?php endif; ?>
<div class="block_contacts">
	<span class="header1">Наши контакты</span>
	<div class="map_wrapper">
		<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=kyloOFrKPEVhyIpswWvEn25dSsSd3eFx&width=950&height=400"></script>
	</div>
	
	
	<!-- <span style="font-size: small;">mitasych@gmail.com<br><br>тел. +38 (050) 4932803<br></span> -->
	<div class="contacts_content">
        <div class="arrow_up"></div>
        <div class="contacts_left">
            <div class="infoblock">
                <div><span>Адрес</span></div>
                <!-- <div>г.Алматы, ул.Гоголя 22</div> -->
                <div>г.Алматы, Айтеке би 198/27</div>
                <div>(оф.35)</div>
            	<div>ABC KAZ CCTV</div>
            </div><!-- .infoblock -->
            <div class="infoblock">
                <div><span>Контактный телефон</span></div>
                <div class="tel">+7 (727) 379-06-47</div>
                <div class="tel">+7 (727) 395-90-87</div>
                <div>10:00 - 20:00, звонок беспланый</div>
            </div><!-- .infoblock -->
            <!-- <div class="infoblock">
                <div><span>Факс</span></div>
                <div>+7 (727) 2938-132</div>
            </div> --><!-- .infoblock -->
            	<!--<div class="infoblock"></div>
            	<div class="infoblock"></div>-->
        </div><!-- .contacts_left -->
        <div class="contacts_right">
		
			<?php if(1):?>
			<!-- <h2><?php //echo Yii::t('main','You can send us a request')?></h2> -->
			
			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
				'id'=>'contacts-form',
				'enableAjaxValidation'=>false,
				'enableClientValidation' => true,
			)); ?>
			
				<!-- <p class="help-block"> --><?//php echo Yii::t('main','Fields with ') ?><!-- <span class="required">*</span> -->
				<?php //echo Yii::t('main',' are required.') ?><!-- </p> -->
			
				<?php echo $form->errorSummary($model); ?>
				
				<!-- <div class="row"> -->
					<?php //echo $form->labelEx($model,'name'); ?>
					<?php echo $form->textField($model,'name',array('class'=>'input_style','placeholder'=>'Ваше имя')); ?>
					<?php echo $form->error($model,'name'); ?>
				<!-- </div> -->
					<?php //echo $form->labelEx($model,'email'); ?>
					<?php echo $form->textField($model,'email',array('class'=>'input_style','placeholder'=>'Ваш e-mail')); ?>
					<?php echo $form->error($model,'email'); ?>
					
					<?php //echo $form->labelEx($model,'body'); ?>
					<?php echo $form->textArea($model,'content',array('class'=>'input_style','placeholder'=>'Ваш вопрос')); ?>
					<?php echo $form->error($model,'content'); ?>
			
				<?php //echo $form->textFieldRow($model,'name',array('class'=>'input_style','placeholder'=>'Ваше имя')); ?>
				
				<?php //echo $form->textFieldRow($model, 'subject', array('class'=>'input_style','maxlength'=>45)); ?>
			
				<?php //echo $form->textFieldRow($model,'email',array('class'=>'input_style','maxlength'=>500)); ?>
			
				<?php //echo $form->textAreaRow($model,'content',array('class'=>'input_style', 'rows'=>5)); ?>
				
				<?php //echo $form->checkBoxRow($model, 'copyMe'); ?>
				
				<?php if ($model->requireCaptcha): ?>
					<div class="captcha">
						<?php echo CHtml::activeTextField($model, 'verifyCode',array('class'=>'input_style','placeholder'=>'Код с картинки')); ?>
						<div class="captcha_box"><?php $this->widget('CCaptcha'); ?></div> 
					</div>
				<?php endif; ?>
				<div class="clear"></div>
				<!-- <div class="form-actions"> -->
					<?php $this->widget('bootstrap.widgets.TbButton', array(
						'buttonType'=>'submit',
						'type'=>'primary',
						'label'=>Yii::t('main','ОТПРАВИТЬ'),
						'htmlOptions' => array('class'=>'button'),
					)); ?>
				<!-- </div> -->
			<!-- array('class'=>'button'), -->
			<?php $this->endWidget(); ?>
		</div><!-- .contacts_right -->
    </div><!-- ..contacts_content -->
		<?php endif; ?>

</div><!-- .block_contacts -->