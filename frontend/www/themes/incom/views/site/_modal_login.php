<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = Yii::t('main','Login');
$this->breadcrumbs = array(
	Yii::t('main','Login'),
);
?>

<?php if(Yii::app()->user->hasFlash('success')):
        echo Yii::app()->user->getFlash('success'); 
endif; ?>
<div class="block_modal">
	<div class="block_modal_wrapper">
		<div class="modal_content">
			<div class="modal_header">
            	<div class="left_line"></div>
            	    <span>Вход в систему</span>
                <div class="right_line"></div>
                <a class="modal_header_close" href="#"></a>
            </div><!-- .modal_header -->
            
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'login-form',
				'action' => Yii::app()->createUrl('/site/login'),
				'enableAjaxValidation' => false,
				'enableClientValidation' => true,
				'clientOptions' => array(
					'validateOnSubmit' => true,
				),
			)); ?>
				<?php //echo $form->label($model, 'username');?>
				<?php echo $form->textField($model, 'username',array('class'=>'input_style','placeholder'=>'Имя пользователя','style'=>'margin-top: 20px;')); ?>
				<?php echo $form->error($model, 'username');?>
				<br/>
				<?php //echo $form->label($model, 'password');?>
				<?php echo $form->passwordField($model, 'password',array('class'=>'input_style','placeholder'=>'Пароль')); ?>
				<?php echo $form->error($model, 'password');?><br/>
				<?php echo CHtml::link(Yii::t('main','Reset Password'), array('/user/resetpassword'),array('class' => 'fleft modal_txt')) ?> <br/><br/>
				
				<?php echo $form->checkBox($model, 'rememberMe',array('class' => 'fleft styled','style'=>'margin:18px 10px 0 0;')); ?>
				<?php echo $form->label($model, 'rememberMe',array('class' => 'fleft modal_txt','style'=>'margin-top: 15px;'));?>
				<!-- <div class="clear"></div> -->
				
				<?php if ($model->requireCaptcha): ?>
					<?php $this->widget('CCaptcha'); ?>
					<?php echo CHtml::activeTextField($model, 'verifyCode'); ?>
				<?php endif; ?>
				<div class="actions">
					<?php echo CHtml::submitButton(Yii::t('main','Login'),array('class' => 'fleft modal_button')); ?>
				</div>
			
				<?php $this->endWidget(); ?>
				<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'top-sidebar')); ?>
					
					<div style="height: 10px"></div>
				<?php $this->endWidget();?>
				
				<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'sidebar-two')); ?>
					
					
				<?php $this->endWidget();?>
				
				<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'calendar-sidebar')); ?>
					
				<?php $this->endWidget();?>
            	<div class="clear"></div>
            	<div class="modal_footer">
                	<span class="modal_txt">У вас еще нет аккаунта?</span>
                    <a href="#" class="modal_txt">Зарегистрироваться</a>
                </div>
		</div><!-- .modal_content -->
	</div><!-- .block_modal_wrapper -->
</div><!-- .block_modal -->