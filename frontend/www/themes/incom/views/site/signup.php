<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = 'SignUp';
$this->breadcrumbs = array(
	'SignUp',
);
?>

	<div class="block_registration">
		<div class="header1">Регистрация нового аккаунта</div>
		<div class="clear" style="height: 15px;"></div>
		<div class="registration_left">
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'login-form',
				'enableAjaxValidation' => false,
				'enableClientValidation' => true,
				'clientOptions' => array(
				'validateOnSubmit' => true,
				),
			)); ?>
		
			<?php echo $form->errorSummary($model); ?>
		
			<?php //echo $form->label($model, 'username');?>
			<?php echo $form->textField($model, 'username',array('class'=>'input_style','placeholder'=>'Ваше имя')); ?>
			<?php echo $form->error($model, 'username');?>
			<br/>
			<?php //echo $form->label($model, 'password');?>
			<?php echo $form->passwordField($model, 'password',array('class'=>'input_style','placeholder'=>'Пароль')); ?>
			<?php echo $form->error($model, 'password');?>
			<br/>
			<?php //echo $form->label($model, 'password_repeat');?>
			<?php echo $form->passwordField($model, 'password_repeat',array('class'=>'input_style','placeholder'=>'Повторите пароль')); ?>
			<?php echo $form->error($model, 'password_repeat');?>
			<br/>
			<?php //echo $form->label($model, 'email');?>
			<?php echo $form->textField($model, 'email',array('class'=>'input_style','placeholder'=>'Электронная почтач')); ?>
			<?php echo $form->error($model, 'email');?>
			<br/>
			
				<?php if ($model->requireCaptcha): ?>
					<div class="captcha">
						<?php echo CHtml::activeTextField($model, 'verifyCode',array('class'=>'input_style','placeholder'=>'Код с картинки')); ?>
						<div class="captcha_box"><?php $this->widget('CCaptcha'); ?></div> 
					</div>
				<?php endif; ?>
				<div class="clear"></div>
					<?php $this->widget('bootstrap.widgets.TbButton', array(
						'buttonType'=>'submit',
						'type'=>'primary',
						'label'=>'РЕГИСТРАЦИЯ',
						'htmlOptions' => array('class'=>'button'),
					)); ?>
		
			<?php $this->endWidget(); ?>
			
		</div><!-- .<div class="registration_left"> -->
		
		<div class="registration_right">
        	<div class="registration_infoblock_header">
            	    <div>Подключаемый тариф </div><div>Домашний</div>
            </div>
			<div class="registration_infoblock">  
                <div class="registration_infoblock_line">
                	<div>Цена камеры в месяц</div>
                	<div><?php echo !empty($tariff->cost_camera_per_month) ? $tariff->cost_camera_per_month.' тенге' : '0 тенге';?></div>
                </div>
                <div class="registration_infoblock_line">
                	<div>Подключаемых камер</div>
                	<div><?php echo $tariff->number_cameras?> шт.</div>
                </div>
                <div class="registration_infoblock_line">
                	<div>Передача доступа к камере</div><div><?php echo $tariff->another_users_access; ?></div>
                </div>
                <div class="registration_infoblock_line">
                	<div>Локальный видеоархив</div><div class="check_<?php echo $tariff->local_archive; ?>"></div>            	
                </div>
                <div class="registration_infoblock_line">
                	<div>Облачный видеоархив</div><div class="check_<?php echo $tariff->clode_archive; ?>"></div>
                </div>
            </div><!-- .registration_infoblock -->
            <div class="clear" style="height:20px;"></div>
            <a href="#" class="link_green">Выбрать другой тариф</a>
        </div><!-- .registration_right -->
		<div class="clear"></div>
	</div><!-- .block_registration -->
