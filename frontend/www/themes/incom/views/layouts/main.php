<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width">
    
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/normalize.css">
	<!-- <link rel="stylesheet" type="text/css" href=" --><?php //echo Yii::app()->theme->baseUrl; ?><!-- /css/main.css"> -->
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/vendor/modernizr-2.6.2.min.js'); ?>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
    	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/main.css'); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/main.js'); ?>
	<!--[if lte IE 7]><script>alert("Для корректного отображения сайта используйте Internet Explorer не ниже 8 версии или любой другой современный браузер! ")</script><![endif]-->
	<!--[if IE]>
		<script>
			document.createElement('header');
			document.createElement('nav');
			document.createElement('section');
			document.createElement('article');
			document.createElement('aside');
			document.createElement('footer');
		</script>
	<![endif]-->
	<script type="text/javascript"> 
	var bookmarkurl="http://domain.com" 
	var bookmarktitle="<?php echo CHtml::encode($this->pageTitle); ?>" 
	function addbookmark(){ 
		if (document.all) 
		window.external.AddFavorite(bookmarkurl,bookmarktitle) 
	} 
	</script> 
</head>

<body>
<div id="wrapper">
	<div id="header">
		<div class="second_menu">
			<?php $this->widget('zii.widgets.CMenu',array(
				'items'=>array(
					array('label'=>'Главная', 'url'=>array('/site/index')),
					array('label'=>'Как это работает', 'url'=>array('/site/#')),
					array('label'=>'Тарифы', 'url'=>array('/site/tariffs')),
					array('label'=>'Товары', 'url'=>array('/site/#')),
					array('label'=>'Контакты', 'url'=>array('/site/contacts')),
					array('label'=>'Регистрация', 'url'=>array('/site/signup')),
					array('label'=>'Вход', 
							'url'=>array('/site/login'), 
							'visible'=>Yii::app()->user->isGuest,
							'itemOptions'=>array(
									'data-toggle' => 'modal',
									'data-target' => '#login',
									'onclick'=>'wrapperHide();'
							)
						),
					array(
							'label'=>'Eщё', 
							'url'=>array('/site/signup'),
							'visible'=>!Yii::app()->user->isGuest, 
							'itemOptions'=>array('class'=>'dropdown'),
							'linkOptions'=>array(
									'class'=>'dropdown-toggle',
									'data-toggle'=>'dropdown',
									),
							'submenuOptions' => array( 'class' => 'dropdown-menu position', 'id'=>'submenu-dd' ),
							'template' => '{menu} <b class="caret"></b>',
							'items' => array (
									array('label'=>Yii::app()->user->name,'itemOptions'=>array('class'=>'user_name'),/* ,'template' => '{menu} <b class="caret"></b>' */),
									array('itemOptions'=>array('class'=>'divider'),/* ,'template' => '{menu} <b class="caret"></b>' */),
									array('label'=>'Выход', 'url'=>array('/site/logout')),
									array('label'=>'Кабинет', 'url' => array('/cabinet')),
							),
						),
					/* array('label'=>'Выход ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest) */
				),
				'htmlOptions'=>array('class'=>'level_top')
			)); ?>
		</div><!-- .second_menu -->
		
		<?php 
			$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'login')); ?>
			 
			   <?php echo $this->renderPartial('/site/_modal_login', array('model'=>new LoginForm())); ?>
			 
		<?php $this->endWidget(); ?>
		
		<script type="text/javascript">
			$('div.modal-backdrop').live('click',function(){
				wrapperShow();
			});
		
			$('.modal-body#xBut').live('click',function(){
				wrapperShow();
			});
		
			function wrapperShow(){
				$('#wrapper').css('position', 'relative')
			}
						
			function wrapperHide(){
				$('#wrapper').css('position', 'static')
			}
		</script>
		
		<div class="clear" style="height:20px;"></div>
	</div><!-- #header -->
   
    <div class="block_logo">
        <div>
            <div class="logo">
            	<a href="/"></a>
                <div class="slogan3">
                	<span>Твое видео — твое спокойствие</span>
                    <div class="clear"></div>
                    <div>
                        <div id="time_now_top"></div>
                        <div id="date_now_top"></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .block_logo -->
	<script type="text/javascript">
		$(document).ready(function() {
			function myDate(){
				var now = new Date();
			
				var now_Hour = now.getHours();
				var out_Hour;
			
				if(now_Hour<10){
					out_Hour="0"+now_Hour;
				}
				else{
					out_Hour=now_Hour;
				}
			
				var now_min = now.getMinutes();
				var out_min;
			  
				if(now_min<10){
					out_min="0"+now_min;
				}
				else{
					out_min=now_min;
				}
			
				var out_day;
				var out_month;
				var out_year;
			
				var now_Year_str = now.getYear();
			  
				out_year = now_Year_str.toString().substr(-2, 2);
			
				var now_Day = now.getDate();
				if(now_Day<10){
					out_day="0"+now_Day;
				}
				else{
					out_day=now_Day;
				}
			
				var now_Month = now.getMonth()+1;
				if(now_Month<10){
					out_month="0"+now_Month;
				}
				else{
					out_month=now_Month;
				}
			
				$("#time_now_top").html(out_Hour+":"+out_min);
				$("#date_now_top").html(out_day+"."+out_month+"."+out_year);
			} 
			myDate();
			setInterval(myDate, 10000);
		});
	</script>
	<!-- <div class="container" style="height: 900px; background: #ccc;"> -->
		<?php echo $content ?>
	<!-- </div> --> <!-- /container -->
    <div class="clear"></div>
	<footer id="footer">
		<div class="second_menu">
			<?php $this->widget('zii.widgets.CMenu',array(
				'items'=>array(
					array('label'=>'Главная', 'url'=>array('/site/index')),
					array('label'=>'Как это работает', 'url'=>array('/site/#')),
					array('label'=>'Тарифы', 'url'=>array('/site/tariffs')),
					array('label'=>'Товары', 'url'=>array('/site/contacts')),
					array('label'=>'Контакты', 'url'=>array('/site/contacts')),
					array('label'=>'Регистрация', 'url'=>array('/site/signup')),
					array('label'=>'Вход', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					array(
							'label'=>'Eщё', 
							'url'=>array('/site/signup'),
							'visible'=>!Yii::app()->user->isGuest, 
							'itemOptions'=>array('class'=>'dropdown'),
							'linkOptions'=>array(
									'class'=>'dropdown-toggle',
									'data-toggle'=>'dropdown',
									),
							'submenuOptions' => array( 'class' => 'dropdown-menu position', 'id'=>'submenu-dd' ),
							'template' => '{menu} <b class="caret"></b>',
							'items' => array (
									array('label'=>Yii::app()->user->name,'itemOptions'=>array('class'=>'user_name'),/* ,'template' => '{menu} <b class="caret"></b>' */),
									array('itemOptions'=>array('class'=>'divider'),/* ,'template' => '{menu} <b class="caret"></b>' */),
									array('label'=>'Выход', 'url'=>array('/site/logout')),
									array('label'=>'Кабинет', 'url' => array('/cabinet')),
							),
						),
					/* array('label'=>'Выход ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest) */
				),
				'htmlOptions'=>array('class'=>'level_top')
			)); ?>
		</div><!-- .second_menu -->
        <div class="block_logo">
            <div>
                <div class="logo">
                	<a href="/"></a>
                	<div class="support">
                    	<div>
                        	<span>Консультации по телефону</span>
                            <div>+7 (727) 379-06-47<br />+7 (727) 395-90-87</div>
                        </div>
                    	<div>
                        	<span>Работаем без выходных</span>
                            <div>10:00 - 20:00</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</footer><!-- #footer -->
</div><!-- #wrapper -->
</body>
</html>
