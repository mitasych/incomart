<?php $this->beginContent('//layouts/mainCabinet'); ?>
<!-- <div class="container">
	<div id="content"> -->
		<?php echo $content; ?>
		
		<div class="sb well">
			<form class="navbar-search pull-left">
			  <input type="text" class="search-query" placeholder="<?php echo Yii::t('main','Search')?>">
			  <button type="submit" class="btn"><i class="icon-search"></i></button>
			</form>
			<div class="clearfix"></div>
			<hr style=" margin:10px;" />
			<div>
				<?php 
				
				$this->widget('UserCamsWidget');
				
				$this->widget('bootstrap.widgets.TbButton', array(
						'buttonType'=>'button',
						'type'=>'info',
						'label'=>'Список камер',
						'toggle'=>true,
						'htmlOptions'=>array('id'=>'toggle_cameras_list'),
				));
				
				
				$this->widget('bootstrap.widgets.TbButton', array(
						'buttonType' => 'link',
						'url'=>'/user/cameraAdd',
						'type'=>'primary',
						'htmlOptions' => array('style'=>'float:right; margin:0 20px 20px 0;'),
						'label'=>'Добавить камеру'/* Yii::t('main','Search') */,
					)); 
				?>
			</div>
		</div>
	<!-- </div> --><!-- content -->
<!-- </div> -->
<script type="text/javascript">
	$('#toggle_cameras_list').click(function(){
		$( "#camera-grid" ).toggle();
	});
</script>
<?php $this->endContent(); ?>