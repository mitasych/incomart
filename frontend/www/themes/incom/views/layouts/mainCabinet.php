<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width">
    
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/normalize.css">
	<!-- <link rel="stylesheet" type="text/css" href=" --><?php //echo Yii::app()->theme->baseUrl; ?><!-- /css/main.css"> -->
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/vendor/modernizr-2.6.2.min.js'); ?>
	<?//php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/cufon-yui.js'); ?>
	<?//php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/Myriad_Pro_400-Myriad_Pro_600-Myriad_Pro_italic_400-Myriad_Pro_italic_600.font.js'); ?>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
    	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/cabinet.css'); ?>
	<!--[if lte IE 7]><script>alert("Для корректного отображения сайта используйте Internet Explorer не ниже 8 версии или любой другой современный браузер! ")</script><![endif]-->
	<!--[if IE]>
		<script>
			document.createElement('header');
			document.createElement('nav');
			document.createElement('section');
			document.createElement('article');
			document.createElement('aside');
			document.createElement('footer');
		</script>
	<![endif]-->
	<script type="text/javascript"> 
	var bookmarkurl="http://domain.com" 
	var bookmarktitle="<?php echo CHtml::encode($this->pageTitle); ?>" 
	function addbookmark(){ 
		if (document.all) 
		window.external.AddFavorite(bookmarkurl,bookmarktitle) 
	} 
	</script> 
</head>

<body>
	<div id="header" class="cabinet">

		<!-- ************************************************************************* -->
		
		<?php 
		$this->widget('bootstrap.widgets.TbNavbar', array(
				'type'=>'inverse', // null or 'inverse'
				'brand'=>'VIDEOnline',
				'brandUrl'=>'#',
				'collapse'=>true, // requires bootstrap-responsive.css
				'items'=>array(
						array(
								'class'=>'bootstrap.widgets.TbMenu',
								'items'=>array(
										array('label'=>'Мои камеры','icon'=>'icon-facetime-video icon-white', 'url'=>array('/cabinet/index')),
										//array('label'=>'На карте','icon'=>'icon-map-marker icon-white', 'url'=>'#'),
										array('label'=>'Экспорт архива','icon'=>'icon-download-alt icon-white', 'url'=>array('/cabinet/archiveExport')),
										array('label'=>'Мой счёт','icon'=>'icon-briefcase icon-white', 'url'=>array('/user/account')),
										array('label'=>'Мои услуги','icon'=>'icon-shopping-cart icon-white', 'url'=>array('/user/services')),
										array('label'=>'Тикеты','icon'=>'icon-align-justify icon-white', 'url'=>'#', 'items'=>array(
												array('label'=>'Список запросов', 'url'=>'/user/tickets'),
												array('label'=>'Создать запрос', 'url'=>'/user/createTicket'),
												array('label'=>'Создать отзыв', 'url'=>'/user/createReview'),
										)),
										array('label'=>'Еще...','icon'=>'icon-align-justify icon-white', 'url'=>'#', 'items'=>array(
												array('label'=>'Action', 'url'=>'#'),
												array('label'=>'Another action', 'url'=>'#'),
												array('label'=>'Something else here', 'url'=>'#'),
												'---',
												array('label'=>'NAV HEADER'),
												array('label'=>'Separated link', 'url'=>'#'),
												array('label'=>'One more separated link', 'url'=>'#'),
										)),
								),
						),
						/* '<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>', */
						array(
								'class'=>'bootstrap.widgets.TbMenu',
								'htmlOptions'=>array('class'=>'pull-right'),
								'items'=>array(
										array(
												'label'=>Yii::app()->user->name,
												'icon'=>'icon-user icon-white',
												'url'=>'#',
												'items' => array (
														array('label'=>Yii::t('menu','Profile'), 'url' => array('/user/'.Yii::app()->user->id)),
														array('label'=>Yii::t('menu','Edit profile'), 'url' => array('/user/update/'.Yii::app()->user->id)),
														array('label'=>Yii::t('menu','Update password'), 'url' => array('/user/updatepassword/')),
														array('label'=>Yii::t('menu','Cabinet'), 'url' => array('/cabinet/')),
												),
												),
										array('label'=>'','icon'=>'icon-share icon-white','url'=>array('/site/logout')),

								),
						),
				),
		));
		?>
	</div><!-- #header -->
	
	
	<!-- **************************************************************************************** -->
	<!-- <div class="container" style="height: 900px; background: #ccc;"> -->
	<div class="container cabinet">
		<!-- <div class="row"> -->
			<div class="span12">
				<?php echo $content ?>
				
				
			</div><!-- .span12 -->
		<!-- </div> --><!-- .row -->
	</div><!-- .container .cabinet -->
	<!-- </div> --> <!-- /container -->
	<!-- **************************************************************************************** -->
	<div class="clear"></div>
	<div class="container">
		<!-- <div class="row"> -->
			<div class="footer span12 cabinet">
				<span>VIDEOnline © 2010–2013</span>
				<a href="/user/createReview"><i class="icon-comment"></i>Оставить отзыв</a>
				<a href="#"><i class="icon-download"></i>Загрузки</a>
				<a href="#"><i class="icon-question-sign"></i>Помощь</a>
			</div><!-- .footer -->
		<!-- </div> --><!-- .row -->
	</div><!-- .container .cabinet -->

</body>
</html>
