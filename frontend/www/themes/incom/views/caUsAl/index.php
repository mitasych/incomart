<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->
<?php
$this->breadcrumbs=array(
	'CaUsAlControllers',
);

$this->menu=array(
	array('label'=>'Create CaUsAlController', 'url'=>array('create')),
	array('label'=>'Manage CaUsAlController', 'url'=>array('admin')),
);
?>

<h1>CaUsAlControllers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
