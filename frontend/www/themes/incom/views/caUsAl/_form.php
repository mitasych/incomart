<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'client-account-create-form',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
	
    <div class="row">
        <?php echo $form->labelEx($model,'id_camera'); ?>
        <?php echo $form->textField($model,'id_camera'); ?>
        <?php echo $form->error($model,'id_camera'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'id_user'); ?>
        <?php echo $form->textField($model,'id_user'); ?>
        <?php echo $form->error($model,'id_user'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'create_time'); ?>
        <?php echo $form->textField($model,'create_time'); ?>
        <?php echo $form->error($model,'create_time'); ?>
    </div>
	
    <div class="row">
        <?php echo $form->labelEx($model,'end_time'); ?>
        <?php echo $form->textField($model,'end_time'); ?>
        <?php echo $form->error($model,'end_time'); ?>
    </div>
	
	
    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form --> 
