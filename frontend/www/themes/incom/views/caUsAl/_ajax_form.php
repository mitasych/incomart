<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->

<div class='form'>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'client-account-create-form',
    'enableAjaxValidation'=>false,
)); ?>

    <?php // echo $form->errorSummary($model); ?>
    
    <?php echo $form->hiddenField($model,'id_camera'); ?>
	
    <div>
        <?php echo $form->labelEx($model,'username'); ?>
        <?php 
// 	        $this->widget('bootstrap.widgets.TbTypeahead',array(
// 	        		'model'=>$model,
// 	        		'attribute'=>'username',
// 	        		'options'=> array(
// 	        				'source'=>'js:function(query,process){
// 		        				$.ajax({
// 			        				url: ''.$this->createUrl('user/autocompleteNames').'',
// 			        				type: 'GET',
// 			        				dataType:'json',
// 			        				data: {term: query},
// 			        				success: process,
// 						        });
// 					        }',
// 	        		),
// 	        ));
        $this->widget('zii.widgets.jui.CJuiAutoComplete',
        		array(
        				'model'=>$model,
        				'attribute'=>'username',
        				'source'=>$this->createUrl('user/autocompleteNames'),
        				'options'=> array(
        						'showAnim'=>'fold',
        				),
        		)
        );
        ?>
        <?php // echo $form->textField($model,'username'); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>
    
    <div>
        <?php echo $form->labelEx($model,'timeUse'); ?>
        <?php echo $form->textField($model,'timeUse'); ?>
        <?php echo $form->error($model,'timeUse'); ?>
    </div>
     
    <div class='buttons'>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'primary',
			'label'=>'Добавить',
        	'htmlOptions'=>array('class'=>'addAlUs'),
		)); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form --> 
