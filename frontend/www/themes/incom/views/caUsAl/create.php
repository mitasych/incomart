<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->
<?php
$this->breadcrumbs=array(
	'CaUsAlControllers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CaUsAlControllers', 'url'=>array('index')),
    array('label'=>'Manage CaUsAlController', 'url'=>array('admin')),
);
?>

<h1>Create CaUsAlController</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
