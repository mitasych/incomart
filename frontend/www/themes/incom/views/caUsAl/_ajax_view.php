<div class="isset_user" id="isUs_<?php echo $model->id_user; ?>">
	<div class="is_us_name"><?php echo $model->user->username; ?></div>
	<div class="is_us_endtime"><?php echo $model->end_time; ?></div>
	<div class="is_us_delete">
		<?php 
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'button',
				'type'=>'danger',
				'size'=>'mini',
				'icon'=>'remove white',
				'label'=>'',
				'htmlOptions'=>array('class'=>'remAllowUse', 'onClick'=>'removeAlUs('.$model->id_user.')'),
			)); 
		?>
	</div>
</div>
<div class="clear"></div>