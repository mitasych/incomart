<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->
<?php
$this->breadcrumbs=array(
	'CaUsAlControllers'=>array('index'),
	'View'=>array('view', 'id_camera'=>$model->id_camera, 'id_user'=>$model->id_user),
	'Update',
);

$this->menu=array(
	array('label'=>'List CaUsAlController', 'url'=>array('index')),
	array('label'=>'Create CaUsAlController', 'url'=>array('create')),
	array('label'=>'View CaUsAlController', 'url'=>array('view', 'id_camera'=>$model->id_camera, 'id_user'=>$model->id_user)),
	array('label'=>'Manage CaUsAlController', 'url'=>array('admin')),
); 
?>

<h1>Update Client</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
