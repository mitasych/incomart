<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->
<?php
$this->breadcrumbs=array(
	'CaUsAlControllers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List CaUsAlControllers', 'url'=>array('index')),
	array('label'=>'Create CaUsAlController', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('cameraUserAllowedgrid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage CaUsAlControllers</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'cameraUserAllowedgrid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id_camera',
        'Id Camera',
        'labels',
        'create_time',
        'Create Time',
        'labels',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array
            (
                'view' => array
                (
                    'url'=>
                    'Yii::app()->createUrl("cameraUserAllowed/view/", 
                                            array("id_camera"=>$data->id_camera, "id_user"=>$data->id_user
											))',
                ),
                'update' => array
                (
                    'url'=>
                    'Yii::app()->createUrl("cameraUserAllowed/update/", 
                                            array("id_camera"=>$data->id_camera, "id_user"=>$data->id_user
											))',
                ),
                'delete'=> array
                (
                    'url'=>
                    'Yii::app()->createUrl("cameraUserAllowed/delete/", 
                                            array("id_camera"=>$data->id_camera, "id_user"=>$data->id_user
											))',
                ),
            ),
        ),
    ),
)); ?>
