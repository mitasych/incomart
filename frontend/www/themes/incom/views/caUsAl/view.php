<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->
<?php
$this->breadcrumbs=array(
	'CaUsAlControllers'=>array('index'),
	'View',
);

$this->menu=array(
	array('label'=>'List CaUsAlController', 'url'=>array('index')),
	array('label'=>'Create CaUsAlController', 'url'=>array('create')),
	array('label'=>'Update CaUsAlController', 'url'=>array('update', 'id_camera'=>$model->id_camera, 'id_user'=>$model->id_user)),
	array('label'=>'Delete CaUsAlController', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	                                           'id_camera'=>$model->id_camera, 'id_user'=>$model->id_user),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CaUsAlController', 'url'=>array('admin')),
);
?>

<h1>View CaUsAlController</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_camera',
		'id_user',
		'username',
		'create_time',
		'end_time',
	),
)); ?>
