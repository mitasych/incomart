<?php
$this->breadcrumbs=array(
	'Tickets'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ticket','url'=>array('index')),
	array('label'=>'Create Ticket','url'=>array('create')),
	array('label'=>'Update Ticket','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Ticket','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ticket','url'=>array('admin')),
);
?>
<div class="page-header">
	<h1>
	<?php echo Yii::t('main','View Ticket: "'); echo $model->id.'"'; ?>
	</h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
				'name'=>'theme', 
				'value'=>$model->textTheme,
				),
		'description',
		'create_time',
		'status',
		'close_time',
		'priority',
		'camera_id',
		'account_id',
	),
)); ?>
<?php echo $this->renderPartial('ticket/indexMessages', array('dataProvider'=>$messages->search(),));?>