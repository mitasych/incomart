<?php
$this->breadcrumbs=array(
	'Ticket Messages'=>array('index'),
	'Create',
);

// $this->menu=array(
// 	array('label'=>'List TicketMessage','url'=>array('index')),
// 	array('label'=>'Manage TicketMessage','url'=>array('admin')),
// );
?>

<h1>Create TicketMessage</h1>

<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'link',
		'url'=>'/admin/ticket/'.$model->ticket_id,
		//'type'=>'primary',
		'htmlOptions' => array('style'=>'margin: 0 0 0 0px;'),
		//'htmlOptions' => array('style'=>'float:left; margin: 0 0px 0px 70px;'),
		'label'=>'Просмотреть тикет'/* Yii::t('main','Search') */,
	)); 
?>
<br><br>
<?php $this->widget('bootstrap.widgets.TbBox', array(
    'title' => 'Текст тикета',
    'headerIcon' => 'icon-home',
    'content' => $ticket_text
)); ?>
<?php if (!empty($model->ask_id)):?>
	<?php $this->widget('bootstrap.widgets.TbBox', array(
			'title' => 'Текст вопроса',
		    'headerIcon' => 'icon-hand-right',
		    'content' => $quest_text
		)); 
	?>
<?php endif; ?>

<?php echo $this->renderPartial('ticket/_formMessage', array('model'=>$model)); ?>