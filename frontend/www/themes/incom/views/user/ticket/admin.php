<?php
$this->breadcrumbs=array(
	'Tickets'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Ticket','url'=>array('index')),
	array('label'=>'Create Ticket','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ticket-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Мои тикеты</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'link',
		'url'=>'/user/createTicket',
// 		'type'=>'primary',
		'htmlOptions' => array('style'=>'float:left; text-decoration: none;'),
		'label'=>'Создать тикет'/* Yii::t('main','Search') */,
	)); 
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'ticket-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'theme',
		'description',
		'create_time',
		'status',
		'close_time',
		/*
		'priority',
		'user_id',
		'camera_id',
		'account_id',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{view}',
			'buttons'=>array(
						'view'=>array(
								'url'=>'"/user/viewTicket/".$data->id',
							),
					),
		),
	),
)); ?>
