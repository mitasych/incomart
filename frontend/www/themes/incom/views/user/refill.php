<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Cabinet','url'=>array('cabinet')),
	array('label'=>'Add Camera','url'=>array('cameraAdd')),
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-account-transaction-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('main','Fields with ') ?><span class="required">*</span>
	<?php echo Yii::t('main',' are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php // echo $form->textFieldRow($model,'account_id',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'type_transaction_id',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'guid_transaction',array('class'=>'span5','maxlength'=>45)); ?>

	<?php // echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php // echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'status_error',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>
	
	<?php echo $form->textFieldRow($model,'amount',array('class'=>'span5','maxlength'=>45)); ?>
	
	<?php // echo $form->textFieldRow($model,'payment_type',array('class'=>'span5','maxlength'=>45)); ?>
	
	<?php echo $form->radioButtonListRow($model, 'payment_type', UserAccountTransaction::paymentLabels()); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? Yii::t('main','Create') : Yii::t('main','Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
