<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Cabinet','url'=>array('cabinet')),
	array('label'=>'Add Camera','url'=>array('cameraAdd')),
);
?>

Подтвердите оплату <?php echo $model['amount']; ?> тг.

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-account-transaction-form',
	'enableAjaxValidation'=>false,
	'action' => 'http://3dsecure.kkb.kz/jsp/process/logon.jsp',
// 	'action' => 'https://epay.kkb.kz/jsp/process/logon.jsp',
)); ?>


	<?php // echo $form->errorSummary($model); ?>

	
	<?php // echo $form->hiddenField($model,'amount'); ?>
	<?php echo CHtml::hiddenField('Signed_Order_B64', $model['content64'])?>
	<?php echo CHtml::hiddenField('email', $model['email'])?>
	<?php echo CHtml::hiddenField('BackLink', Yii::app()->createAbsoluteUrl('/user/account'))?>
	<?php echo CHtml::hiddenField('PostLink', Yii::app()->createAbsoluteUrl('/user/payResponse'))?>
	<?php echo CHtml::hiddenField('appendix', $model['appendix'])?>
	<?php // echo $form->hiddenField($model,'amount'); ?>
	
	<?php // echo $form->textFieldRow($model,'amount',array('class'=>'span5','maxlength'=>45)); ?>
	
	<?php // echo $form->textFieldRow($model,'payment_type',array('class'=>'span5','maxlength'=>45)); ?>
	
	<?php // echo $form->radioButtonListRow($model, 'payment_type', UserAccountTransaction::paymentLabels()); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'htmlOptions'=>array('name'=>'GotoPay'),
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Перейти к оплате',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
