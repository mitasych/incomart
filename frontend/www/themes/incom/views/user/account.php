
<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Cabinet','url'=>array('cabinet')),
	array('label'=>'Add Camera','url'=>array('cameraAdd')),
);

?>
	<?php if(Yii::app()->user->hasFlash('no_account')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('no_account'); ?>
        </div>
	<?php endif; ?>

	<div class="page-header">
		<h2><?php echo Yii::t('main','Your Account')?></h2>
	</div>
	<div class="alert alert-info span8">
	  	Если вам необходимо производить оплату на расчетный счет, 
	  	<a href="#">заключите договор обслуживания для юридических лиц</a>
	  	<div class="clearfix"></div>
	  	<?php 
	  	$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'type'=>'primary',
				'htmlOptions' => array('style'=>'float:right; margin:0 20px 20px 0;'),
				'label'=>'Закрыть'/* Yii::t('main','Search') */,
			)); 
		?>
	</div>
	<div class="clearfix"></div>
	<div class="page-header">
		<h2>Ваш баланс</h2>
	</div>
	<blockquote>
		У вас на счету <i><?php echo (!is_null($model->balance) ? $model->balance : 0).' '.$model->currency->shortening; ?></i>
	</blockquote>
	<div class="page-header">
		<h2>Пополнить баланс</h2>
		<div>
		<?php 
	  	$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'link',
				'type'=>'primary',
	  			'url'=>$this->createUrl('user/refillAccount', array('acc_id'=>$model->id)),
				'htmlOptions' => array('style'=>'float:fleft; margin:15px 20px 0 0;'),
				'label'=>'Напрямую'/* Yii::t('main','Search') */,
			)); 
		?>
		</div>
	</div>
	<div class="page-header">
		<h2>История платежей</h2>
	</div>
	<blockquote class="fleft">
		История платежей:
	</blockquote>
	
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'user-account-transaction-grid',
		'dataProvider'=>$transactions->search(),
		'filter'=>$transactions,
		'columns'=>array(
// 			'id',
// 			'account_id',
// 			'guid_transaction',
			array(
					'name'=>'guid_transaction',
					'header'=>Yii::t('labels', 'Transaction'),
				),
			array(
					'name'=>'type_transaction_id',
					'filter'=>TransactionType::listItems(),
					'value'=>'$data->typeTransaction->name'
				),
			'description',
			array(
					'name'=>'status',
					'filter'=>UserAccountTransaction::listStatus(),
					'value'=>'$data->statusName'
				),
			array(
					'name'=>'amount',
					'value'=>'$data->amountFormat',
				),
			'create_time',
			/*
			'status_error',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{view}',
			),
			*/
		),
	)); ?>
	
	<?php 
  /* 	$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'htmlOptions' => array('style'=>'float:fleft; margin:15px 20px 0 0;'),
			'label'=>'Закрыть'//Yii::t('main','Search'),
		));  */
	?>