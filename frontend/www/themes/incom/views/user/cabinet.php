<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Cabinet','url'=>array('cabinet')),
	array('label'=>'Add Camera','url'=>array('cameraAdd')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('camera-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php if(0):?>
	<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
	</p>
	
	<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
	<div class="search-form" style="display:none">
	<?php $this->renderPartial('/camera/_search',array(
		'model'=>$model,
	)); ?>
	</div><!-- search-form -->
<?php endif;?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'camera-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
// 		'manufacturer_id',
		array(
				'name'=>'manufacturer_id',
				'type'=>'raw',
				'value'=>'$data->manufacturer->name'
				),
		array(
				'name'=>'model_id',
				'type'=>'raw',
				'value'=>'$data->model->name'
				),
// 		'model_id',
		'IP_address',
		'login',
		/*
		'password',
		'port',
		'url',
		'user_id',
		'create_time',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view}{delete}',
			'buttons'=>array
			(
					'view' => array
					(
							'url'=>'Yii::app()->createUrl("camera/view", array("id"=>$data->id))',
					),
					'delete' => array
					(
							'url'=>'Yii::app()->createUrl("camera/delete", array("id"=>$data->id))',
					),
			),
		),
	),
)); ?>
