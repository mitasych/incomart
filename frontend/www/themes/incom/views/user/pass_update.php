<?php
/**
 * login.php
 *
 * Example page <given as is, not even checked styles>
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:27 AM
 */
$this->pageTitle = Yii::t('main','Update Password');
$this->breadcrumbs = array(
	Yii::t('main','Update Password'),
);
?>
<h1><?php echo Yii::t('main','Update Password')?></h1>

<p><?php echo Yii::t('main','Please fill out the following form with your login credentials:')?></p>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'reset-pass-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
)); ?>

	<?php echo $form->label($model, 'old_password');?>
	<?php echo $form->passwordField($model, 'old_password'); ?>
	<?php echo $form->error($model, 'old_password');?>
	
	<?php echo $form->label($model, 'new_password');?>
	<?php echo $form->passwordField($model, 'new_password'); ?>
	<?php echo $form->error($model, 'new_password');?>
	
	<?php echo $form->label($model, 'new_password_repeat');?>
	<?php echo $form->passwordField($model, 'new_password_repeat'); ?>
	<?php echo $form->error($model, 'new_password_repeat');?>
	<br/>
	<?php $this->widget('CCaptcha'); ?>
	<?php echo CHtml::activeTextField($model, 'verifyCode'); ?>
	<div class="actions">
		<?php echo CHtml::submitButton(Yii::t('main','Update Password')); ?>
	</div>

<?php $this->endWidget(); ?>