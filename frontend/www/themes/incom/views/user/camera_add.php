<?php
// $this->breadcrumbs=array(
// 	'Cameras'=>array('index'),
// 	'Create',
// );

$this->menu=array(
	array('label'=>'Cabinet','url'=>array('cabinet')),
	array('label'=>'Add Camera','url'=>array('cameraAdd')),
);
?>

<h1><?php echo Yii::t('main','Create Camera')?></h1>

<?php echo $this->renderPartial('/camera/_form', array('model'=>$model, 'tarif'=>$tarif)); ?>