<?php
$this->menu=array(
	array('label'=>'Cabinet','url'=>array('cabinet')),
	array('label'=>'Add Camera','url'=>array('cameraAdd')),
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-account-transaction-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'tariff_id', Tarif::listItems(), array('options' => array($isTarif=>array('selected'=>true)))); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>Yii::t('main','Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
