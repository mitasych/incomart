<?php
return array(
	/* *********************** main_menu ******************************** */
		'Login'=>'Войти',
		'Logout'=>'Выйти',
		'Prices & Registration'=>'Цены и регистрация',
		'Home'=>'Главная',
		'Reviews'=>'Отзывы',
		'Questions & Support'=>'Вопросы и Поддержка',
		'As reseller'=>'Как реселлер',
		'As customer'=>'Как заказчик',
		'Profile'=>'Профиль',
		'Edit profile'=>'Редактировать профиль',
		'Update password'=>'Изменить пароль',
		'Cabinet'=>'Кабинет',
	);
?>