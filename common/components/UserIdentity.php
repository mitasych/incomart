<?php
/**
 * UserIdentity.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 8/12/12
 * Time: 10:00 PM
 */
class UserIdentity extends CUserIdentity {
	/**
	 * @var integer id of logged user
	 */
	private $_id;
	
	const ERROR_USER_ONLINE = 101;

	/**
	 * Authenticates username and password
	 * @return boolean CUserIdentity::ERROR_NONE if successful authentication
	 */
	public function authenticate() {
		$attribute = strpos($this->username, '@') ? 'email' : 'username';
		$user = User::model()->find(array(
										'condition' => $attribute . '=:loginname AND status = 1', 
										'params' => array(
														':loginname' => $this->username)
										));
		if ($user === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} else if(!is_null($user->sess_id)){
			$this->errorCode = self::ERROR_USER_ONLINE;
		} else if ($user->password!==crypt($this->password,$user->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$this->_id = $user->id;
			$this->username = $user->username;
			$this->setState('vkey', $user->activkey);
			$this->errorCode = self::ERROR_NONE;
		}
// 		echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump(!$this->errorCode); echo'</pre>';die;
		return !$this->errorCode ? !$this->errorCode : self::ERROR_USER_ONLINE;;
	}

	/**
	 *
	 * @return integer id of the logged user, null if not set
	 */
	public function getId() {
		return $this->_id;
	}
}