<?php

class WebUser extends CWebUser
{
	private $_model = null;
	
	public function getRole()
	{
		if ($user = $this->getModel()) {
			return $this->_model->role->machine_name;
		}
	}
	
	public function getModel()
	{
		if (!$this->isGuest && $this->_model === null) {
			$this->_model = User::model()->findByPk($this->id);
		}
		
		return $this->_model;
	}
	
	public function getSuperUser()
	{
		if ($user = $this->getModel()) {
			if ($user->superuser == 1) {
				return true;
			}
		}
		
		return false;
	}
	
	public function checkAccess($operation, $params=array(), $allowCaching=true)
	{
		if (ctype_upper($operation[0])) {
			$operation = app()->params['type_app'].'.'.$operation;
		}
		// Allow superusers access implicitly and do CWebUser::checkAccess for others.
		return $this->superUser===true ? true : parent::checkAccess($operation, $params, $allowCaching);
	}
}