<?php
/**
 * Controller.php
 *
 */
class Controller extends CController {

	public $breadcrumbs = array();
	public $menu = array();
	
	public function filters()
	{
		return array(
				array('common.components.RightsFilter',
						'params'=>array(
								'create_user_id' => isset($_REQUEST['id']) ? ($this->id != 'site' && isset($this->loadModel($_REQUEST['id'])->create_user_id) ? $this->loadModel($_REQUEST['id'])->create_user_id : '') : '',
						)),
		);
	}
	
	public function filterRights($filterChain)
	{
		$filter = new RightsFilter;
		$filter->allowedActions = $this->allowedActions();
		$filter->filter($filterChain);
	}
	
	/**
	 * @return string the actions that are always allowed separated by commas.
	 */
	public function allowedActions()
	{
		return '';
	}
	
	/**
	 * Denies the access of the user.
	 * @param string $message the message to display to the user.
	 * This method may be invoked when access check fails.
	 * @throws CHttpException when called unless login is required.
	 */
	public function accessDenied($message=null)
	{
		if( $message===null )
			$message = Yii::t('core', 'You are not authorized to perform this action.');
	
		$user = Yii::app()->getUser();
		if( $user->isGuest===true )
			$user->loginRequired();
		else{
			throw new CHttpException(403, $message);
		}
	}

}
