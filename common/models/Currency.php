<?php

/**
 * This is the model class for table "currency".
 *
 * The followings are the available columns in table 'currency':
 * @property integer $id
 * @property string $name
 * @property string $machine_name
 * @property string $shortening
 * @property string $code
 * @property string $symbol
 * @property string $symbol_place
 *
 * The followings are the available model relations:
 * @property UserAccount[] $userAccounts
 */
class Currency extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Currency the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'currency';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, machine_name, shortening', 'required'),
			array('name, machine_name, shortening, code, symbol', 'length', 'max'=>45),
			array('symbol_place', 'length', 'max'=>6),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, machine_name, shortening, code, symbol, symbol_place', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userAccounts' => array(self::HAS_MANY, 'UserAccount', 'currency_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('labels','Name'),
			'machine_name' => Yii::t('labels','Machine Name'),
			'shortening' => Yii::t('labels','Shortening'),
			'code' => Yii::t('labels','Code'),
			'symbol' => Yii::t('labels','Symbol'),
			'symbol_place' => Yii::t('labels','Place Of Symbol'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('machine_name',$this->machine_name,true);
		$criteria->compare('shortening',$this->shortening,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('symbol',$this->symbol,true);
		$criteria->compare('symbol_place',$this->symbol_place,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function listItems()
	{
		$res = self::model()->findAll(array('order'=>'id'));
		return CHtml::listData($res, 'id', 'name');
	}
	
	public static function listPlace()
	{
		return array('before'=>Yii::t('labels', 'Before'), 'after'=>Yii::t('labels', 'After'));
	}
}