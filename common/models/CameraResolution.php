<?php

/**
 * This is the model class for table "camera_resolution".
 *
 * The followings are the available columns in table 'camera_resolution':
 * @property integer $id
 * @property string $name
 * @property integer $height_dimension
 * @property integer $width_dimension
 *
 * The followings are the available model relations:
 * @property Camera[] $cameras
 */
class CameraResolution extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CameraResolution the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'camera_resolution';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, height_dimension, width_dimension', 'required'),
			array('height_dimension, width_dimension', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, height_dimension, width_dimension', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cameras' => array(self::MANY_MANY, 'Camera', 'camera_rel_resolution(resolution_id, camera_id)'),
			'tarifsRel' => array(self::HAS_MANY, 'CameraResolutionRelTarif', 'resolution_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('labels','Name'),
			'height_dimension' => Yii::t('labels','Height Dimension'),
			'width_dimension' => Yii::t('labels','Width Dimension'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('height_dimension',$this->height_dimension);
		$criteria->compare('width_dimension',$this->width_dimension);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function listItems()
	{
		$res = self::model()->findAll(array('order'=>'id'));
		return CHtml::listData($res, 'id', 'name');
	}
	
	public function getRelTarifs()
	{
		$arRelTarifs = array();
	
		foreach($this->tarifsRel as $tariff){
			$arRelTarifs[$tariff->tarif_id] = array('label'=>'Тариф '.$tariff->tariff->name, 'value'=>$tariff->cost_per_day);
		}
		return $arRelTarifs;
	}
	
	public function getIsRelTarifs()
	{
		$all = Tarif::listItemExt();
		$is = $this->relTarifs;
	
		if (!empty($is)) {
			foreach($all as $k => &$v){
				if (isset($is[$v['id']])) {
					$v['val'] = $is[$v['id']]['value'];
				}
				else{
					$v['val'] = 0;
				}
			}
		}
	
		return $all;
	}
	
	public function getRelTarifByTarifId($id)
	{
		foreach($this->isRelTarifs as $tarif){
			if ($tarif['id'] == $id) {
				return $tarif['val'];
			}
		}
		return 0;
	}
}