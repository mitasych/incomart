<?php

/**
 * This is the model class for table "cameras_model".
 *
 * The followings are the available columns in table 'cameras_model':
 * @property integer $id
 * @property string $name
 * @property integer $manufacturer_id
 * @property string $url
 * @property string $frame_rate_ids
 * @property string $resolutions_ids
 *
 * The followings are the available model relations:
 * @property CamerasManufacturer $manufacturer
 * @property Camera[] $cameras
 * @property CameraFrameRate[] $cameraFrameRates
 */
class CamerasModel extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CamerasModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cameras_model';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, manufacturer_id', 'required'),
			array('manufacturer_id', 'numerical', 'integerOnly'=>true),
			array('name, url', 'length', 'max'=>256),
			array('frame_rate_ids, resolutions_ids', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, manufacturer_id, url, frame_rate_ids, resolutions_ids', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manufacturer' => array(self::BELONGS_TO, 'CamerasManufacturer', 'manufacturer_id'),
			'rel_frame_rates' => array(self::HAS_MANY, 'CameraRelFrameRate', 'camera_model_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('labels','Name'),
			'manufacturer_id' => Yii::t('labels','Manufacturer'),
			'url' => 'Url',
			'frame_rate_ids' => Yii::t('labels','Frame Rate Ids'),
			'frameRates' => Yii::t('labels','Frame Rate Ids'),
			'resolutions_ids' => Yii::t('labels','Resolutions Ids'),
			'resolutions' => Yii::t('labels','Resolutions Ids'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('manufacturer_id',$this->manufacturer_id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('frame_rate_ids',$this->frame_rate_ids,true);
		$criteria->compare('resolutions_ids',$this->resolutions_ids,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getFrameRates()
	{
		$rates = explode(',', $this->frame_rate_ids);
		
		return $rates;
	}
	
	public function getNamesFrameRates()
	{
		$rates = CameraFrameRate::model()->findAllByAttributes(array('id'=>explode(',', $this->frame_rate_ids)));
		
		$names = CHtml::listData($rates, 'id', 'frame_rate');
		foreach ($names as &$name){
			$name = $name.' fps';
		}
		
		return implode(', ', $names);
		
	}
	
	public function getNamesResolutions()
	{
		$rates = CameraResolution::model()->findAllByAttributes(array('id'=>explode(',', $this->resolutions_ids)));
		
		return implode(', ', CHtml::listData($rates, 'id', 'name'));
	}
	
	public function getResolutions()
	{
		$res = explode(',', $this->resolutions_ids);
		
		return $res;
	}
	
	protected function beforeValidate() {
		$this->frame_rate_ids = implode(',', $_POST['CamerasModel']['frameRates']);
		$this->resolutions_ids = implode(',', $_POST['CamerasModel']['resolutions']);
		
		return parent::beforeValidate();
	}
}