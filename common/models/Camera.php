<?php

/**
 * This is the model class for table "camera".
 *
 * The followings are the available columns in table 'camera':
 * @property integer $id
 * @property string $name
 * @property string $machine_name
 * @property integer $manufacturer_id
 * @property integer $model_id
 * @property integer $frame_rate_id
 * @property integer $resolution_id
 * @property integer $bitrate_id
 * @property string $disk_space_per_day
 * @property integer $archive
 * @property integer $archive_days_number
 * @property string $disk_space
 * @property string $IP_address
 * @property string $login
 * @property string $password
 * @property integer $port
 * @property string $url
 * @property integer $user_id
 * @property string $create_time
 * @property string $cost_per_day
 * @property integer $active
 * @property string $last_payment_time
 *
 * The followings are the available model relations:
 * @property CamerasManufacturer $manufacturer
 * @property CamerasModel $model
 * @property User $user
 */
class Camera extends Details
{
	private $importantAttrs = array(
			'active',
			'login', 
			'password',
			'IP_address',
			'archive',
			'archive_days_number', 
			'port',
			'url'
			);
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Camera the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'camera';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
// 			array('name, manufacturer_id, model_id, frame_rate_id, resolution_id, IP_address, login, password, port, url, user_id, create_time', 'required'),
			array('name, machine_name, frame_rate_id, resolution_id, bitrate_id, IP_address, login, password, url, user_id, create_time', 'required'),
			array('port', 'numerical', 'integerOnly'=>false),
			array('manufacturer_id, model_id, frame_rate_id, resolution_id, bitrate_id, archive, archive_days_number, user_id, active', 'numerical', 'integerOnly'=>true),
			array('name, machine_name, url', 'length', 'max'=>256),
			array('allowedUsers', 'length', 'max'=>1000),
			array('disk_space_per_day, disk_space, IP_address, login, password, cost_per_day', 'length', 'max'=>45),
			array('last_payment_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, machine_name, manufacturer_id, model_id, frame_rate_id, resolution_id, bitrate_id, disk_space_per_day, archive, archive_days_number, disk_space, IP_address, login, password, port, url, user_id, create_time, allowedUsers, cost_per_day, active, last_payment_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manufacturer' => array(self::BELONGS_TO, 'CamerasManufacturer', 'manufacturer_id'),
			'model' => array(self::BELONGS_TO, 'CamerasModel', 'model_id'),
			'frameRate' => array(self::BELONGS_TO, 'CameraFrameRate', 'frame_rate_id'),
			'resolution' => array(self::BELONGS_TO, 'CameraResolution', 'resolution_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'users' => array(self::HAS_MANY, 'CameraUserAllowed', 'id_camera'),
			'countUsers' => array(self::STAT, 'CameraUserAllowed', 'id_camera'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('labels','Name'),
			'machine_name' => Yii::t('labels','Machine Name'),
			'manufacturer_id' => Yii::t('labels','Manufacturer'),
			'model_id' => Yii::t('labels','Model'),
			'frame_rate_id' => Yii::t('labels','Frame Rate'),
			'resolution_id' => Yii::t('labels','Resolution'),
			'bitrate_id' => 'Bitrate (kbps)',
			'disk_space_per_day' => Yii::t('labels','Disk Space Per Day (Mb)'),
			'archive' => Yii::t('labels','Archive'),
			'archive_days_number' => Yii::t('labels','Archive Days Number'),
			'disk_space' => Yii::t('labels','Disk Space'),
			'IP_address' => Yii::t('labels','Ip Address'),
			'login' => Yii::t('labels','Login'),
			'password' => Yii::t('labels','Password'),
			'port' => Yii::t('labels','Port'),
			'url' => 'Url',
			'user_id' => Yii::t('labels','User'),
			'create_time' => Yii::t('labels','Create Time'),
			'cost_per_day' => Yii::t('labels','Cost Per Day'),
			'allowedUsers' => Yii::t('labels','Allowed Users'),
			'active' => Yii::t('labels','Active'),
			'last_payment_time' => Yii::t('labels','Last Payment Time'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('machine_name',$this->machine_name,true);
		$criteria->compare('manufacturer_id',$this->manufacturer_id);
		$criteria->compare('model_id',$this->model_id);
		$criteria->compare('frame_rate_id',$this->frame_rate_id);
		$criteria->compare('resolution_id',$this->resolution_id);
		$criteria->compare('bitrate_id',$this->bitrate_id);
		$criteria->compare('disk_space_per_day',$this->disk_space_per_day,true);
		$criteria->compare('archive',$this->archive);
		$criteria->compare('archive_days_number',$this->archive_days_number,true);
		$criteria->compare('disk_space',$this->disk_space,true);
		$criteria->compare('IP_address',$this->IP_address,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('port',$this->port);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('cost_per_day',$this->cost_per_day,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('last_payment_time',$this->last_payment_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate()
	{		
		if ($this->isNewRecord) {
			$this->machine_name = Translite::rusencode($this->name, '_', true);
			$this->machine_name .= uniqid("_");
		}
		
		return parent::beforeValidate();
	}
	
// 	public function getAllowedUsers()
// 	{
// 		$out = '';
// 		if (!empty($this->users)) {
// 			foreach ($this->users as $user){
// 				$users[] = $user->username;
// 			}
// 			$out = implode(', ', $users);
// 		}
		
// 		return $out; 
// 	}
	
	public function getAllowedUsers()
	{
		$out = array();
		if (!empty($this->users)) {
			foreach ($this->users as $user){
				$out[]['username'] = $user->username;
			}
			$out = implode(', ', $users);
		}
		
		return $out; 
	}
	
	public function setAllowedUsers($val)
	{
		if (!empty($val)) {
			
			
			$old_users = explode(', ', $this->allowedUsers);
			$users = explode(',', $val);
			foreach ($users as &$item){
				$item = trim($item);
			}
			
			$delUsers = array_diff($old_users, $users);
			
			$addUsers = array_diff($users, $old_users);
			
			if (!empty($addUsers)) {
				foreach($addUsers as $user){
					if ($userModel = User::model()->findByAttributes(array('username'=>$user))) {
						if (!CameraUserAllowed::model()->findByAttributes(array('id_user'=>$userModel->id, 'id_camera'=>$this->id))) {
							$cameraRel = new CameraUserAllowed;
							$cameraRel->id_camera = $this->id;
							$cameraRel->id_user = $userModel->id;
							$cameraRel->save();
						}
					}
				} 
			}
			
			if (!empty($delUsers)) {
// 				echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($delUsers); echo'</pre>';die;
				foreach($delUsers as $user){
					if ($userModel = User::model()->findByAttributes(array('username'=>$user))) {
// 						echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($userModel->id); echo'</pre>';die;
						if ($cameraRel = CameraUserAllowed::model()->findByAttributes(array('id_user'=>$userModel->id, 'id_camera'=>$this->id))) {
							$cameraRel->delete();
						}
					}
				} 
			}
		}	
		
		$this->allowedUsers = $val;
		
		return $this->allowedUsers;
	}
	
	protected function afterSave()
	{
// 		if ($this->getOldAttributeValue('status')==0 && $this->status==1) {
			
// 		}
		if ($this->active==1) {
			$reload = 0;
			
			$changeAttrs = $this->attributesChangedNames();
			
			
			$changeImpAttrs = !empty($changeAttrs) ? array_intersect($this->importantAttrs, $changeAttrs) : array();

			$port = ($this->port != 0) ? ':'.$this->port : '';
			// block create live stream on videoserver
			$text = "stream ".$this->machine_name." {\n";
			$text .= "	url rtsp://".$this->login.":".$this->password."@".$this->IP_address.$port.$this->url.";\n";
			if ($this->archive == 1) {
				$text .= "	dvr /etc/flussonic/storage ".$this->archive_days_number."d;\n";
			}
			$text .= "}\n";
			
			$ssh = new Net_SSH2(Settings::val('vsHost'));

 			if (!$ssh->login(Settings::val('vsRoot'), Settings::val('vsRootPass'))) {
				exit('Login Failed');
			}
			else {
				if ($this->isNewRecord) {
					$ssh->exec('cd '.Settings::val('vsDirectory').' && touch '.$this->machine_name.'.conf && echo "'.$text.'" > '.$this->machine_name.'.conf');
					$reload = 1;
				}
				else {
					if (!($ssh->exec('test -f '.Settings::val('vsDirectory').$this->machine_name.'.conf && echo 1') == 1)) {
						$ssh->exec('cd '.Settings::val('vsDirectory').' && touch '.$this->machine_name.'.conf && echo "'.$text.'" > '.$this->machine_name.'.conf');
						$reload = 1;
					}
					elseif(!empty($changeImpAttrs)){
						$ssh->exec('echo "'.$text.'" > '.Settings::val('vsDirectory').$this->machine_name.'.conf');
						$reload = 1;
					}
				}
			}
			
			if ($reload == 1) {
				$ssh->exec('/etc/init.d/flussonic reload');
			}
			
			// end block
		}
		else {
			if (!$this->isNewRecord && $this->attributeChanged('active')) {
				$text = "%stream ".$this->machine_name." {\n";
				$text .= "%	url rtsp://".$this->login.":".$this->password."@".$this->IP_address.$this->url.";\n";
				if ($this->archive == 1) {
					$text .= "%	dvr /etc/flussonic/storage ".$this->archive_days_number."d;\n";
				}
				$text .= "%}\n";
				
				$ssh = new Net_SSH2(Settings::val('vsHost'));
					
				if (!$ssh->login(Settings::val('vsRoot'), Settings::val('vsRootPass'))) {
					exit('Login Failed');
				}
				
				if ($ssh->exec('test -f '.Settings::val('vsDirectory').$this->machine_name.'.conf && echo 1') == 1) {
					$ssh->exec('echo "'.$text.'" > '.Settings::val('vsDirectory').$this->machine_name.'.conf');
					$ssh->exec('/etc/init.d/flussonic reload');
				}
				else{
// 					$this->attributes = $this->oldAttributes;
					$this->save();
					echo 'Error';
				}
			}
		}
		
		
		return parent::afterSave();
	}
	
	public function autoPay()
	{
		if (UserAccount::checkBalance($this->user_id, $this->cost_per_day)) {
			
			$transaction = new UserAccountTransaction;
			
			$transaction->amount = $this->cost_per_day;
			$transaction->type_transaction_id = 2;
			$transaction->description = 'Оплата дневного тарифа по камере '.$this->name.'('.$this->machine_name.')';
			$transaction->account_id = $this->user->account->id;
			$transaction->payment_type = UserAccountTransaction::PAY_INNER;
			$transaction->status = UserAccountTransaction::STATUS_SUCCESS;
			
			if ($transaction->save()) {
				$this->last_payment_time = $transaction->create_time;
				$this->save();
			}
		}
		else{
			$this->active = 0;
			$this->save();
		}
	}
	
	public function getPayDate()
	{
		$checkDate = date('Y-m-d H:i:s', mktime(date('H')-24));
		
		if ($this->last_payment_time <= $checkDate) {
			return true;
		}
		
		return false;
	}
	
	public function behaviors()
	{
		return array(
				'AttributesBackupBehavior' => 'common.extensions.behaviors.AttributesBackupBehavior',
		);
	}
	
	public function beforeDelete()
	{
		$ssh = new Net_SSH2(Settings::val('vsHost'));
		if (!$ssh->login(Settings::val('vsRoot'), Settings::val('vsRootPass'))) {
			exit('Login Failed');
			return false;
		}
		
		if ($ssh->exec('test -f '.Settings::val('vsDirectory').$this->machine_name.'.conf && echo 1') == 1) {
			if ($ssh->exec('rm '.Settings::val('vsDirectory').$this->machine_name.'.conf && echo 1') == 1) {
				$ssh->exec('/etc/init.d/flussonic reload');
			}
		}
		
		
		return parent::beforeDelete();
	}
	
	public function beforeSave()
	{
		
		return parent::beforeSave();
	}
	
}