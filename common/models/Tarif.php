<?php

/**
 * This is the model class for table "tarif".
 *
 * The followings are the available columns in table 'tarif':
 * @property integer $id
 * @property string $name
 * @property string $machine_name
 * @property integer $archive
 * @property integer $archive_term
 * @property integer $archive_space
 * @property string $archive_cost_per_mb
 * @property string $archive_cost_per_day
 * @property integer $archive_download
 * @property integer $number_cameras
 * @property integer $safe_mode
 * @property integer $local_archive
 * @property integer $embed_mode
 * @property integer $clode_archive
 * @property integer $AVI_export
 * @property integer $another_users_access
 * @property integer $online_broadcasting
 * @property integer $extended_support
 * @property integer $payment_settlement_account
 * @property integer $free_period
 * @property string $cost_camera_per_month
 *
 * The followings are the available model relations:
 * @property TarifCamera[] $tarifCameras
 * @property TarifVideoQuality[] $tarifVideoQualities
 * @property TarifBitrates[] $tarifBitrates
 */
class Tarif extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tarif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, machine_name', 'required'),
			array('archive, archive_term, archive_space, archive_download, number_cameras, safe_mode, local_archive, embed_mode, clode_archive, AVI_export, another_users_access, online_broadcasting, extended_support, payment_settlement_account, free_period', 'numerical', 'integerOnly'=>true),
			array('name, machine_name', 'length', 'max'=>200),
			array('archive_cost_per_mb, cost_camera_per_month, archive_cost_per_day', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, machine_name, archive, archive_term, archive_space, archive_cost_per_mb, archive_cost_per_day, archive_download, number_cameras, safe_mode, local_archive, embed_mode, clode_archive, AVI_export, another_users_access, online_broadcasting, extended_support, payment_settlement_account, free_period, cost_camera_per_month', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
// 			'tarifCameras' => array(self::HAS_MANY, 'TarifCamera', 'tarif_id'),
// 			'tarifVideoQualities' => array(self::HAS_MANY, 'TarifVideoQuality', 'tarif_id'),
 			'tarifBitrates' => array(self::HAS_MANY, 'VideoBitrateRelTarif', 'tarif_id'),
 			'tarifFrameRates' => array(self::HAS_MANY, 'CameraFrameRateRelTarif', 'tarif_id'),
 			'tarifResolutions' => array(self::HAS_MANY, 'CameraResolutionRelTarif', 'tarif_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('labels','Name'),
			'machine_name' => Yii::t('labels','Machine Name'),
// 			'archive' => Yii::t('labels','Archive'),
			'archive' => Yii::t('labels','Archive'),
			'archive_term' => Yii::t('labels','Archive Term'),
			'archive_space' => Yii::t('labels','Archive Space'),
			'archive_cost_per_mb' => 'Archive Cost Per Mb',
			'archive_cost_per_day' => 'Archive Cost Per Day',
			'archive_download' => Yii::t('labels','Archive Download'),
			'number_cameras' => Yii::t('labels','Number Cameras'),
			'safe_mode' => Yii::t('labels','Safe Mode'),
			'local_archive' => Yii::t('labels','Local Archive'),
			'embed_mode' => Yii::t('labels','Embed Mode'),
			'clode_archive' => Yii::t('labels','Cloud Archive'),
			'AVI_export' => Yii::t('labels','AVI Export'),
			'another_users_access' => Yii::t('labels','Another Users Access'),
			'online_broadcasting' => Yii::t('labels','Online Broadcasting'),
			'extended_support' => Yii::t('labels','Extended Support'),
			'payment_settlement_account' => Yii::t('labels','Payment Settlement Account'),
			'free_period' => Yii::t('labels','Free Period'),
			'cost_camera_per_month' => Yii::t('labels','Cost Camera Per Month'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('machine_name',$this->machine_name,true);
		$criteria->compare('archive',$this->archive);
		$criteria->compare('archive_term',$this->archive_term);
		$criteria->compare('archive_space',$this->archive_space);
		$criteria->compare('archive_cost_per_mb',$this->archive_cost_per_mb,true);
		$criteria->compare('archive_cost_per_day',$this->archive_cost_per_day,true);
		$criteria->compare('archive_download',$this->archive_download);
		$criteria->compare('number_cameras',$this->number_cameras);
		$criteria->compare('safe_mode',$this->safe_mode);
		$criteria->compare('local_archive',$this->local_archive);
		$criteria->compare('embed_mode',$this->embed_mode);
		$criteria->compare('clode_archive',$this->clode_archive);
		$criteria->compare('AVI_export',$this->AVI_export);
		$criteria->compare('another_users_access',$this->another_users_access);
		$criteria->compare('online_broadcasting',$this->online_broadcasting);
		$criteria->compare('extended_support',$this->extended_support);
		$criteria->compare('payment_settlement_account',$this->payment_settlement_account);
		$criteria->compare('free_period',$this->free_period);
		$criteria->compare('cost_camera_per_month',$this->cost_camera_per_month,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function listItems()
	{
		$tariffs = self::model()->findAll(array('order'=>'id'));
	
		return CHtml::listData($tariffs, 'id', 'name');
	}
	
	public static function listItensByMachine()
	{
		$tariffs = self::model()->findAll(array('order'=>'id'));
		
		return CHtml::listData($tariffs, 'id', 'machine_name');
	}
	
	public static function listItemExt()
	{
		$tariffs = self::model()->findAll(array('order'=>'id'));
		
		foreach ($tariffs as $tariff){
			$out[$tariff->machine_name] = array('name'=>$tariff->name, 'id'=>$tariff->id, 'val'=>'0.00');
		}
		
		return $out;
	}
	
	public function getSimpleBitrates()
	{
		$out = array();
		
		if(!empty($this->tarifBitrates)){
			foreach($this->tarifBitrates as $bitrate){
				$out[$bitrate->bitrate_id] = $bitrate->bitrate->value;
			}
		}
		
		return $out;
	}
	
}