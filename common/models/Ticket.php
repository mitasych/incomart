<?php

/**
 * This is the model class for table "ticket".
 *
 * The followings are the available columns in table 'ticket':
 * @property string $id
 * @property integer $theme
 * @property string $description
 * @property string $create_time
 * @property string $status
 * @property string $close_time
 * @property string $priority
 * @property integer $user_id
 * @property integer $camera_id
 * @property integer $account_id
 *
 * The followings are the available model relations:
 * @property UserAccount $account
 * @property Camera $camera
 * @property User $user
 * @property TicketMessage[] $ticketMessages
 */
class Ticket extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ticket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ticket';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, description, user_id', 'required'),
			array('theme, user_id, camera_id, account_id', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>36),
			array('status', 'length', 'max'=>5),
			array('status', 'in', 'range'=>array_keys(self::listStatus())),
			array('priority', 'length', 'max'=>6),
			array('priority', 'in', 'range'=>array_keys(self::listPriority())),
			array('create_time, close_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, theme, description, create_time, status, close_time, priority, user_id, camera_id, account_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'account' => array(self::BELONGS_TO, 'UserAccount', 'account_id'),
			'camera' => array(self::BELONGS_TO, 'Camera', 'camera_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'ticketMessages' => array(self::HAS_MANY, 'TicketMessage', 'ticket_id'),
			'lastMessage' => array(self::HAS_ONE, 'TicketMessage', 'ticket_id', 'order'=>'lastMessage.order_in_ticket DESC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'theme' => Yii::t('labels','Theme'),
			'description' => Yii::t('labels','Description'),
			'create_time' => Yii::t('labels','Create Time'),
			'status' => Yii::t('labels','Status'),
			'close_time' => Yii::t('labels','Close Time'),
			'priority' => Yii::t('labels','Priority'),
			'user_id' => Yii::t('labels','User'),
			'camera_id' => Yii::t('labels','Camera'),
			'account_id' => Yii::t('labels','Account'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('theme',$this->theme);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('close_time',$this->close_time,true);
		$criteria->compare('priority',$this->priority,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('camera_id',$this->camera_id);
		$criteria->compare('account_id',$this->account_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function listThemes()
	{
		return array(
				1=>'Вопросы, связанные с камерой',
				2=>'Вопросы, связанные со счётом',
				3=>'Другие вопросы'
			);
	}
	
	public static function listStatus()
	{
		return array(
					'open'=>'Открыт', 
					'close'=>'Закрыт'
				);
	}
	
	public static function listPriority()
	{
		return array(
					'low'=>'низкий',
					'middle'=>'средний',
					'high'=>'высокий'
				);
	}
	
	public function getTextTheme()
	{
		if ($this->theme) {
			$themes = self::listThemes();
			return $themes[$this->theme];
		}
		
		return null;
	}
	
	protected function beforeValidate()
	{
		if ($this->isNewRecord && !$this->id) {
			$this->id = Guid::create_guid();
		}
		return parent::beforeValidate();
	}
	
	protected function getLastMessageOrder()
	{
		if (!is_null($this->lastMessage)) {
			return $this->lastMessage->order_in_ticket;
		}
		
		return 0;
	}
}