<?php

/**
 * This is the model class for table "banner_services".
 *
 * The followings are the available columns in table 'banner_services':
 * @property integer $id
 * @property string $name
 * @property integer $banner_size_id
 * @property string $place
 * @property string $placement_period_item
 * @property integer $placement_period_number
 * @property string $cost
 *
 * The followings are the available model relations:
 * @property BannerSizes $bannerSize
 */
class BannerServices extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BannerServices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'banner_services';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, banner_size_id, place, placement_period_item, placement_period_number, cost', 'required'),
			array('banner_size_id, placement_period_number', 'numerical', 'integerOnly'=>true),
			array('place', 'length', 'max'=>500),
			array('placement_period_item', 'length', 'max'=>100),
			array('name, cost', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, banner_size_id, place, placement_period_item, placement_period_number, cost', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bannerSize' => array(self::BELONGS_TO, 'BannerSizes', 'banner_size_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('labels','Name'),
			'banner_size_id' => Yii::t('labels','Size'),
			'place' => Yii::t('labels','Place'),
			'placement_period_item' => Yii::t('labels','Placement Period Item'),
			'placement_period_number' => Yii::t('labels','Placement Period Number'),
			'cost' => Yii::t('labels','Стоимость'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('banner_size_id',$this->banner_size_id);
		$criteria->compare('place',$this->place,true);
		$criteria->compare('placement_period_item',$this->placement_period_item,true);
		$criteria->compare('placement_period_number',$this->placement_period_number);
		$criteria->compare('cost',$this->cost,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getPlacementItems()
	{
		return array(1=>Yii::t('labels','Week'), 2=>Yii::t('labels','Month'), 3=>Yii::t('labels','Year'));
	}
	
	public static function listItems()
	{
		$ban_services = self::model()->findAll(array('order'=>'id'));
		
		return array_merge(array(0=>'Select'), CHtml::listData($ban_services, 'id', 'name'));
	}
}