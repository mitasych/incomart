<?php

/**
 * This is the model class for table "banner".
 *
 * The followings are the available columns in table 'banner':
 * @property integer $id
 * @property string $name
 * @property integer $size_id
 * @property string $content
 * @property string $create_time
 * @property string $start_time
 * @property string $end_time
 * @property integer $create_user_id
 *
 * The followings are the available model relations:
 * @property bannerSizes $size
 */
class Banner extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Banner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'banner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, size_id, create_time, create_user_id, content', 'required'),
			array('size_id, create_user_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('name', 'match', 'pattern'=>'/[а-Я]+/', 'not'=>true,
				'message'=>'Нельзя использовать русские буквы.'),
			array('content', 'length', 'max'=>1000),
			array('start_time, end_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, size_id, content, create_time, start_time, end_time, create_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'size' => array(self::BELONGS_TO, 'BannerSizes', 'size_id'),
			'createUser' => array(self::BELONGS_TO, 'User', 'create_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('labels','Name'),
			'size_id' => Yii::t('labels','Size'),
			'content' => Yii::t('labels','Content'),
			'create_time' => Yii::t('labels','Create Time'),
			'start_time' => Yii::t('labels','Start Time'),
			'end_time' => Yii::t('labels','End Time'),
			'create_user_id' => Yii::t('labels','Create User'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('size_id',$this->size_id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('create_user_id',$this->create_user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}