<?php

/**
 * This is the model class for table "camera_user_allowed".
 *
 * The followings are the available columns in table 'camera_user_allowed':
 * @property integer $id_camera
 * @property integer $id_user
 * @property string $create_time
 * @property string $end_time
 */
class CameraUserAllowed extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CameraUserAllowed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function primaryKey(){
		return array('id_camera', 'id_user');
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'camera_user_allowed';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_camera, id_user, create_time', 'required'),
			array('id_camera, id_user, timeUse', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>256, 'allowEmpty'=>false),
			array('id_user', 'uniqueRecord'),
			array('end_time, username, timeUse', 'safe'),
			array('username', 'exist', 'attributeName'=>'username', 'className'=>'User'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_camera, id_user, create_time, end_time, username, timeUse', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'camera' => array(self::BELONGS_TO, 'Camera', 'id_camera'),
				'user' => array(self::BELONGS_TO, 'User', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_camera' => Yii::t('labels','Id Camera'),
			'id_user' => Yii::t('labels','Id User'),
			'create_time' => Yii::t('labels','Create Time'),
			'end_time' => Yii::t('labels','End Time'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_camera',$this->id_camera);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('end_time',$this->end_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getUsername() 
	{
		
		if (!is_null($this->user)) {
			return $this->user->username;
		}
		
		return false;
	}
	
	public function setUsername($val)
	{
		$this->username = $val;
		
		return $this->username;
	}

	public function getTimeUse() 
	{
		
// 		if (!is_null($this->user)) {
// 			return $this->user->username;
// 		}
		
		return false;
	}
	
	public function setTimeUse($val)
	{
		$this->timeUse = $val;
		
		return $this->timeUse;
	}
	
	public function uniqueRecord()
	{
		if (CameraUserAllowed::model()->findByPk(array('id_camera'=>$this->id_camera, 'id_user'=>$this->id_user))) {
			$this->addError('username', 'Этот пользователь уже добавлен');
		}
		elseif ($this->id_user == $this->camera->user_id){
			$this->addError('username', 'Вы не можете добавить себя');
		}
	}
	
	public function beforeValidate()
	{
		
		return parent::beforeValidate();
	}

	public function beforeSave()
	{
		$this->end_time = date('Y-m-d H:i:s', mktime(date('H')+($this->timeUse*24)));
		return parent::beforeSave();
	}
}