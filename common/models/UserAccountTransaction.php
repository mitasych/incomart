<?php

/**
 * This is the model class for table "user_account_transaction".
 *
 * The followings are the available columns in table 'user_account_transaction':
 * @property integer $id
 * @property integer $account_id
 * @property integer $type_transaction_id
 * @property string $guid_transaction
 * @property string $description
 * @property integer $status
 * @property integer $status_error
 * @property string $create_time
 * @property string $amount
 * @property integer $payment_type
 *
 * The followings are the available model relations:
 * @property UserAccount $account
 * @property TransactionType $typeTransaction
 * @property TransactionsError $statusError
 */
class UserAccountTransaction extends Details
{
	const STATUS_WAIT = 0;
	const STATUS_SUCCESS = 1;
	const STATUS_ERROR = 2;
	const STATUS_CANCEL = 3;
	
	const PAY_INNER = 0;
	const PAY_DIRECTLY = 1;
	const PAY_EPAY = 2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserAccountTransaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_account_transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, type_transaction_id, guid_transaction, status, create_time, amount, payment_type', 'required'),
			array('account_id, type_transaction_id, status, status_error, payment_type', 'numerical', 'integerOnly'=>true),
			array('guid_transaction, amount', 'length', 'max'=>45),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_id, type_transaction_id, guid_transaction, description, status, status_error, create_time, amount, payment_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'account' => array(self::BELONGS_TO, 'UserAccount', 'account_id'),
			'typeTransaction' => array(self::BELONGS_TO, 'TransactionType', 'type_transaction_id'),
			'statusError' => array(self::BELONGS_TO, 'TransactionsError', 'status_error'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => Yii::t('labels','Account'),
			'type_transaction_id' => Yii::t('labels','Type Transaction'),
			'guid_transaction' => Yii::t('labels','Guid Transaction'),
			'description' => Yii::t('labels','Description'),
			'status' => Yii::t('labels','Status'),
			'status_error' => Yii::t('labels','Status Error'),
			'create_time' => Yii::t('labels','Create Time'),
			'amount' => Yii::t('labels','Amount'),
			'payment_type' => Yii::t('labels','Payment Type'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('type_transaction_id',$this->type_transaction_id);
		$criteria->compare('guid_transaction',$this->guid_transaction,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('status_error',$this->status_error);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('payment_type',$this->payment_type);
		
		if (!isset($_REQUEST['UserAccountTransaction_sort']) || $_REQUEST['UserAccountTransaction_sort'] != 'create_time') {
			$criteria->order = 'create_time DESC';
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function listStatus ()
	{
		return array(
					self::STATUS_WAIT=>Yii::t('main', 'wait'), 
					self::STATUS_SUCCESS=>Yii::t('main', 'success'), 
					self::STATUS_ERROR=>Yii::t('main', 'error'), 
					self::STATUS_CANCEL=>Yii::t('main', 'cancel')
				);
	}
	
	public function getStatusName()
	{
		$name = self::listStatus();
		
		return $name[$this->status];
	}
	
	public function getAmountFormat()
	{
		return $this->amount.' '.$this->account->currency->shortening;
	}
	
	protected function beforeValidate()
	{
		if ($this->isNewRecord) {
			$this->guid_transaction = uniqid('tr_', true);
		}
		
		return parent::beforeValidate();
	}
	
	protected function beforeSave()
	{
		if ($this->payment_type == self::PAY_INNER && $this->type_transaction_id == 2) {
			$this->account->balance -= $this->amount;
			
			$this->account->save();
		}
		return parent::beforeSave();
	}
	
	protected function afterSave()
	{
		if (!$this->isNewRecord && $this->getOldAttributeValue('status')==0 && $this->status==1) {
			$this->account->balance += $this->amount;
			if ($this->account->save()) {
				
				$text = 'Ваш счёт пополнен на '.$this->amount.'тг.';
				
				$message = new YiiMailMessage;
				$message->setBody($text, 'text/html');
				$message->subject = 'Successful payment';
				$message->addTo($this->account->user->email);
				$message->from = 'support@nikosmart.com';
				Yii::app()->mail->send($message);
				
			}
		}
		return parent::afterSave();
	}
	
	public static function paymentLabels()
	{
		return array(
					self::PAY_DIRECTLY => Yii::t('labels','Directly'), 
					self::PAY_EPAY => Yii::t('labels','Bank payment')
				);
	}
	
	public function behaviors() 
	{
		return array(
				'AttributesBackupBehavior' => 'common.extensions.behaviors.AttributesBackupBehavior',
		);
	}
}