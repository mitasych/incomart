<?php

/**
 * This is the model class for table "banner_sizes".
 *
 * The followings are the available columns in table 'banner_sizes':
 * @property integer $id
 * @property string $name
 * @property integer $width
 * @property integer $height
 *
 * The followings are the available model relations:
 * @property Banner[] $banner
 */
class BannerSizes extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BannerSizes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'banner_sizes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, width, height', 'required'),
			array('width, height', 'numerical', 'integerOnly'=>true),
			array('name', 'match', 'pattern'=>'/[а-Я]+/', 'not'=>true,
            	'message'=>'Нельзя использовать русские буквы.'),
			array('name', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, width, height', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'banner' => array(self::HAS_MANY, 'Banner', 'size_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('labels','Name'),
			'width' => Yii::t('labels','Width'),
			'height' => Yii::t('labels','Height'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function listItems()
	{
		$ban_sizes = self::model()->findAll(array('order'=>'id'));
		return CHtml::listData($ban_sizes, 'id', 'name');
	}
	
}