<?php

/**
 * This is the model class for table "camera_resolution_rel_tarif".
 *
 * The followings are the available columns in table 'camera_resolution_rel_tarif':
 * @property integer $tarif_id
 * @property integer $resolution_id
 * @property string $cost_per_day
 */
class CameraResolutionRelTarif extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CameraResolutionRelTarif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'camera_resolution_rel_tarif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tarif_id, resolution_id, cost_per_day', 'required'),
			array('tarif_id, resolution_id', 'numerical', 'integerOnly'=>true),
			array('cost_per_day', 'type', 'type'=>'float'),
// 			array('cost_per_day', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tarif_id, resolution_id, cost_per_day', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tariff'=>array(self::BELONGS_TO, 'Tarif', 'tarif_id'),
			'resolution'=>array(self::BELONGS_TO, 'CameraResolution', 'resolution_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tarif_id' => 'Tarif',
			'resolution_id' => 'Resolution',
			'cost_per_day' => 'Cost Per Day',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tarif_id',$this->tarif_id);
		$criteria->compare('resolution_id',$this->resolution_id);
		$criteria->compare('cost_per_day',$this->cost_per_day,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}