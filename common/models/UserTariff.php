<?php

/**
 * This is the model class for table "user_tariff".
 *
 * The followings are the available columns in table 'user_tariff':
 * @property integer $user_id
 * @property integer $tariff_id
 * @property string $create_time
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Tarif $tariff
 */
class UserTariff extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserTariff the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_tariff';
	}
	
	public function primaryKey(){
		return array('user_id', 'tariff_id', 'create_time');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, tariff_id, create_time', 'required'),
			array('user_id, tariff_id, active', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, tariff_id, create_time, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'tariff' => array(self::BELONGS_TO, 'Tarif', 'tariff_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => Yii::t('labels','User'),
			'tariff_id' => Yii::t('labels','Tariff'),
			'create_time' => Yii::t('labels','Create Time'),
			'active' => Yii::t('labels','Active'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('tariff_id',$this->tariff_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}