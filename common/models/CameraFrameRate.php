<?php

/**
 * This is the model class for table "camera_frame_rate".
 *
 * The followings are the available columns in table 'camera_frame_rate':
 * @property integer $id
 * @property string $frame_rate
 *
 * The followings are the available model relations:
 * @property Camera[] $cameras
 */
class CameraFrameRate extends Details
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CameraFrameRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'camera_frame_rate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('frame_rate', 'required'),
			array('frame_rate', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, frame_rate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rel_camera_models' => array(self::HAS_MANY, 'CameraRelFrameRate', 'frame_rate_id'),
			'tarifsRel' => array(self::HAS_MANY, 'CameraFrameRateRelTarif', 'frame_rate_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'frame_rate' => Yii::t('labels','Frame Rate'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('frame_rate',$this->frame_rate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function listItems()
	{
		$frame_rates = self::model()->findAll(array('order'=>'id'));
		return CHtml::listData($frame_rates, 'id', 'frame_rate');
	}
	
	public function getRelTarifs()
	{
		$arRelTarifs = array();
		
		foreach($this->tarifsRel as $tariff){
			$arRelTarifs[$tariff->tarif_id] = array('label'=>'Тариф '.$tariff->tariff->name, 'value'=>$tariff->cost_per_day);
		}
		return $arRelTarifs;
	}
	
	public function getIsRelTarifs()
	{
		$all = Tarif::listItemExt();
		$is = $this->relTarifs;
	
		if (!empty($is)) {
			foreach($all as $k => &$v){
				if (isset($is[$v['id']])) {
					$v['val'] = $is[$v['id']]['value'];
				}
				else{
					$v['val'] = 0;
				}
// 				$v['val'] = $is[$v['id']]['value'];
				// 			$v['name'] = $is[$v['id']]['label'];
			}
		}
	
		return $all;
	}
	
	public function getRelTarifByTarifId($id)
	{
		foreach($this->isRelTarifs as $tarif){
			if ($tarif['id'] == $id) {
				return $tarif['val'];
			}
		}
		return 0;
	}
	
}