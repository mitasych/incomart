<?php

/**
 * This is the model class for table "user_account".
 *
 * The followings are the available columns in table 'user_account':
 * @property integer $id
 * @property integer $user_id
 * @property string $create_time
 * @property integer $currency_id
 * @property string $balance
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Currency $currency
 * @property UserAccountTransaction[] $userAccountTransactions
 */
class UserAccount extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, create_time, currency_id', 'required'),
			array('user_id, currency_id', 'numerical', 'integerOnly'=>true),
			array('balance', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, create_time, currency_id, balance', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'currency' => array(self::BELONGS_TO, 'Currency', 'currency_id'),
			'userAccountTransactions' => array(self::HAS_MANY, 'UserAccountTransaction', 'account_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => Yii::t('labels','User'),
			'create_time' => Yii::t('labels','Create Time'),
			'currency_id' => Yii::t('labels','Currency'),
			'balance' => Yii::t('labels','Balance'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('currency_id',$this->currency_id);
		$criteria->compare('balance',$this->balance,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function checkBalance($user_id, $summ)
	{
		$account = self::model()->findByAttributes(array('user_id'=>$user_id));
		
		if ($account->balance >= $summ) {
			return true;
		}
		
		return false;
	}
	
	
}