<?php

/**
 * This is the model class for table "video_bitrate".
 *
 * The followings are the available columns in table 'video_bitrate':
 * @property integer $id
 * @property string $value
 *
 * The followings are the available model relations:
 * @property Tarif[] $tarifs
 */
class VideoBitrate extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VideoBitrate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video_bitrate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('value', 'required'),
			array('value', 'length', 'max'=>45),
			array('value', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'tarifs' => array(self::MANY_MANY, 'Tarif', 'video_bitrate_rel_tarif(bitrate_id, tarif_id)'),
			'tarifsRel' => array(self::HAS_MANY, 'VideoBitrateRelTarif', 'bitrate_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'value' => Yii::t('labels','Value (kbps)'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getRelTarifs()
	{
		$arRelTarifs = array();
		foreach($this->tarifsRel as $tariff){
			$arRelTarifs[$tariff->tarif_id] = array('label'=>'Тариф '.$tariff->tariff->name, 'value'=>$tariff->cost_per_day);
		}
		return $arRelTarifs;
	}
	
	public function getIsRelTarifs()
	{
		$all = Tarif::listItemExt();
		$is = $this->relTarifs;
		
		
		if (!empty($is)) {
			foreach($all as $k => &$v){
				if (isset($is[$v['id']])) {
					$v['val'] = $is[$v['id']]['value'];
				}
				else{
					$v['val'] = 0;
				}
// 				$v['val'] = $is[$v['id']]['value'];
			}
		}
		
		return $all;
	}
	
	public static function listItems()
	{
		$res = self::model()->findAll(array('order'=>'id'));
		return CHtml::listData($res, 'id', 'value');
	}
	
	public function getRelTarifByTarifId($id)
	{
		foreach($this->isRelTarifs as $tarif){
			if ($tarif['id'] == $id) {
				return $tarif['val'];
			}
		}
		return 0;
	}
	
}