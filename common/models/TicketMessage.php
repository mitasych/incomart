<?php

/**
 * This is the model class for table "ticket_message".
 *
 * The followings are the available columns in table 'ticket_message':
 * @property string $id
 * @property string $ticket_id
 * @property integer $order_in_ticket
 * @property string $previous_id
 * @property string $type
 * @property integer $user_id
 * @property string $create_time
 * @property string $message
 * @property string $ask_id
 *
 * The followings are the available model relations:
 * @property TicketMessage $ask
 * @property TicketMessage[] $ticketMessages
 * @property User $user
 * @property Ticket $ticket
 */
class TicketMessage extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TicketMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ticket_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, ticket_id, type, user_id, create_time, message', 'required'),
			array('order_in_ticket, user_id', 'numerical', 'integerOnly'=>true),
			array('id, ticket_id, previous_id, ask_id', 'length', 'max'=>36),
			array('type', 'length', 'max'=>8),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ticket_id, order_in_ticket, previous_id, type, user_id, create_time, message, ask_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ask' => array(self::BELONGS_TO, 'TicketMessage', 'ask_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'ticket' => array(self::BELONGS_TO, 'Ticket', 'ticket_id'),
			'ticketMessages' => array(self::HAS_MANY, 'TicketMessage', 'ask_id'),
			'lastAnswer' => array(self::HAS_ONE, 'TicketMessage', 'ask_id', 'order'=>'lastAnswer.order_in_ticket DESC'),
			'countAnswer' => array(self::STAT, 'TicketMessage', 'ask_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ticket_id' => Yii::t('labels','Ticket'),
			'order_in_ticket' => Yii::t('labels','Order In Ticket'),
			'previous_id' => Yii::t('labels','Previous'),
			'type' => Yii::t('labels','Type'),
			'user_id' => Yii::t('labels','User'),
			'create_time' => Yii::t('labels','Create Time'),
			'message' => Yii::t('labels','Message'),
			'ask_id' => Yii::t('labels','Ask'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ticket_id',$this->ticket_id,true);
		$criteria->compare('order_in_ticket',$this->order_in_ticket);
		$criteria->compare('previous_id',$this->previous_id,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('ask_id',$this->ask_id,true);
// 		if (!empty($this->ticket_id)) {
// 			echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($criteria->order); echo'</pre>';die;
// 			$criteria->order = 'order_in_ticket';
// 		}
		
// 		echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($criteria); echo'</pre>';die;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeValidate()
	{
		if ($this->isNewRecord && !$this->id) {
			$this->id = Guid::create_guid();
		}
		return parent::beforeValidate();
	}
	
	protected function beforeSave()
	{
		if ($this->isNewRecord) {
			if (empty($this->ask_id)) {
				$this->order_in_ticket += $this->ticket->lastMessageOrder;
			}
			else{
				$lastOrder = $this->ask->order_in_ticket;
				
				if ($this->ask->countAnswer>0) {
					$lastOrder = $this->ask->lastAnswer->order_in_ticket;
				}
				$criteria = new CDbCriteria;
				$criteria->condition = 'ticket_id = :ticket_id';
				$criteria->addCondition('order_in_ticket > :order_in_ticket');
				$criteria->params = array(
								':ticket_id'=>$this->ticket_id, 
								':order_in_ticket' => $lastOrder
							);
				
				$underAskMessages = TicketMessage::model()->findAll($criteria);
				
				if (!empty($underAskMessages)) {
					foreach ($underAskMessages as $message) {
						$message->order_in_ticket += $message->order_in_ticket;
						$message->save();
					}
				}
				$this->order_in_ticket += $lastOrder;
			}
		}
		
		return parent::beforeSave();
	}
}