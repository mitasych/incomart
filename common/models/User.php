<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $activkey
 * @property string $create_time
 * @property string $lastvisit_at
 * @property integer $superuser
 * @property integer $status
 * @property integer $login_attempts
 * @property string $sess_id
 *
 * The followings are the available model relations:
 * @property UserProfile $userProfile
 *
 */
class User extends Details
{
	
	const SCENARIO_SIGNUP = 'signup';
	const SCENARIO_ACTIVATE = 'activate';
	
	public $password_repeat;
	public $verifyCode;
	public $password_reset = 0;
	public $role;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email', 'required'),
			array('username', 'unique'),
			array('superuser, status, password_reset, login_attempts', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>20),
			array('password', 'length', 'min'=>6, 'max'=>30),
			array('password_repeat, email', 'required', 'on'=>self::SCENARIO_SIGNUP),
			array('password_repeat', 'length', 'min'=>6, 'max'=>30),
			array('password', 'compare', 'compareAttribute'=>'password_repeat', 'on'=>self::SCENARIO_SIGNUP),
			array('email', 'email', 'on'=>self::SCENARIO_SIGNUP),
			array('verifyCode', 'validateCaptcha', 'on'=>self::SCENARIO_SIGNUP),
			array('email', 'length', 'min'=>6, 'max'=>50),
			array('email', 'unique'),
			array('password, email, activkey', 'length', 'max'=>128),
			array('sess_id', 'length', 'max'=>32),
			array('create_time, lastvisit_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('username, password, email, activkey, create_time, lastvisit_at, superuser, status, role, login_attempts, sess_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'isRole' => array(self::HAS_MANY, 'AuthAssignment', 'userid'),
			'profile' => array(self::HAS_ONE, 'UserProfile', 'user_id'),
			'tariff' => array(self::HAS_ONE, 'UserTariff', 'user_id', 'on'=>'tariff.active = 1'),
			'account' => array(self::HAS_ONE, 'UserAccount', 'user_id'),
			'cameras' => array(self::MANY_MANY, 'Camera', 'camera_user_allowed(id_user, id_camera)'),
			'selfCameras'=>array(self::HAS_MANY, 'Camera', 'user_id'),
			'countSelfCameras'=>array(self::STAT, 'Camera', 'user_id'),
			'sess' => array(self::BELONGS_TO, 'UserSession', 'sess_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => Yii::t('labels','User name'),
			'role' => Yii::t('labels', 'Roles'),
			'password' => Yii::t('labels','Password'),
			'email' => Yii::t('labels','Email'),
			'activkey' => Yii::t('labels','Active key'),
			'create_time' => Yii::t('labels','Date of registration'),
			'lastvisit_at' => Yii::t('labels','Last Visit'),
			'superuser' => Yii::t('labels','Super user'),
			'status' => Yii::t('labels','Status'),
			'password_repeat' => Yii::t('labels','Password Repeat'),
			'password_reset' => Yii::t('labels','Password Reset'),
			'verifyCode' => Yii::t('labels', 'Verify Code'),
			'login_attempts' => Yii::t('labels', 'Login Attempts'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;
		
		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('activkey',$this->activkey,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('lastvisit_at',$this->lastvisit_at,true);
		$criteria->compare('superuser',$this->superuser);
		$criteria->compare('status',$this->status);
		$criteria->compare('login_attempts',$this->login_attempts);
		
		$criteria->with = array('isRole');
		if (!is_null($this->role)) {
			$criteria->together = true;
		}
		$criteria->compare('isRole.itemname',$this->role, true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
// 			'pagination' => array(
// 					'pageSize' => 10,
// 			)
		));
	}
	
	protected function afterValidate()
	{
			
		if ($this->isNewRecord || $this->password_reset == 1) {
			$this->password = crypt($this->password, $this->password);
		}
		parent::afterValidate();
	}
	
	public function validateCaptcha($attribute, $params) 
	{
		if ($this->getRequireCaptcha())
			CValidator::createValidator('captcha', $this, $attribute, $params)->validate($this);
	}
	
	public function getRequireCaptcha() 
	{
// 		return ($user = $this->user) !== null && $user->login_attempts >= self::MAX_LOGIN_ATTEMPTS;
		return true;
	}
	
	public function generateActiveCode()
	{
		return md5(uniqid());
	}
	
	public function generatePass()
	{
		$length = 12;
		$chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
		shuffle($chars);
		$password = implode(array_slice($chars, 0, $length));
		
		return $password;
	}
	

	public static function listItems()
	{
		$listUser = self::model()->findAll(array(
				'order'=>'id',
		));
		return CHtml::listData($listUser, 'id', 'username');
	}
	
	public function getRoles() 
	{
		return !empty($this->isRole) ? CHtml::listData($this->isRole, 'itemname', 'itemname') : array('authorized');
	}
	
	public function getStrRoles() 
	{
// 		return is_array($this->roles) ? implode(', ', $this->roles) : $this->roles;
		return implode(', ', $this->roles);
	}
	
	public function getStatuses()
	{
		return array(
				0 => Yii::t('labels','unactivated'), 
				1 => Yii::t('labels','activated'), 
				2 => Yii::t('labels','banned')
			);
	}
	
	public function getUserType()
	{
		if (in_array('customer', $this->roles)) {
			return 'customer';
		}
		elseif (in_array('reseller', $this->roles)) {
			return 'reseller';
		}
		return false;
	}
	
	public function getCustomerTypes()
	{
		return array('customer', 'reseller');
	}
	
	public function getAvailableCameras()
	{
		$out = array();
		if (!empty($this->cameras)) {
			foreach ($this->cameras as $camera){
				$out[] = $camera->id;
			}
		}
		
		return $out; 
	}
	
	public function loadNames($keyword){
		$tags=$this->findAll(array(
				'condition'=>'username LIKE :keyword',
				'params'=>array(
						':keyword'=>$keyword.'%',
				)
		));
		return $tags;
	}
	
	public function getCountAllowedUsers()
	{
		$number = 0;
		if (!empty($this->selfCameras)) {
			foreach ($this->selfCameras as $camera){
				$number += $camera->countUsers;
			} 
		}
		return $number;
	}
	
	public function afterFind() 
	{
		if (is_null($this->tariff)) {
			$tariff = new UserTariff;
			
			$tariffs = Tarif::listItems();
			
			$tariff->user_id = $this->id;
			$tariff->tariff_id = key($tariffs);
			$tariff->active = 1;
			$tariff->save();
		}
	}
}