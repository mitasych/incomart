<?php

/**
 * This is the model class for table "user_profile".
 *
 * The followings are the available columns in table 'user_profile':
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $city
 * @property string $icq
 * @property string $skype
 * @property string $photo
 * @property string $about
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserProfile extends Details
{
	public $email;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
// 			array('user_id', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, city, icq, skype, photo', 'length', 'max'=>100),
			array('about', 'length', 'max'=>1000),
			array('email', 'email'),
			array('email', 'unique', 'allowEmpty' => false, 'attributeName' => 'email', 'className' => 'User', 'criteria' => array(
                     'condition' => 'id <> :user_id',
                     'params' => array(':user_id' => $this->user_id))
					),
// 			array('email', 'unique', 'allowEmpty' => false, 'attributeName' => 'email', 'className' => 'User'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, first_name, last_name, city, icq, skype, photo, about', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => Yii::t('labels','User'),
			'first_name' => Yii::t('labels','First Name'),
			'last_name' => Yii::t('labels','Last Name'),
			'city' => Yii::t('labels','City'),
			'icq' => 'Icq',
			'skype' => 'Skype',
			'photo' => Yii::t('labels','Photo'),
			'about' => Yii::t('labels','About Me'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('icq',$this->icq,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('about',$this->about,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeSave()
	{
// 		echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($this->user_id); echo'</pre>';die;
		if (!$this->isNewRecord) {
			User::model()->updateByPk($this->user_id,array('email'=>$this->email));
		}
		return parent::beforeSave();
	}
	
	protected function afterFind()
	{
		$this->email = $this->user->email;
		return parent::afterFind();
	}
	
	public function getProfileByUser()
	{
		return self::model()->find(array(
				'select'=>'*',
				'condition'=>'user_id=:userID',
				'params'=>array(':userID'=>Yii::app()->user->id)
				));
	}
	
	public function getProfileByID($id)
	{
		return self::model()->find(array(
				'select'=>'*',
				'condition'=>'user_id=:userID',
				'params'=>array(':userID'=>$id)
				));
	}
}