<?php

/**
 * This is the model class for table "camera_rel_frame_rate".
 *
 * The followings are the available columns in table 'camera_rel_frame_rate':
 * @property integer $frame_rate_id
 * @property integer $camera_id
 */
class CameraRelFrameRate extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CameraRelFrameRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'camera_rel_frame_rate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('frame_rate_id, camera_id', 'required'),
			array('frame_rate_id, camera_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('frame_rate_id, camera_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'camera_models' => array(self::BELONGS_TO, 'CamerasModel', 'camera_model_id'),
				'frame_rate' => array(self::BELONGS_TO, 'CameraFrameRate', 'frame_rate_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'frame_rate_id' => Yii::t('labels','Frame Rate'),
			'camera_id' => Yii::t('labels','Camera'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('frame_rate_id',$this->frame_rate_id);
		$criteria->compare('camera_id',$this->camera_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
}