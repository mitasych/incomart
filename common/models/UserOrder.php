<?php

/**
 * This is the model class for table "user_order".
 *
 * The followings are the available columns in table 'user_order':
 * @property string $id
 * @property integer $transaction_id
 * @property integer $user_id
 * @property string $create_time
 * @property string $amount
 * @property string $currency
 * @property integer $response_code
 * @property string $customer_name
 * @property integer $error
 * @property string $error_type
 * @property string $error_message
 * @property integer $error_code
 *
 * The followings are the available model relations:
 * @property UserAccountTransaction $transaction
 * @property User $user
 */
class UserOrder extends Details
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transaction_id, user_id, create_time, amount', 'required'),
			array('transaction_id, user_id, error', 'numerical', 'integerOnly'=>true),
			array('response_code, error_code', 'length', 'max'=>2),
			array('amount, currency', 'length', 'max'=>45),
			array('customer_name, error_message', 'length', 'max'=>256),
			array('error_type', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, transaction_id, user_id, create_time, amount, currency, response_code, customer_name, error, error_type, error_message, error_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transaction' => array(self::BELONGS_TO, 'UserAccountTransaction', 'transaction_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'transaction_id' => 'Transaction',
			'user_id' => 'User',
			'create_time' => 'Create Time',
			'amount' => 'Amount',
			'currency' => 'Currency',
			'response_code' => 'Response Code',
			'customer_name' => 'Customer Name',
			'error' => 'Error',
			'error_type' => 'Error Type',
			'error_message' => 'Error Message',
			'error_code' => 'Error Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('transaction_id',$this->transaction_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('response_code',$this->response_code);
		$criteria->compare('customer_name',$this->customer_name,true);
		$criteria->compare('error',$this->error);
		$criteria->compare('error_type',$this->error_type,true);
		$criteria->compare('error_message',$this->error_message,true);
		$criteria->compare('error_code',$this->error_code);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function afterSave()
	{
		if(is_null($this->error) && $this->response_code == '00'){
			$this->transaction->status=1;
			$this->transaction->save();
		}
	
		return parent::afterSave();
	}
}