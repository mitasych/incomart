
<script type="text/javascript">
	function previousMonth()
	{
		var data = "<?php echo $this->previousMonthLink; ?>";
		selectMonth(data);
	}
	
	function nextMonth()
	{
		var data = "<?php echo $this->nextMonthLink; ?>";
		selectMonth(data);
	}
	function selectMonth(data)
	{
			$.ajax({  
				type: "POST",
				url: "/site/selectMonth",
				data: data,
				success: function(ajax){ 
		            $("#calendar").html(ajax);
		        },
		       dataType: "html",
		    });
		return false;
	}
</script>

		<table id="calendar">
		    <thead>
		        <tr class="month-year-row">
		            <th class="previous-month">
						<?php echo CHtml::link(CHtml::image(Yii::app()->theme->baseUrl."/img/calendar_l.jpg"), 
		            		"#".$this->previousMonthLink, array('onClick'=>"previousMonth()"));
		            	?>
		            </th>
		            <th colspan="5" style="text-transform: capitalize;">
		            	<?php echo $this->monthName.' '.$this->year; ?>
					</th>
		            <th class="next-month">
		            	<?php echo CHtml::link(CHtml::image(Yii::app()->theme->baseUrl."/img/calendar_r.jpg"), 
		            		"#".$this->nextMonthLink, array('onClick'=>"nextMonth()"));
		            	?>
		            </th>
		        </tr>
		        <tr class="weekdays-row">
		            <?php foreach(Yii::app()->locale->getWeekDayNames('abbreviated', true) as $weekDay): ?>
		                <th><?php echo $weekDay; ?></th>
		            <?php endforeach; ?>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		        <?php $daysStarted = false; $day = 1; ?>
		        <?php for($i = 1; $i <= $this->daysInCurrentMonth+$this->firstDayOfTheWeek; $i++): ?>
		            <?php if(!$daysStarted) $daysStarted = ($i == $this->firstDayOfTheWeek+1); ?>
		            <td <?php if($day == $this->day) echo 'class="calendar-selected-day"'; ?>
		            	<?php if($this->getExistPost($day)) echo 'style="font-weight: bold"'; ?>>
		                <?php if($daysStarted && $day <= $this->daysInCurrentMonth): ?>
		                    <?php echo CHtml::link($day, $this->getDayLink($day)); $day++; ?>
		                <?php endif; ?>
		            </td>
		            <?php if($i % 7 == 0): ?>
		                </tr><tr>
		            <?php endif; ?>
		        <?php endfor; ?>
		        </tr>
		    </tbody>
		</table>
