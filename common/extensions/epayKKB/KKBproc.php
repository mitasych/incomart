<?php

class KKBproc {

	function process_XML($filename,$reparray) {

		$filename = dirname(__FILE__).'/'.$filename;

		if(is_file($filename)){
			$content = file_get_contents($filename);
			foreach ($reparray as $key => $value) {
				$content = str_replace("[".$key."]",$value,$content);
			}
			return $content;
		}
		else
		{
			return "[ERROR]";
		}
	}

	function split_sign($xml,$tag){


		$array = array();
		$letterst = stristr($xml,"<".$tag);
		$signst = stristr($xml,"<".$tag."_SIGN");
		$signed = stristr($xml,"</".$tag."_SIGN");
		$doced = stristr($signed,">");
		$array['LETTER'] = substr($letterst,0,-strlen($signst));
		$array['SIGN'] = substr($signst,0,-strlen($doced)+1);
		$rawsignst = stristr($array['SIGN'],">");
		$rawsigned = stristr($rawsignst,"</");
		$array['RAWSIGN'] = substr($rawsignst,1,-strlen($rawsigned));
		return $array;
	}

	public function process_request($order_id,$currency_code,$amount,$config_file = 'main.cfg',$b64=true) {

		$config_file = dirname(__FILE__).'/'.$config_file;

		if(is_file($config_file)){
			$config=parse_ini_file($config_file,0);
		}
		else
		{
			return "Config not exist";
		}

		if (strlen($order_id)>0){
			if (is_numeric($order_id)){
				if ($order_id>0){
// 					$order_id = sprintf ("%06d",$order_id);
					$order_id = $order_id;
				} else { return "Null Order ID";
				}
			} else { return "Order ID must be number";
			}
		} else { return "Empty Order ID";
		}

		if (strlen($currency_code)==0){
			return "Empty Currency code";
		}
		if ($amount==0){
			return "Nothing to charge";
		}
		if (strlen($config['PRIVATE_KEY_FN'])==0){
			return "Path for Private key not found";
		}
		if (strlen($config['XML_TEMPLATE_FN'])==0){
			return "Path for Private key not found";
		}

		$request = array();
		$request['MERCHANT_CERTIFICATE_ID'] = $config['MERCHANT_CERTIFICATE_ID'];
		$request['MERCHANT_NAME'] = $config['MERCHANT_NAME'];
		$request['ORDER_ID'] = $order_id;
		$request['CURRENCY'] = $currency_code;
		$request['MERCHANT_ID'] = $config['MERCHANT_ID'];
		$request['AMOUNT'] = $amount;

		$kkb = new KKBsign();
		$kkb->invert();
		if (!$kkb->load_private_key($config['PRIVATE_KEY_FN'],$config['PRIVATE_KEY_PASS'])){
			if ($kkb->ecode>0){
				return $kkb->estatus;
			}
		}

		$result = $this->process_XML($config['XML_TEMPLATE_FN'],$request);
		if (strpos($result,"[RERROR]")>0){
			return "Error reading XML template.";
		}
		$result_sign = '<merchant_sign type="RSA">'.$kkb->sign64($result).'</merchant_sign>';
		$xml = "<document>".$result.$result_sign."</document>";
		if ($b64){
			return base64_encode($xml);
		} else {return $xml;
		}
	}
	// -----------------------------------------------------------------------------------------------
	function process_response($response,$config_file = 'main.cfg') {

		$config_file = dirname(__FILE__).'/'.$config_file;

		if(is_file($config_file)){
			$config=parse_ini_file($config_file,0);
		} 
		else {
			$data["ERROR"] = "Config not exist";$data["ERROR_TYPE"] = "ERROR"; return $data;
		}

		$xml_parser = new KKBxml();
		$result = $xml_parser->parse($response);
// 		echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($result); echo'</pre>';die;
		if (in_array("ERROR",$result)){
			return $result;
		}
		if (in_array("DOCUMENT",$result)){
			$kkb = new KKBsign();
			$kkb->invert();
			$data = $this->split_sign($response,"BANK");
			$check = $kkb->check_sign64($data['LETTER'], $data['RAWSIGN'], $config['PUBLIC_KEY_FN']);
			if ($check == 1)
				$data['CHECKRESULT'] = "[SIGN_GOOD]";
			elseif ($check == 0)
			$data['CHECKRESULT'] = "[SIGN_BAD]";
			else
				$data['CHECKRESULT'] = "[SIGN_CHECK_ERROR]: ".$kkb->estatus;
			return array_merge($result,$data);
		}
		return "[XML_DOCUMENT_UNKNOWN_TYPE]";
	}
	// -----------------------------------------------------------------------------------------------
	function process_refund($reference, $approval_code, $order_id, $currency_code, $amount, $reason, $config_file) {

		if(!$reference) return "Empty Transaction ID";

		if(is_file($config_file)){
			$config=parse_ini_file($config_file,0);
		} else { return "Config not exist";
		}

		if (strlen($order_id)>0){
			if (is_numeric($order_id)){
				if ($order_id>0){
// 					$order_id = sprintf ("%06d",$order_id);
					$order_id = $order_id;
				} else { return "Null Order ID";
				}
			} else { return "Order ID must be number";
			}
		} else { return "Empty Order ID";
		}

		if(!$reason) $reason = "Transaction revert";

		if (strlen($currency_code)==0){
			return "Empty Currency code";
		}
		if ($amount==0){
			return "Nothing to charge";
		}
		if (strlen($config['PRIVATE_KEY_FN'])==0){
			return "Path for Private key not found";
		}
		if (strlen($config['XML_COMMAND_TEMPLATE_FN'])==0){
			return "Path to xml command template not found";
		}

		$request = array();
		$request['MERCHANT_ID'] = $config['MERCHANT_ID'];
		$request['MERCHANT_NAME'] = $config['MERCHANT_NAME'];
		$request['COMMAND'] = 'reverse';
		$request['REFERENCE_ID'] = $reference;
		$request['APPROVAL_CODE'] = $approval_code;
		$request['ORDER_ID'] = $order_id;
		$request['CURRENCY'] = $currency_code;
		$request['MERCHANT_ID'] = $config['MERCHANT_ID'];
		$request['AMOUNT'] = $amount;
		$request['REASON'] = $reason;

		$kkb = new KKBsign();
		$kkb->invert();
		if (!$kkb->load_private_key($config['PRIVATE_KEY_FN'],$config['PRIVATE_KEY_PASS'])){
			if ($kkb->ecode>0){
				return $kkb->estatus;
			}
		}

		$result = $this->process_XML($config['XML_COMMAND_TEMPLATE_FN'],$request);
		if (strpos($result,"[RERROR]")>0){
			return "Error reading XML template.";
		}
		$result_sign = '<merchant_sign type="RSA" cert_id="' . $config['MERCHANT_CERTIFICATE_ID'] . '">'.$kkb->sign64($result).'</merchant_sign>';
		$xml = "<document>".$result.$result_sign."</document>";
		return $xml;
	}
	// -----------------------------------------------------------------------------------------------
	function process_complete($reference, $approval_code, $order_id, $currency_code, $amount, $config_file) {

		if(!$reference) return "Empty Transaction ID";

		if(is_file($config_file)){
			$config=parse_ini_file($config_file,0);
		} else { return "Config not exist";
		}

		if (strlen($order_id)>0){
			if (is_numeric($order_id)){
				if ($order_id>0){
					$order_id = $order_id;
// 					$order_id = sprintf ("%06d",$order_id);
				} else { return "Null Order ID";
				}
			} else { return "Order ID must be number";
			}
		} else { return "Empty Order ID";
		}

		if (strlen($currency_code)==0){
			return "Empty Currency code";
		}
		if ($amount==0){
			return "Nothing to charge";
		}
		if (strlen($config['PRIVATE_KEY_FN'])==0){
			return "Path for Private key not found";
		}
		if (strlen($config['XML_COMMAND_TEMPLATE_FN'])==0){
			return "Path for xml command template not found";
		}

		$request = array();
		$request['MERCHANT_ID'] = $config['MERCHANT_ID'];
		$request['MERCHANT_NAME'] = $config['MERCHANT_NAME'];
		$request['COMMAND'] = 'complete';
		$request['REFERENCE_ID'] = $reference;
		$request['APPROVAL_CODE'] = $approval_code;
		$request['ORDER_ID'] = $order_id;
		$request['CURRENCY'] = $currency_code;
		$request['MERCHANT_ID'] = $config['MERCHANT_ID'];
		$request['AMOUNT'] = $amount;
		$request['REASON'] = '';

		$kkb = new KKBsign();
		$kkb->invert();
		if (!$kkb->load_private_key($config['PRIVATE_KEY_FN'],$config['PRIVATE_KEY_PASS'])){
			if ($kkb->ecode>0){
				return $kkb->estatus;
			}
		}

		$result = $this->process_XML($config['XML_COMMAND_TEMPLATE_FN'],$request);
		if (strpos($result,"[RERROR]")>0){
			return "Error reading XML template.";
		}
		$result_sign = '<merchant_sign type="RSA" cert_id="' . $config['MERCHANT_CERTIFICATE_ID'] . '">'.$kkb->sign64($result).'</merchant_sign>';
		$xml = "<document>".$result.$result_sign."</document>";
		return $xml;
	}
}
