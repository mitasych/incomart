<?php

class RegularpayCommand extends CConsoleCommand {
	public function run($args) {
		
		$checkDate = date('Y-m-d H:i:s', mktime(date('H')-24));
		
		$cameras = Camera::model()->findAll(
				'active = 1 AND last_payment_time <= :date', 
				array(':date' => $checkDate));
		
		foreach ($cameras as $camera){
			$camera->autoPay();
		}
	}
}
