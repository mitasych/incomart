<?php

return CMap::mergeArray(
		require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main.php'),
		array(
				"basePath" => 'console',
				'name' => 'Cron',
				"components" => array(
						'log' => array(
								'class' => 'CLogRouter',
								'routes' => array(
										array(
												'class' => 'CFileLogRoute',
												'logFile' => 'cron.log',
												'levels' => 'error, warning',
										),
										array(
												'class' => 'CFileLogRoute',
												'logFile' => 'cron_trace.log',
												'levels' => 'trace',
										),
								),
						),
						'db' => array(
								'connectionString' => $params['db.connectionString'],
								'username' => $params['db.username'],
								'password' => $params['db.password'],
								'schemaCachingDuration' => YII_DEBUG ? 0 : 86400000, // 1000 days
								'enableParamLogging' => YII_DEBUG,
								'charset' => 'utf8'
						),
				)
		)
);

