<?php

$baseFront = (__DIR__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..';
$ruMesDir = $baseFront.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'common/messages/ru/';
$ruCommonFile = $ruMesDir.'menu.php';

$ruCommonArray = file_exists($ruCommonFile) ? require($ruCommonFile) : array();

return $ruCommonArray;


?>