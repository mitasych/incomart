<?php

class AuthItemController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
// 	public function filters()
// 	{
// 		return array(
// // 			'accessControl', // perform access control for CRUD operations
// // 			'postOnly + delete', // we only allow deletion via POST request
// 			'rights'
// 		);
// 	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AuthItem;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AuthItem']))
		{
			$authData = Yii::app()->request->getPost('AuthItem');
			$model->attributes=$authData;
			$model->type=intval($model->type);
			
			$auth=Yii::app()->authManager;
			
			if($auth->createAuthItem($model->name,$model->type,$model->description,$model->bizrule,$model->data)){
				if (!empty($authData['childItem'])) {
					$auth->addItemChild($model->name, $authData['childItem']);
				}
				$this->redirect(array('view','id'=>$model->name));
			}
// 			if($model->save())
// 				$this->redirect(array('view','id'=>$model->name));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		$model=AuthItem::model()->findByPk($_GET['id']);
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AuthItem']))
		{
			$model->attributes=$_POST['AuthItem'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->name));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AuthItem');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AuthItem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AuthItem']))
			$model->attributes=$_GET['AuthItem'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=AuthItem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='auth-item-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionGenerate()
	{
		// Get the generator and authorizer
		$generator = new RGenerator();
		// Createh the form model
		$model = new GenerateForm();
	
		// Form has been submitted
		if( isset($_POST['GenerateForm'])===true )
		{
// 			echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($_POST['GenerateForm']); echo'</pre>';die;
			// Form is valid
			$model->attributes = $_POST['GenerateForm'];
			if( $model->validate()===true )
			{
				$items = array(
						'tasks'=>array(),
						'operations'=>array(),
				);
	
				// Get the chosen items
				foreach( $model->items as $itemname=>$value )
				{
					if( (bool)$value===true )
					{
						if( strpos($itemname, '*')!==false )
							$items['tasks'][] = $itemname;
						else
							$items['operations'][] = $itemname;
					}
				}
	
				// Add the items to the generator as tasks and operations and run the generator.
				$generator->addItems($items['tasks'], CAuthItem::TYPE_TASK);
				$generator->addItems($items['operations'], CAuthItem::TYPE_OPERATION);
				if( ($generatedItems = $generator->run())!==false && $generatedItems!==array() )
				{
					Yii::app()->getUser()->setFlash('success',
							Yii::t('core', 'Authorization items created.')
					);
					$this->redirect(array('authItem/permissions'));
				}
			}
		}
	
		// Get all items that are available to be generated
		$items = $generator->getControllerActions();
	
		// We need the existing operations for comparason
		
		$authItemsModels = AuthItem::model()->findAll('type=0');
// 		echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump(app()->authManager); echo'</pre>';die;
		foreach($authItemsModels as $auModel){
			$authItems[$auModel->name] = new CAuthItem(app()->authManager, $auModel->name, $auModel->type, $auModel->description, $auModel->bizrule, unserialize($auModel->data));
				
		}
		
// 		$authItems = $this->_authorizer->getAuthItems(array(
// 				CAuthItem::TYPE_TASK,
// 				CAuthItem::TYPE_OPERATION,
// 		));
		$existingItems = array();
		foreach( $authItems as $itemName=>$item )
			$existingItems[ $itemName ] = $itemName;
	
		Yii::app()->clientScript->registerScript('rightsGenerateItemTableSelectRows',
				"jQuery('.generate-item-table').rightsSelectRows();"
		);
	
		// Render the view
		$this->render('generate', array(
				'model'=>$model,
				'items'=>$items,
				'existingItems'=>$existingItems,
		));
	}
	
	public function actionPermissions()
	{
		$authManager = app()->authManager;
		
		$model = new AuthItemsChildForm;
		
		$roles = AuthItem::model()->findAll('type = 2');
		
		$arRoles = CHtml::listData($roles, 'name', 'name');
		
		$items = CHtml::listData(AuthItem::model()->findAll('type IN(0,1)'), 'name', 'name');
		
		$exItems = CHtml::listData(AuthItemChild::model()->findAll(), 'parent', 'child');
		
		
		
		if (isset($_POST['AuthItemsChildForm'])) {
			$itemsRoles = $_POST['AuthItemsChildForm'];
			
			foreach ($itemsRoles as $role => $children){
				foreach ($children as $child => $value) {
// 					echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($child); echo'</pre>';
					if ($value == 1 && !$authManager->hasItemChild($role, $child)) {
						$authManager->addItemChild($role, $child);
					}
					if ($value == 0 && $authManager->hasItemChild($role, $child)) {
						$authManager->removeItemChild($role, $child);
					}
				}
				//app()->authManager->addItemChild($role, $childName)
// 				echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($role); echo'</pre>';
			}
		}
		
// 		echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump(count($roles)); echo'</pre>';die;
		$this->render('permissions', array(
				'roles'=>$roles,
				'items'=>$items,
				'model'=>$model,
// 				'existingItems'=>$existingItems,
		));
	}
}
