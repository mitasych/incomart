<?php

class TicketMessageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
// 	public function filters()
// 	{
// 		return array(
// 			'accessControl', // perform access control for CRUD operations
// 		);
// 	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
// 	public function accessRules()
// 	{
// 		return array(
// 			array('allow',  // allow all users to perform 'index' and 'view' actions
// 				'actions'=>array('index','view'),
// 				'users'=>array('*'),
// 			),
// 			array('allow', // allow authenticated user to perform 'create' and 'update' actions
// 				'actions'=>array('create','update'),
// 				'users'=>array('@'),
// 			),
// 			array('allow', // allow admin user to perform 'admin' and 'delete' actions
// 				'actions'=>array('admin','delete'),
// 				'users'=>array('admin'),
// 			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
// 		);
// 	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		if (isset($_GET['idTicketMessage'])) {
			$model=TicketMessage::model()->findByPk($_GET['idTicketMessage']);
				
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
			$this->render('view',array(
				'model'=>$model,
			));
		}
		else{
			throw new CHttpException(404,'The requested page does not exist.');
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TicketMessage;

		if (!isset($_REQUEST['ticket_id'])) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		
		$model->ticket_id = $_REQUEST['ticket_id'];
		$ticket = Ticket::model()->findByPk($_REQUEST['ticket_id']);
		
		$quest_text = NULL;
		if (isset($_REQUEST['ask_id'])) {
			$model->ask_id = $_REQUEST['ask_id'];
			$quest = TicketMessage::model()->findByPk($_REQUEST['ask_id']);
			$quest_text = $quest->message;
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TicketMessage']))
		{
			$model->attributes=$_POST['TicketMessage'];
			$model->user_id = user()->id;
			$model->type = 'support';
			if($model->save())
				$this->redirect(array('/ticket/view','id'=>$model->ticket_id));
		}

		$this->render('create',array(
			'model'=>$model,
			'ticket_text'=>$ticket->description,
			'quest_text'=>$quest_text
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TicketMessage']))
		{
			$model->attributes=$_POST['TicketMessage'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new TicketMessage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TicketMessage']))
			$model->attributes=$_GET['TicketMessage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=TicketMessage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ticket-message-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
