<?php
$this->breadcrumbs=array(
	Yii::t('main','Transactions Errors')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Transactions Error'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Transactions Error'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Transactions Errors'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update Transactions Error: "'); echo $model->name.'"'; ?></h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>