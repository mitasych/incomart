<?php
$this->breadcrumbs=array(
	Yii::t('main','Transactions Errors')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Transactions Error'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Transactions Error'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Transactions Error'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Transactions Errors'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Transactions Error: "'); echo $model->name.'"'; ?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'machine_name',
		'error_description',
	),
)); ?>
