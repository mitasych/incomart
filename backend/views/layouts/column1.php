<?php $this->beginContent('//layouts/main'); ?>
<div class="container">

	
<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		/* 'title'=>Yii::t('main','Operations'), */ 
		'decorationCssClass'=>'span',
	));
		/* $this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'clear'),
			'itemCssClass'=>'span',
		)); */
	$this->widget('bootstrap.widgets.TbButtonGroup', array(
			'buttons'=>$this->menu,
			/* 'htmlOptions' => array('class'=>'btn-group-vertical'), */
	));
	$this->endWidget();
?>
	
	<div class="clear"></div>
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>