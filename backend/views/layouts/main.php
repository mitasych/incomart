<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="en"/>

	<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
	      media="screen, projection"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
	      media="print"/>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php // echo Yii::app()->request->baseUrl; ?>/css/ie.css"
	      media="screen, projection"/>
	<![endif]-->
	<!-- <link rel="stylesheet" type="text/css" href=" --><?php //echo Yii::app()->request->baseUrl; ?><!-- /css/main.css"> -->
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/main.css'); ?>
</head>

<body>

<?php // $user = Yii::app()->getUser();?>
<?php // echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump(Yii::app()->getUser()->checkAccess('editor')); echo'</pre>';die;?>

<div class="container" id="page">
	<?php $this->widget('bootstrap.widgets.TbNavbar', array(
	'type' => 'inverse', // null or 'inverse'
	'brand' => Yii::app()->name,
	'brandUrl' => '/site/index',
	'collapse' => true, // requires bootstrap-responsive.css
	'items' => array(
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'items' => array(
				array('label' => Yii::t('main','Home'), 'icon'=>'home', 'url' => array('/site/index')),
	//			array('label' => 'About', 'url' => array('/site/page', 'view' => 'about')),
				array('label' => Yii::t('main','Manage'), 'icon'=>'briefcase', 'url' => array('/post'), 'items' => array (
						array('label' => Yii::t('main','Banners'), 'icon'=>'icon-retweet', 'url' => array('/banner/index/'),
								'items' => array(
										array('label' => Yii::t('main','Banners'), 'icon'=>'icon-retweet', 'url' => array('/banner/index/')),
										array('label' => Yii::t('main','Banners Sizes'), 'icon'=>'icon-resize-full', 'url' => array('/bannerSizes/index/')),
								)),

						array('label' => Yii::t('main','Users'), 'icon'=>'user', 'url' => array('/user')),
						array('label' => Yii::t('main','Rights'), 'icon'=>'list', 'url' => array('/authItem'), 'visible' => Yii::app()->user->superuser,
								'items' => array (
										array('label' => Yii::t('main','Permissions'), 'icon'=>'list', 'url' => array('/authItem/permissions')),
										array('label' => Yii::t('main','Generate'), 'icon'=>'align-left', 'url' => array('/authItem/generate')),
								)),
						'---',
						array('label' => Yii::t('main','Tarifs'), 'icon'=>'briefcase', 'url' => array('/tarif')),
						'---',
						array('label' => Yii::t('main','Accounts'), 'icon'=>'list', 'url' => array('/userAccount'),
								'items' => array(
											array('label' => Yii::t('main','Accounts'), 'icon'=>'tag', 'url' => array('/userAccount')),
											array('label' => Yii::t('main','Transactions'), 'icon'=>'tag', 'url' => array('/userAccountTransaction')),
											array('label' => Yii::t('main','Transactions Types'), 'icon'=>'tag', 'url' => array('/transactionType')),
											array('label' => Yii::t('main','Transactions Errors'), 'icon'=>'tag', 'url' => array('/transactionsError')),
											array('label' => Yii::t('main','Currencies'), 'icon'=>'tag', 'url' => array('/currency'))
										),
								
								),
						'---',
						array('label' => Yii::t('main','Cameras'), 'icon'=>'facetime-video', 'url' => array('/authItem'),
								'items' => array (
										array('label' => Yii::t('main','Manufacturers'), 'icon'=>'list', 'url' => array('/camerasManufacturer')),
										array('label' => Yii::t('main','Models'), 'icon'=>'align-left', 'url' => array('/camerasModel')),
										array('label' => Yii::t('main','Frame Rate'), 'icon'=>'film', 'url' => array('/cameraFrameRate')),
										array('label' => Yii::t('main','Resolution'), 'icon'=>'picture', 'url' => array('/cameraResolution')),
										array('label' => Yii::t('main','Bitrate'), 'icon'=>'film', 'url' => array('/videoBitrate')),
										array('label' => Yii::t('main','Cameras'), 'icon'=>'facetime-video', 'url' => array('/camera')),
								)),
						'---',
						array('label' => Yii::t('main','Tickets'), 'icon'=>'list', 'url' => array('/ticket')),
						array('label' => Yii::t('main','Reviews'), 'icon'=>'comment', 'url' => array('/review')),
						'---',
						array('label' => Yii::t('main','Settings'), 'icon'=>'cog', 'url' => array('/settings')),
				)),
				array('label' => Yii::t('main','Create'), 'icon'=>'file', 'items' => array (
						array('label' => Yii::t('main','User'), 'icon'=>'user', 'url' => array('/user/create')),
						array('label' => Yii::t('main','Banner'), 'icon'=>'icon-retweet', 'url' => array('/banner/create/')),
				)),
 				array('label' => Yii::t('main','Login'), 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
				array('label' => Yii::t('main','Logout') ./*  Yii::app()->user->name . */ '', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
			),
		),
	),
)); ?>
	<!-- mainmenu -->
	<div class="container">
	<?php // echo '<b>'.__FILE__.' -- '.__LINE__.'</b><pre>'; var_dump($this->breadcrumbs); echo'</pre>';die;?>
		<?php if (isset($this->breadcrumbs)): ?>
			<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links' => $this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
		<?php endif ?>

		<?php echo $content; ?>
		<hr/>
		<div id="footer">
			Copyright &copy; <?php echo date('Y'); ?> by NikoSmart.<br/><img>
			All Rights Reserved.<br/>
		</div>
		<!-- footer -->
	</div>
</div>
<!-- page -->
</body>
</html>