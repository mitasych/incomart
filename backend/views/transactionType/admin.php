<?php
$this->breadcrumbs=array(
	Yii::t('main','Transactions Types')=>array('index'),
	Yii::t('main','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Transactions Type'),'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('transaction-type-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Manage Transactions Types');?></h1>
</div>

<?php echo CHtml::link(Yii::t('main','Advanced Search'),'#',array('class'=>'search-button','title'=>Yii::t("main", "You may optionally enter a comparison operator (<, <=, >, >=, <> or =) at the beginning of each of your search values to specify how the comparison should be done."))); ?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'transaction-type-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'machine_name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
<script type="text/javascript"> 
	$('.search-button').tooltip('hide','left');
</script>