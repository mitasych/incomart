<?php
$this->breadcrumbs=array(
	Yii::t('main','Transactions Types')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Transactions Type'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update  Transactions Type'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete  Transactions Type'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Transactions Types'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Transactions Type: "'); echo $model->name.'"'; ?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'machine_name',
	),
)); ?>
