<?php
$this->breadcrumbs=array(
	Yii::t('main','Transactions Types')=>array('index'),
	Yii::t('main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Manage Transactions Types'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Create Transactions Type');?></h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>