<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Camera','url'=>array('index')),
	array('label'=>'Manage Camera','url'=>array('admin')),
);
?>

<h1>Create Camera</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>