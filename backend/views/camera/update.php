<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Camera','url'=>array('index')),
	array('label'=>'Create Camera','url'=>array('create')),
	array('label'=>'View Camera','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Camera','url'=>array('admin')),
);
?>

<h1>Update Camera <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>