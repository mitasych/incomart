<?php
$this->breadcrumbs=array(
	'Cameras'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Camera','url'=>array('index')),
	array('label'=>'Create Camera','url'=>array('create')),
	array('label'=>'Update Camera','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Camera','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Camera','url'=>array('admin')),
);
?>

<h1>View Camera #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'machine_name',
		'manufacturer_id',
		'model_id',
		'frame_rate_id',
		'resolution_id',
		'bitrate_id',
		'disk_space_per_day',
		'archive',
		'archive_days_number',
		'disk_space',
		'IP_address',
		'login',
		'password',
		'port',
		'url',
		'user_id',
		'create_time',
		'cost_per_day',
	),
)); ?>
