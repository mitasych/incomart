<div class="view_small_std span2" id="img_<?php echo $data->id?>">
	<a href="#" class="thumbnail" rel="tooltip" data-title="Tooltip" style="height:150px; text-align: center;">
		<div class="image-thumb">
			<?php echo CHtml::image('/image/uploaded/thumb/'.$data->img, $data->img, array('descr'=>$data->description)); ?>
		</div>
		<br />
		<?php $cutTitle = mb_strlen($data->title, 'UTF-8') > 12 ? mb_substr($data->title, 0, 12, 'UTF-8').'...' : $data->title; ?>
		<span class="img-title" image-id="<?php echo $data->id?>" image-title-cut="<?php echo $cutTitle?>" image-title="<?php echo $data->title?>" image-descr="<?php echo $data->description?>" style="background-color: #fff; position: relative; padding: 2px 3px" onmouseover="this.innerHTML=this.getAttribute('image-title'); this.style.zIndex=100;" onmouseout="this.innerHTML=this.getAttribute('image-title-cut'); this.style.zIndex=0;">
			<?php echo $cutTitle; ?>
		</span>
	
	
		<?php /*
		<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
		<?php echo CHtml::encode($data->create_user_id); ?>
		<br />
	
		*/ ?>
	</a>
</div>