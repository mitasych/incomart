<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'image-update-post-form',
	'action'=>$action,
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
				//'enctype' => 'multipart/form-data',
				
			),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>150)); ?>

	<?php echo $form->textFieldRow($model,'description',array('class'=>'span5','maxlength'=>500)); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'primary',
			'id'=>'updButton',
			'label'=>Yii::t('main','Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
