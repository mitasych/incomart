<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $this->pageTitle; ?></title>
<script type="text/javascript" src="/admin/assets/jquery.js"></script>
<script type="text/javascript" src="/admin/assets/jquery.form.js"></script>
<?php // Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/assets/jquery.form.js'); ?>
<link rel="stylesheet" type="text/css" href="/admin/assets/8795efd6/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/admin/assets/8795efd6/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css" href="/admin/assets/8795efd6/css/bootstrap-yii.css">
<link rel="stylesheet" type="text/css" href="/admin/assets/8795efd6/css/jquery-ui-bootstrap.css">
<script type="text/javascript" src="/admin/assets/8795efd6/js/bootstrap.bootbox.min.js"></script>
<script type="text/javascript" src="/admin/assets/8795efd6/js/bootstrap.js"></script>
</head>
<body>

<?php
$this->breadcrumbs=array(
	Yii::t('main','Image Posts')=>array('index'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create ImagePost'),'url'=>array('create')),
	array('label'=>Yii::t('main','Manage ImagePost'),'url'=>array('admin')),
);
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'addImage')); ?>
 
	<div class="modal-header">
	    <a class="close" data-dismiss="modal">&times;</a>
	</div>
	<div class="modal-body">
	    <?php echo $this->renderPartial('_form', array('model'=>$model, 'action' => Yii::app()->createUrl($this->id.'/create'))); ?>
	</div>
 
<?php $this->endWidget(); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'updateImage')); ?>
 
	<div class="modal-header">
	    <a class="close" data-dismiss="modal">&times;</a>
	</div>
	 
	<div class="modal-body">
	    <?php echo $this->renderPartial('_update_form', array('model'=>$model, 'action' => Yii::app()->createUrl($this->id.'/update'))); ?>
	</div>
 
<?php $this->endWidget(); ?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Upload Image',
    'type'=>'primary',
    'htmlOptions'=>array(
        'data-toggle'=>'modal',
        'data-target'=>'#addImage',
    	'height'=>'500px'
    ),
)); ?>


<h1><?php echo Yii::t('main','Image Posts');?></h1>

<div class="span-3">
	<?php $this->widget('bootstrap.widgets.TbMenu', array(
    'type'=>'list',
    'items'=>$usersItems,
)); ?>
</div>

<div id="mainListImages" style="float:left">
	<div class="span-19">
	<?php $this->widget('bootstrap.widgets.TbThumbnails', array(
	    'dataProvider'=>$dataProvider,
	    'template'=>"{items}\n{pager}",
	    'itemView'=>'_view',
	//     'itemView'=>'_thumb',
	)); ?>
		<?php
	// 		 $this->widget('bootstrap.widgets.TbListView',array(
	// 			'dataProvider'=>$dataProvider,
	// 			'itemView'=>'_view',
	// 		)); 
		 ?>
	
	</div>
</div>
	
<script type="text/javascript">
	$('.image-thumb').live('click', function(){
		var img = $(this).find('img').attr('alt');
		var descr = $(this).find('img').attr('descr');
		console.log(img);

		var url = '/image/uploaded/large/'+img;
		var funcNum = getUrlParam('CKEditorFuncNum');
		//
		console.log(<?php echo $funcNum;?>);
		//console.log(window.opener.CKEDITOR.tools);
		//window.opener.CKEDITOR.tools.callFunction( <?php echo $funcNum;?>, url, function() {
		window.opener.CKEDITOR.tools.callFunction( funcNum, url, function() {
		  // Get the reference to a dialog window.
		  var element, dialog = this.getDialog();
		  // Check if this is the Image dialog window.
		  if (dialog.getName() == 'image') {
		    // Get the reference to a text field that holds the "alt" attribute.
		    element = dialog.getContentElement( 'info', 'txtAlt' );
		    // Assign the new value.
		    if (element)
		      element.setValue(descr);
		  }
		  // Return false to stop further execution - in such case CKEditor will ignore the second argument (fileUrl)
		  // and the onSelect function assigned to a button that called the file browser (if defined).
		  //return false;
		});
		window.close();
		//window.opener.CKEDITOR.tools.callFunction(<?php echo $funcNum;?>, url, 'yes');
		//window.parent.CKEDITOR.tools.callFunction(<?php echo $funcNum;?>, url, 'yes');
		
	});

	function getUrlParam(paramName){
	  var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
	  var match = window.location.search.match(reParam) ;
	 
	  return (match && match.length > 1) ? match[1] : '' ;
	}

	function reContent(user, date){
		$.ajax({
			'url':'/admin/imagePost/ajaxImages?date='+date+'&user='+user,
			'cache':false,
			'success':function(html){
				console.log(html)
				$("#mainListImages").html(html)
			}
		});
	}
</script>

<script type="text/javascript">

var updFormId = '#image-update-post-form';
var updForm = $('#image-update-post-form');

	//function ajaxUpdateImageInfo(id, title, descr){
	$('.img-title').live('click', function(){
		var id=$(this).attr('image-id');
		var title=$(this).attr('image-title');
		var descr=$(this).attr('image-descr');
		
		$('#updateImage').modal();
		
		console.log(updForm.lenght);
		updForm.attr('action', '/admin/imagePost/update/'+id);
		updForm.find('#ImagePost_title').val(title);
		updForm.find('#ImagePost_description').val(descr);

	});

	$('#updButton').live('click',function(){
		
		var options = {
				dataType: 'json',
				success:       function(responseText, statusText, xhr, $form){
									//progress.close();
									console.log(responseText);
									console.log(statusText);
									
									
									if(responseText.errors){
										console.log(responseText.errors);
										/*var array = responseText.errors.img;
										progress.removeClass('progress-info').addClass('progress-danger');
										$('#image_error_block').empty();
										$('#image_error_block').append('<div class="alert alert-block alert-error">'
												+'<p>Необходимо исправить следующие ошибки:</p><ul></ul></div>');
										for (var key in array) {
											var val = array[key];
											$('#image_error_block ul').append('<li>'+val+'</li>');
										}
										$('#image_error_block').show();	*/								
									}
									else{
										var targetSpan = $("span.img-title[image-id='"+responseText.id+"']");
										targetSpan.attr('image-title-cut', responseText.cut_title);
										targetSpan.attr('image-title', responseText.title);
										targetSpan.attr('image-descr', responseText.description);
										targetSpan.html(responseText.cut_title);
										$('#img_'+responseText.id+' img').attr('descr', responseText.description);

										$("div.modal-header a.close").trigger('click');
										$(updFormId+" :input").each(function(){
											$(this).val('');
										});
										/*$("div.modal-header a.close").trigger('click');
										$(form_id+" :input").each(function(){
											$(this).val('');
										});
										$('#Post_main_image').val(responseText.id);
										bar.width(0);
										progress.hide();*/
									}
								},  // post-submit callback 
			}; 
			
//			console.log($(form_id));
			$(updFormId).ajaxSubmit(options);
	});

</script>
</body>
</html>