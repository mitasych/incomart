<?php
$this->breadcrumbs=array(
	Yii::t('main','Cameras Models')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Cameras Model'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Cameras Model'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Cameras Model'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Cameras Models'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Cameras Model: "'); echo $model->name.'"'; ?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
// 		'manufacturer_id',
		array(
				'name'=>'manufacturer_id',
// 				'label'=>'Manufacturer',
				'value'=>$model->manufacturer->name
		),
		array(
				'name'=>'frame_rate_ids',
// 				'label'=>'FrameRates',
				'value'=>$model->namesFrameRates
		),
		array(
				'name'=>'resolutions_ids',
// 				'label'=>'NamesResolutions',
				'value'=>$model->namesResolutions
		),
		'url',
	),
)); ?>
