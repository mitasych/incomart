<?php
$this->breadcrumbs=array(
	Yii::t('main','Cameras Models')=>array('index'),
	Yii::t('main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Manage Cameras Models'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Create Cameras Model');?></h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>