<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cameras-model-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="main span8">
		<div class="well">
			<div class="fleft span7">
				<?php echo $form->textFieldRow($model,'name',array('class'=>'unit-fluid','maxlength'=>256)); ?>
				
				<?php echo $form->textFieldRow($model,'url',array('class'=>'unit-fluid','maxlength'=>256)); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div><!-- end main -->
	<div class="side span4">
		<div class="form-actions" style="margin-top: 0;">
			<?php echo $form->dropDownListRow($model, 'frameRates', CameraFrameRate::listItems(), array('multiple'=>true,'class'=>'unit-fluid')); ?>
			
			<?php echo $form->dropDownListRow($model, 'resolutions', CameraResolution::listItems(), array('multiple'=>true,'class'=>'unit-fluid')); ?>
			
			<?php // echo $form->textFieldRow($model,'manufacturer_id',array('class'=>'span5')); ?>
			<hr />
			<?php echo $form->dropDownListRow($model, 'manufacturer_id', CamerasManufacturer::listItems(), array('empty'=>'Select','class'=>'unit-fluid')); ?>
			
			<?php // echo $form->labelEx($model,'media_type', array('class'=>"displ_inline")); ?>
			<?php // echo $form->ListBox($model,'arMedia', MediaType::listItems(), 
					//	array('multiple'=>'multiple', 'onChange'=>'mediaEmbed()','class'=>'unit-fluid')); ?>
		</div>
	</div>
	<div class="clear"></div>
	<div class="span12">
		<div class="form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'),
			)); ?>
		</div>
		<p class="note"><?php echo Yii::t('main','Fields with ') ?>
		<span class="required">*</span><?php echo Yii::t('main',' are required.') ?></p>
	</div>
</div>
<?php $this->endWidget(); ?>
