<?php
$this->breadcrumbs=array(
	Yii::t('main','Cameras Models')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Cameras Model'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Cameras Model'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Cameras Models'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update Cameras Model: "'); echo $model->name.'"'; ?></h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>