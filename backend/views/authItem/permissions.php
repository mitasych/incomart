<?php $this->breadcrumbs = array(
	//'Rights'=>Yii::getBaseUrl(),
// 	Yii::t('core', 'Generate items'),
); ?>

<div id="generator">

	<h2><?php echo Yii::t('core', 'Generate items'); ?></h2>

	<p><?php echo Yii::t('core', 'Please select which items you wish to generate.'); ?></p>

	<div class="form">

		<?php $form=$this->beginWidget('CActiveForm'); ?>

			<div class="row">

				<table class="items generate-item-table" border="0" cellpadding="0" cellspacing="0">

					<tbody>

						<tr class="application-heading-row">
							<th colspan="<?php echo count($roles)+1; ?>"><?php echo Yii::t('core', 'Application Front'); ?></th>
						</tr>

						<?php $this->renderPartial('_childItems', array(
							'model'=>$model,
							'form'=>$form,
							'roles'=>$roles,
							'items'=>$items,
// 							'existingItems'=>$existingItems,
// 							'displayModuleHeadingRow'=>true,
// 							'basePathLength'=>strlen(Yii::app()->basePath),
						)); ?>
						

					</tbody>

				</table>

			</div>

   			<div class="row">

				<?php echo CHtml::submitButton(Yii::t('core', 'Create/update permissions')); ?>

			</div>

		<?php $this->endWidget(); ?>

	</div>

</div>

<script type="text/javascript">
function fixTitles() {
	$('.roles-names-row-fixed td').each(function(){
		var sameClass = $(this).attr('class');
		var trueWidth = $('.roles-names-row td.'+sameClass).width();
		$(this).width(trueWidth);
	});
	var $titles = $('.roles-names-row-fixed'); 
	if ($(window).scrollTop() > 100) 
		$titles.css({'position': 'fixed', 'top': '41px', 'display':'block'}); 
	else
		$titles.css({'position': 'relative', 'top': 'auto', 'display': 'none'});
	}
	$(window).scroll(fixTitles);
	fixTitles();
</script>