<?php if(count($roles)>0):?>
	<!-- <tr class="roles-names-row" style="position: fixed;left: 733px;top: 100px;"> -->
	<tr class="roles-names-row" style="">
			<td class="empty_td"></td>
		<?php foreach ($roles as $role):?>
			<td class="role-name-<?php echo $role->name;?>">
				<?php echo $role->name;?>
			</td>
		<?php endforeach;?>
	</tr>
	<tr class="roles-names-row-fixed" style="display:none; background-color: #fff;">
			<td class="empty_td"></td>
		<?php foreach ($roles as $role):?>
			<td class="role-name-<?php echo $role->name;?>">
				<?php echo $role->name;?>
			</td>
		<?php endforeach;?>
	</tr>
<?php endif; ?>

<?php if(count($items)>0):?>
	<?php foreach ($items as $item):?>
		<tr class="item-child-row">
			<td class="item-child-name">
				<?php echo $item;?>
			</td>
			<?php foreach ($roles as $role):?>
				<td class="check-item-child">
					<?php 
						$checked = false;
						if (!empty($role->children)) {
							if (in_array($item, $role->children)) {
								$checked = 'checked';
							}
						}
					?>
					<?php echo $form->checkBox($model, $role->name.'['.$item.']', array('checked' => $checked)); ?>
				</td>
			<?php endforeach;?>
		</tr>
	<?php endforeach;?>
<?php endif;?>

<?php if(0):?>
<?php if( $items['controllers']!==array() ): ?>

	<?php foreach( $items['controllers'] as $key=>$item ): ?>

		<?php if( isset($item['actions'])===true && $item['actions']!==array() ): ?>

			<?php $controllerKey = isset($moduleName)===true ? $app.'.'.ucfirst($moduleName).'.'.$item['name'] : $app.'.'.$item['name']; ?>
			<?php $controllerExists = isset($existingItems[ $controllerKey.'.*' ]); ?>

			<tr class="controller-row <?php echo $controllerExists===true ? 'exists' : ''; ?>">
				<td class="checkbox-column"><?php echo $controllerExists===false ? $form->checkBox($model, 'items['.$controllerKey.'.*]') : ''; ?></td>
				<td class="name-column">
					<?php // echo ucfirst($item['name']).'.*'; ?>
					<?php echo $controllerKey.'.*'; ?>
				</td>
				<td class="path-column"><?php echo substr($item['path'], $basePathLength+1); ?></td>
			</tr>

			<?php $i=0; foreach( $item['actions'] as $action ): ?>

				<?php $actionKey = $controllerKey.'.'.ucfirst($action['name']); ?>
				<?php $actionExists = isset($existingItems[ $actionKey ]); ?>

				<tr class="action-row<?php echo $actionExists===true ? ' exists' : ''; ?><?php echo ($i++ % 2)===0 ? ' odd' : ' even'; ?>">
					<td class="checkbox-column"><?php echo $actionExists===false ? $form->checkBox($model, 'items['.$actionKey.']') : ''; ?></td>
					<td class="name-column">
						<?php // echo $action['name']; ?>
						<?php echo $actionKey; ?>
					</td>
					<td class="path-column"><?php echo substr($item['path'], $basePathLength+1).(isset($action['line'])===true?':'.$action['line']:''); ?></td>
				</tr>

			<?php endforeach; ?>

		<?php endif; ?>

	<?php endforeach; ?>

<?php else: ?>

	<tr><th  class="no-items-row" colspan="3"><?php echo Rights::t('core', 'No actions found.'); ?></th></tr>

<?php endif; ?>
<?php endif;?>