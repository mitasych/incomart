<?php
$this->breadcrumbs=array(
	Yii::t('main','Camera Frame Rates')=>array('index'),
	Yii::t('main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Manage Frame Rates'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Create Frame Rate');?></h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>