<?php
$this->breadcrumbs=array(
	Yii::t('main','Camera Frame Rates')=>array('index'),
	$model->frame_rate=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Frame Rate'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Frame Rate'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Frame Rates'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update Frame Rate: "'); echo $model->frame_rate.'"'; ?></h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>