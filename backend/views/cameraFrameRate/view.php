<?php
$this->breadcrumbs=array(
	Yii::t('main','Camera Frame Rates')=>array('index'),
	$model->frame_rate,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Frame Rate'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Frame Rate'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Frame Rate'),'url'=>'#','htmlOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Frame Rates'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Frame Rate: "'); echo $model->frame_rate.'"'; ?></h1>
</div>

<?php $attribs = array_merge(array('id', 'frame_rate'), $model->relTarifs); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>$attribs
)); ?>
