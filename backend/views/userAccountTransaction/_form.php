<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-account-transaction-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="main span8">
		<div class="well">
			<div class="fleft span7">
				<?php echo $form->textFieldRow($model,'account_id',array('class'=>'unit-fluid')); ?>
			
				<?php echo $form->textFieldRow($model,'type_transaction_id',array('class'=>'unit-fluid')); ?>
			
				<?php echo $form->textFieldRow($model,'guid_transaction',array('class'=>'unit-fluid','maxlength'=>45)); ?>
			
				<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'unit-fluid')); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div><!-- end main -->
	<div class="side span4">
		<div class="form-actions" style="margin-top: 0;">
			<?php // echo $form->textFieldRow($model,'status',array('class'=>'unit-fluid')); ?>
			
			<?php echo $form->dropDownlistRow($model,'status',UserAccountTransaction::listStatus()); ?>
		
			<?php echo $form->textFieldRow($model,'status_error',array('class'=>'unit-fluid')); ?>
		
			<?php echo $form->textFieldRow($model,'create_time',array('class'=>'unit-fluid')); ?>
		
			<?php echo $form->textFieldRow($model,'amount',array('class'=>'unit-fluid','maxlength'=>45)); ?>
		</div>
	</div>
	<div class="clear"></div>
	<div class="span12">
		<div class="form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? Yii::t('main','Create') : Yii::t('main','Save'),
			)); ?>
		</div>
		<p class="note"><?php echo Yii::t('main','Fields with ') ?>
		<span class="required">*</span><?php echo Yii::t('main',' are required.') ?></p>
	</div>
</div>
<?php $this->endWidget(); ?>
