<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'htmlOptions'=>array('class'=>'well custom_search'),
	'method'=>'get',
)); ?>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'account_id',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'type_transaction_id',array('class'=>'unit-fluid')); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'guid_transaction',array('class'=>'unit-fluid','maxlength'=>45)); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'status',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'status_error',array('class'=>'unit-fluid')); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
	<?php echo $form->textFieldRow($model,'create_time',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
	<?php echo $form->textFieldRow($model,'amount',array('class'=>'unit-fluid','maxlength'=>45)); ?>
		</div>
	<div class="fleft span4">
	<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'unit-fluid')); ?>
	</div>
</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>Yii::t('main','Search'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
