<?php
$this->breadcrumbs=array(
	Yii::t('main','User Account Transactions')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create User Account Transaction'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update User Account Transaction'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete User Account Transaction'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage User Account Transactions'),'url'=>array('index')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','View User Account Transaction: "'); echo $model->id.'"'; ?>
	</h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'account_id',
		'type_transaction_id',
		'guid_transaction',
		'description',
		'status',
		'status_error',
		'create_time',
		'amount',
	),
)); ?>
