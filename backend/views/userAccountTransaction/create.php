<?php
$this->breadcrumbs=array(
	Yii::t('main','User Account Transactions')=>array('index'),
	Yii::t('main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Manage User Account Transactions'),'url'=>array('index')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Create User Account Transaction');?>
	</h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>