<?php
$this->breadcrumbs=array(
	Yii::t('main','User Account Transactions')=>array('index'),
	Yii::t('main','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create User Account Transaction'),'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-account-transaction-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Manage User Account Transactions');?>
	</h1>
</div>

<?php echo CHtml::link(Yii::t('main','Advanced Search'),'#',array('class'=>'search-button','title'=>Yii::t("main", "You may optionally enter a comparison operator (<, <=, >, >=, <> or =) at the beginning of each of your search values to specify how the comparison should be done."))); ?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'user-account-transaction-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'account_id',
		'type_transaction_id',
		'guid_transaction',
		'description',
		'status',
		/*
		'status_error',
		'create_time',
		'amount',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
<script type="text/javascript"> 
	$('.search-button').tooltip('hide','left');
</script>