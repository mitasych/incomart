<?php
$this->breadcrumbs=array(
	Yii::t('main','User Account Transactions')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create User Account Transaction'),'url'=>array('create')),
	array('label'=>Yii::t('main','View User Account Transaction'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage User Account Transactions'),'url'=>array('index')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Update User Account Transactions: "'); echo $model->id.'"'; ?>
	</h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>