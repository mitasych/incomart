<?php
$this->breadcrumbs=array(
	Yii::t('main','Currencies')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Currency'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Currency'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Currency'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Currencies'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Currency: "'); echo $model->name.'"'; ?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'machine_name',
		'shortening',
		'code',
		'symbol',
		'symbol_place',
	),
)); ?>
