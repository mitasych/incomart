<?php
$this->breadcrumbs=array(
	Yii::t('main','Currencies')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Currency'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Currency'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Currencies'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update Currency: "'); echo $model->name.'"'; ?></h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>