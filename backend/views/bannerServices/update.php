<?php
$this->breadcrumbs=array(
	Yii::t('main','Banner Services')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Banner Services'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Banner Services'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Banner Services'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update Banner Services: "'); echo $model->name.'"';?></h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>