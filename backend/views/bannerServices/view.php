<?php
$this->breadcrumbs=array(
	Yii::t('main','Banner Services')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Banner Services'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Banner Services'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Banner Services'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Banner Services'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Banner Services: "'); echo $model->name.'"';?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'banner_size_id',
		'place',
		'placement_period_item',
		'placement_period_number',
		'cost',
	),
)); ?>
