<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'banner-services-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="main span8">
		<div class="well">
			<div class="fleft span7">
				<?php echo $form->textFieldRow($model,'name',array('class'=>'unit-fluid','maxlength'=>45)); ?>
			
				<?php echo $form->textFieldRow($model,'place',array('class'=>'unit-fluid','maxlength'=>500)); ?>
			
				<?php echo $form->textFieldRow($model,'cost',array('class'=>'unit-fluid','maxlength'=>45)); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div><!-- end main -->
	<div class="side span4">
		<div class="form-actions" style="margin-top: 0;">
			<?php echo $form->dropDownListRow($model, 'banner_size_id', BannerSizes::listItems(),array('class'=>'unit-fluid')); ?>
			<?php // echo $form->textFieldRow($model,'placement_period_item',array('class'=>'span5','maxlength'=>100)); ?>
			<?php echo $form->dropDownListRow($model, 'placement_period_item', $model->placementItems,array('class'=>'unit-fluid')); ?>
			<?php echo $form->textFieldRow($model,'placement_period_number',array('class'=>'unit-fluid')); ?>
		</div>
	</div>
	<div class="clear"></div>
	<div class="span12">
		<div class="form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'),
			)); ?>
		</div>
		<p class="note"><?php echo Yii::t('main','Fields with ') ?>
		<span class="required">*</span><?php echo Yii::t('main',' are required.') ?></p>
	</div>
</div>
<?php $this->endWidget(); ?>
