<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'htmlOptions'=>array('class'=>'well custom_search'),
	'method'=>'get',
)); ?>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'name',array('class'=>'unit-fluid','maxlength'=>45)); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'banner_size_id',array('class'=>'unit-fluid')); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'place',array('class'=>'unit-fluid','maxlength'=>500)); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'placement_period_item',array('class'=>'unit-fluid','maxlength'=>100)); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'placement_period_number',array('class'=>'unit-fluid')); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'cost',array('class'=>'unit-fluid','maxlength'=>45)); ?>
	</div>
</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>Yii::t('main','Search'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
