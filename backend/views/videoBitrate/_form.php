<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'video-bitrate-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'value',array('class'=>'span5','maxlength'=>45)); ?>

	<?php foreach($model->tarifPrice as $key => $tarVal): ?>
				<?php echo CHtml::label($tarVal['name'], 'tar_'.$tarVal['id'])?>
				<?php echo $form->textField($model, 'tarifPrice['.$tarVal['id'].']', array('id'=>'tar_'.$tarVal['id'], 'value'=>$tarVal['val']));?>
			<?php endforeach;?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
