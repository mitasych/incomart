<?php
$this->breadcrumbs=array(
	'Video Bitrates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List VideoBitrate','url'=>array('index')),
	array('label'=>'Manage VideoBitrate','url'=>array('admin')),
);
?>

<h1>Create VideoBitrate</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>