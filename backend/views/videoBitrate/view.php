<?php
$this->breadcrumbs=array(
	'Video Bitrates'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List VideoBitrate','url'=>array('index')),
	array('label'=>'Create VideoBitrate','url'=>array('create')),
	array('label'=>'Update VideoBitrate','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete VideoBitrate','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage VideoBitrate','url'=>array('admin')),
);
?>

<h1>View VideoBitrate #<?php echo $model->id; ?></h1>

<?php $attribs = array_merge(array('id', 'value'), $model->relTarifs); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>$attribs,
// 	'attributes'=>array(
// 		'id',
// 		'value',
		
// // 		array(
// // 			'name'=>'Tariffs',
// // 			'type'=>'raw',
// // 			'value'=>foreach($model->relTarifs as $tarif) {
// // 				echo "<b>".$tarif['name']."</b> - ".$tarif['cost']."\n";
// // 			}
// // 		),
// 	),
)); ?>