<?php
$this->breadcrumbs=array(
	Yii::t('main','Tarifs')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Tarif'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Tarif'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Tarifs'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update Tarif: "'); echo $model->name.'"'; ?></h1>
</div>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>