<?php
$this->breadcrumbs=array(
	Yii::t('main','Tarifs')=>array('index'),
	Yii::t('main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Manage Tarifs'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Create Tarif');?></h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>