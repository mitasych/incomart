<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'machine_name',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'archive',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'archive_term',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'archive_space',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'archive_download',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'number_cameras',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'safe_mode',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'local_archive',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'embed_mode',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'clode_archive',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'AVI_export',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'another_users_access',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'online_broadcasting',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'extended_support',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'payment_settlement_account',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'free_period',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
