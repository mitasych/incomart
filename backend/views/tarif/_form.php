<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'tarif-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="main span8">
		<div class="well">
			<div class="fleft span7">
				<?php echo $form->textFieldRow($model,'name',array('class'=>'unit-fluid','maxlength'=>200)); ?>
			
				<?php echo $form->textFieldRow($model,'machine_name',array('class'=>'unit-fluid','maxlength'=>200)); ?>
			
				<?php echo $form->textFieldRow($model,'archive_term',array('class'=>'unit-fluid')); ?>
			
				<?php echo $form->textFieldRow($model,'archive_space',array('class'=>'unit-fluid')); ?>

				<?php echo $form->textFieldRow($model,'archive_cost_per_day',array('class'=>'unit-fluid')); ?>

				<?php echo $form->textFieldRow($model,'archive_cost_per_mb',array('class'=>'unit-fluid')); ?>
				
				<?php echo $form->textFieldRow($model,'archive_download',array('class'=>'unit-fluid')); ?>
			
				<?php echo $form->textFieldRow($model,'number_cameras',array('class'=>'unit-fluid')); ?>
			
				<?php echo $form->textFieldRow($model,'another_users_access',array('class'=>'unit-fluid')); ?>
			
				<?php echo $form->textFieldRow($model,'cost_camera_per_month',array('class'=>'unit-fluid')); ?>
				
			</div>
			<div class="clear"></div>
		</div>
	</div><!-- end main -->
	<div class="side span4">
		<div class="form-actions" style="margin-top: 0;">
	
			<?php echo $form->toggleButtonRow($model,'archive',array('class'=>'span5')); ?>
			
			<?php echo $form->toggleButtonRow($model,'clode_archive',array('class'=>'span5')); ?>
		
			<?php echo $form->toggleButtonRow($model,'AVI_export',array('class'=>'span5')); ?>
		
			<?php echo $form->toggleButtonRow($model,'online_broadcasting',array('class'=>'span5')); ?>
		
			<?php echo $form->toggleButtonRow($model,'extended_support',array('class'=>'span5')); ?>
		
			<?php echo $form->toggleButtonRow($model,'payment_settlement_account',array('class'=>'span5')); ?>
		
			<?php echo $form->toggleButtonRow($model,'free_period',array('class'=>'span5')); ?>
			
			<?php echo $form->toggleButtonRow($model,'safe_mode',array('class'=>'span5')); ?>
		
			<?php echo $form->toggleButtonRow($model,'local_archive',array('class'=>'span5')); ?>
		
			<?php echo $form->toggleButtonRow($model,'embed_mode',array('class'=>'span5')); ?>
			
		</div>
	</div>
	<div class="clear"></div>
	<div class="span12">
		<div class="form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'),
			)); ?>
		</div>
		<p class="note"><?php echo Yii::t('main','Fields with ') ?>
		<span class="required">*</span><?php echo Yii::t('main',' are required.') ?></p>
	</div>
</div>
<?php $this->endWidget(); ?>
