<?php
$this->breadcrumbs=array(
	Yii::t('main','Tarifs')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Tarif'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Tarif'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Tarif'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Tarifs'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Tarif: "'); echo $model->name.'"'; ?></h1>
</div>
<?php $this->widget('bootstrap.widgets.TbAlert', array(
	        'block'=>true, 
	        'fade'=>true, 
	        'closeText'=>'&times;', 
	        'alerts'=>array( 
	            'warning'=>array('block'=>true, 'fade'=>true), 
	        ),
)); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'machine_name',
		'archive',
		'archive_term',
		'archive_space',
		'archive_cost_per_day',
		'archive_cost_per_mb',
		'archive_download',
		'number_cameras',
		'safe_mode',
		'local_archive',
		'embed_mode',
		'clode_archive',
		'AVI_export',
		'another_users_access',
		'online_broadcasting',
		'extended_support',
		'payment_settlement_account',
		'free_period',
		'cost_camera_per_month'
	),
)); ?>
