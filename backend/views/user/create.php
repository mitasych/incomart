<?php
$this->breadcrumbs=array(
	Yii::t('main','Users')=>array('index'),
	Yii::t('main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Manage Users'),'url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Create New User');?>
	</h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'roles'=>$roles)); ?>