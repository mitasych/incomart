<?php
$this->breadcrumbs=array(
	Yii::t('main','Users')=>array('index'),
	$model->username=>array('view','id'=>$model->username),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create New User'),'url'=>array('create')),
	array('label'=>Yii::t('main','View User'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Users'),'url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Update User: "'); echo $model->username.'"'; ?>
	</h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model, 'roles'=>$roles)); ?>