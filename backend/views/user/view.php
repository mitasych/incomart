<?php
$this->breadcrumbs=array(
	Yii::t('main','Users')=>array('index'),
	$model->username,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create New User'),'url'=>array('create')),
	array('label'=>Yii::t('main','Edit User'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete User'),'url'=>'#','htmlOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Users'),'url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','View User: "'); echo $model->username.'"'; ?>
	</h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		//'password',
		'email',
		//'activkey',
		'create_time',
		'lastvisit_at',
		'superuser',
		'status',
		array(
			//'label'=>Yii::t('main', 'Roles'),	
			'name'=>'role',
			'value'=>$model->strRoles
		),
		'login_attempts',
	),
)); ?>
