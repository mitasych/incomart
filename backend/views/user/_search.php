<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'htmlOptions'=>array('class'=>'well custom_search'),
	'method'=>'get',
)); ?>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'username',array('class'=>'unit-fluid','maxlength'=>20)); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'email',array('class'=>'unit-fluid','maxlength'=>128)); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
		<?php //echo $form->textFieldRow($model,'activkey',array('class'=>'span5','maxlength'=>128)); ?>
		<?php echo $form->textFieldRow($model,'create_time',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'lastvisit_at',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'superuser',array('class'=>'unit-fluid')); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'status',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php // echo $form->textFieldRow($model,'role_id',array('class'=>'span5')); ?>
		<?php echo $form->textFieldRow($model,'login_attempts',array('class'=>'unit-fluid')); ?>
	</div>
</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>Yii::t('main','Search'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
