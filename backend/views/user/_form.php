<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="main span8">
		<div class="well">
			<div class="fleft span7">
				<?php echo $form->textFieldRow($model,'username',array('class'=>'unit-fluid',)); ?>
				
				<div id="wrap_pass_field" style="<?php echo  $model->isNewRecord ? '' : 'display: none'; ?>">
					<?php echo $form->passwordFieldRow($model,'password',array('class'=>'unit-fluid')); ?>
				</div>
				
				<?php if (!$model->isNewRecord): ?>
					<?php echo $form->checkboxRow($model,'password_reset'); ?>
				<?php endif; ?>
					
				<?php echo $form->textFieldRow($model,'email',array('class'=>'unit-fluid')); ?>
			
				<?php //echo $form->textFieldRow($model,'activkey',array('class'=>'span5','maxlength'=>128)); ?>
			
				<?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>
			
				<?php //echo $form->textFieldRow($model,'lastvisit_at',array('class'=>'span5')); ?>
			
				<?php echo $form->textFieldRow($model,'login_attempts',array('class'=>'unit-fluid')); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div><!-- end main -->
	<div class="side span4">
		<div class="form-actions" style="margin-top: 0;">
			<?php echo $form->toggleButtonRow($model,'superuser'); ?>
		
			<?php echo $form->dropDownListRow($model,'status',$model->statuses,array('class'=>'unit-fluid')); ?>
			
			<div id="roleAssignements">
				<?php echo CHtml::label(Yii::t('labels', 'Roles'), 'roles[]', array('required'=>true))?>
				<?php echo CHtml::dropDownList('roles[]', $model->roles, $roles, array('multiple' => 'multiple','class'=>'unit-fluid'))?>
			</div>	
		</div>
	</div>
	<div class="clear"></div>
	<div class="span12">
		<div class="form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? Yii::t('main','Create') : Yii::t('main','Save'),
			)); ?>
		</div>
		<p class="note"><?php echo Yii::t('main','Fields with ') ?>
		<span class="required">*</span><?php echo Yii::t('main',' are required.') ?></p>
	</div>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		if($('input#User_password_reset').attr('checked') == 'checked'){
			$('#wrap_pass_field').show();
		}
	});
	$('input#User_password_reset').click(function(){
		$('#wrap_pass_field').toggle();
	});
</script>
