<?php $this->pageTitle=Yii::app()->name; ?>

<div class="row">
	<div class="span12 main_header">
		<h1 style="display: inline;" class="fleft"><?php echo Yii::t('main','Site management');?> </h1>
			<div class="fright" style="margin: 10px 15px 10px 0;">
				<h4 style="font-weight: bold; text-align: right;" class="fleft">
					<?php echo Yii::t('main', "User").": ".Yii::app()->user->name?>
				</h4>
				<?php echo CHtml::image('/image/uploaded/avatarthumb/'.$user_profile->photo, "", array("width"=>32,"heigth"=>32,"class"=>"fright","style"=>"margin-left:20px;")); ?>
			</div>
		<div class="clear"></div>
		
		<?php 
			if(Yii::app()->user->isGuest) {
				Yii::app()->user->setFlash('info', '<a href="/admin/site/login">Login</a>');
				$this->widget('bootstrap.widgets.TbAlert');
			}
		?>
	</div>
</div>
<div class="row">
	<div class="span3">
		<!-- <div class="widget-main"> -->
			<?php $this->widget('bootstrap.widgets.TbMenu', array(
			    'type'=>'list',
					'htmlOptions'=>array('class'=>'well_custom'),
			    'items'=>array(
					array('label'=>Yii::t('main','ADMINISTRATION PANEL')),
			        array('label'=>Yii::t('main','Users'), 'icon'=>'user', 'url'=>'/admin/user', 'visible'=>!Yii::app()->user->isGuest),
			        array('label'=>Yii::t('main','View Profile').": $user->name", 'icon'=>'user', 'url'=>'/admin/user/view/'.Yii::app()->user->id),
			        array('label'=>Yii::t('main','Edit Profile').": $user->name", 'icon'=>'user', 'url'=>'/admin/user/update/'.Yii::app()->user->id),
					array('label' =>Yii::t('main','Logout (') . $user->name . ')', 'icon'=>'user white', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
					array('label' => Yii::t('main','Rights'), 'visible' => Yii::app()->user->superuser),
					array('label' => Yii::t('main','Permissions'), 'icon'=>'list', 'url' => array('/authItem/permissions'), 'visible' => Yii::app()->user->superuser),
					array('label' => Yii::t('main','Generate Rights'), 'icon'=>'align-left', 'url' => array('/authItem/generate'), 'visible' => Yii::app()->user->superuser),
					'---',
					array('label'=>'Help', 'icon'=>'flag', 'url'=>'#'),
	// 				array('label'=>Yii::t('main','Settings'), 'icon'=>'cog', 'url'=>'#'),		    
			    ),
			)); ?>
		<!-- </div> -->
	</div>
	
	<div class="span9">
		<div class="row">
			<div class="span3">
				<div class="separator2"></div>
				<div class="widget">
					<?php $this->widget('bootstrap.widgets.TbMenu', array(
					    'type'=>'list',
						'htmlOptions'=>array('class'=>'well'),
					    'items'=>array(
					    ),
					)); ?>
				</div>
			</div>
			
			<div class="span3">
				<div class="separator2"></div>
				<div class="widget">
					<?php $this->widget('bootstrap.widgets.TbMenu', array(
					    'type'=>'list',
						'htmlOptions'=>array('class'=>'well'),
					    'items'=>array(
					    ),
					)); ?>
				</div>
			</div>
			
			<div class="span3">	
				<div class="separator2"></div>
				<div class="widget">
					<?php $this->widget('bootstrap.widgets.TbMenu', array(
					    'type'=>'list',
						'htmlOptions'=>array('class'=>'well'),
					    'items'=>array(
							array('label'=>"USER PROFILE: $user_profile->first_name $user_profile->last_name"),
							'---',
							array('label'=>"$user_profile->first_name $user_profile->last_name", 'icon'=>'', 'url'=>"/user/view/$user->id"),
							array('label'=>"City: $user_profile->city", 'icon'=>'', 'url'=>"/user/view/$user->id"),
							array('label'=>"ICQ: $user_profile->icq", 'icon'=>'', 'url'=>"/user/view/$user->id"),
							array('label'=>"Skype: $user_profile->skype", 'icon'=>'', 'url'=>"/user/view/$user->id"),
							'---',
							array('label'=>"USERS: $users_count"),
							array('label' => Yii::t('main','Users'), 'icon'=>'user', 'url' => array('/user')),
							array('label' => Yii::t('main','Role'), 'icon'=>'eye-close', 'url' => array('/role')),
							'---',
							array('label'=>'', 'icon'=>'', 'url'=>'#'),
					    ),
					)); ?>
				</div>
			</div>
		</div>
		<div class="separator1"></div>
		<div class="row">
			<div class="span6">
				<div class="separator2"></div>
				<div class="widget">
					<?php $this->widget('bootstrap.widgets.TbMenu', array(
						    'type'=>'list',
							'htmlOptions'=>array('class'=>'well'),
						    'items'=>array(),
					)); ?>
			</div>
			</div>
			<div class="span3">
				<div class="separator2"></div>
				<div class="widget">
					<?php $this->widget('bootstrap.widgets.TbMenu', array(
					    'type'=>'list',
						'htmlOptions'=>array('class'=>'well'),
							/* 'htmlOptions'=>array('class'=>'span3'), */
					    'items'=>array(
					        array('label'=>'BANNERS'),
							array('label'=>'Manage Banners', 'icon'=>'icon-retweet', 'url'=>'/admin/banner/index/'),
							array('label'=>'Manage Banners Sizes', 'icon'=>'icon-resize-full', 'url'=>'/admin/bannerSizes/index/'),
					    ),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</div>
	
	

