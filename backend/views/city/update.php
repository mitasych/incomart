<?php
$this->breadcrumbs=array(
	Yii::t('main','Cities')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create City'),'url'=>array('create')),
	array('label'=>Yii::t('main','View City'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Cities'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update City: "'); echo $model->name.'"'; ?></h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>