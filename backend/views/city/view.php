<?php
$this->breadcrumbs=array(
	Yii::t('main','Cities')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create City'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update City'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete City'),'url'=>'#','htmlOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Cities'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View City: "'); echo $model->name.'"'; ?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'machine_name',
	),
)); ?>
