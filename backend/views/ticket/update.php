<?php
$this->breadcrumbs=array(
	Yii::t('main','Tickets')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
		
		/* Yii::t('main','Users')=>array('index'),
		$model->username=>array('view','id'=>$model->username),
		Yii::t('main','Update'), */
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Ticket'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Ticket'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Tickets'),'url'=>array('index')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Update Ticket: "'); echo $model->id.'"'; ?>
	</h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>