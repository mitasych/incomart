<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'htmlOptions'=>array('class'=>'well custom_search'),
	'method'=>'get',
)); ?>

<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'theme',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'create_time',array('class'=>'unit-fluid')); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'status',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'close_time',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'priority',array('class'=>'unit-fluid')); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'user_id',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'camera_id',array('class'=>'unit-fluid')); ?>
	</div>
	<div class="fleft span4">
		<?php echo $form->textFieldRow($model,'account_id',array('class'=>'unit-fluid')); ?>
	</div>
</div>
<div class="row-fluid">
	<div class="fleft span4">
		<?php echo $form->textAreaRow($model,'description',array('class'=>'unit-fluid', 'rows'=>'6', 'cols'=>'50')); ?>
	</div>
</div>
	<?php //echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>36)); ?>

	<?php //echo $form->textFieldRow($model,'theme',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'status',array('class'=>'span5','maxlength'=>5)); ?>

	<?php //echo $form->textFieldRow($model,'close_time',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'priority',array('class'=>'span5','maxlength'=>6)); ?>

	<?php //echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'camera_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'account_id',array('class'=>'span5')); ?>
	
	<?php //echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>Yii::t('main','Search'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
