<?php
$this->breadcrumbs=array(
	Yii::t('main','Tickets')=>array('index'),
	Yii::t('main','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Ticket'),'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ticket-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Manage Tickets');?>
	</h1>
</div>

<?php echo CHtml::link(Yii::t('main','Advanced Search'),'#',array('class'=>'search-button','title'=>Yii::t("main", "You may optionally enter a comparison operator (<, <=, >, >=, <> or =) at the beginning of each of your search values to specify how the comparison should be done."))); ?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'ticket-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'theme',
		'description',
		'create_time',
		'status',
		'close_time',
		/*
		'priority',
		'user_id',
		'camera_id',
		'account_id',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
<script type="text/javascript"> 
	$(/*'.test, .filter-container input,*/ '.search-button').tooltip('hide','left');
	/*$('input.select-on-check, tr, a').live('click',function(){
		setTimeout(function(){
			if($('input.select-on-check:checked').length>0){
				$('#bulk_update_group').show();
			}
			else{
				$('#bulk_update_group').hide();
			}
		}, 600)
	});
	$('input.select-on-check-all').click(function(){
		$('#bulk_update_group').hide();
		if($(this).attr('checked')=='checked'){
			$('#bulk_update_group').show();
		}
	});*/
</script> 
