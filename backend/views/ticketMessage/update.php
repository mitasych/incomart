<?php
$this->breadcrumbs=array(
	'Ticket Messages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TicketMessage','url'=>array('index')),
	array('label'=>'Create TicketMessage','url'=>array('create')),
	array('label'=>'View TicketMessage','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TicketMessage','url'=>array('admin')),
);
?>

<h1>Update TicketMessage <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>