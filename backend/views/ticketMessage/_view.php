<div class="view">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('/ticketMessage/view','idTicketMessage'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message')); ?>:</b>
	<?php echo CHtml::encode($data->message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->create_time); ?>
	<br />
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'link',
			'url'=>'/admin/ticketMessage/create/ticket_id/'.$data->ticket_id.'/ask_id/'.$data->id,
			'type'=>'success',
			'htmlOptions' => array('style'=>'margin: 0 0 0 0px;'),
			//'htmlOptions' => array('style'=>'float:left; margin: 0 0px 0px 70px;'),
			'label'=>'Ответить'/* Yii::t('main','Search') */,
			'size'=>'mini'
		)); 
	?>
	<hr>
	<br />
	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ticket_id')); ?>:</b>
	<?php echo CHtml::encode($data->ticket_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_in_ticket')); ?>:</b>
	<?php echo CHtml::encode($data->order_in_ticket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('previous_id')); ?>:</b>
	<?php echo CHtml::encode($data->previous_id); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ask_id')); ?>:</b>
	<?php echo CHtml::encode($data->ask_id); ?>
	<br />

	*/ ?>

</div>