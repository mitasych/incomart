<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'ticket-message-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model,'ticket_id',array('class'=>'span5','maxlength'=>36)); ?>

	<?php if(!empty($model->ask_id)):?>
		<?php echo $form->hiddenField($model,'ask_id',array('class'=>'span5','maxlength'=>36)); ?>
	<?php endif; ?>
	<?php // echo $form->textFieldRow($model,'order_in_ticket',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'previous_id',array('class'=>'span5','maxlength'=>36)); ?>

	<?php // echo $form->textFieldRow($model,'type',array('class'=>'span5','maxlength'=>8)); ?>

	<?php // echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'message',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
