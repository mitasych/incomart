<?php
$this->breadcrumbs=array(
	'Ticket Messages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TicketMessage','url'=>array('index')),
	array('label'=>'Create TicketMessage','url'=>array('create')),
	array('label'=>'Update TicketMessage','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete TicketMessage','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TicketMessage','url'=>array('admin')),
);
?>

<h1>View TicketMessage #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'ticket_id',
		'order_in_ticket',
		'previous_id',
		'type',
		'user_id',
		'create_time',
		'message',
		'ask_id',
	),
)); ?>
