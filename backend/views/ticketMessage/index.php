<?php
// $this->breadcrumbs=array(
// 	'Ticket Messages',
// );

// $this->menu=array(
// 	array('label'=>'Create TicketMessage','url'=>array('create')),
// 	array('label'=>'Manage TicketMessage','url'=>array('admin')),
// );
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Ticket Messages'); ?>
	</h1>
</div>
<?php $dataProvider->sort->defaultOrder='order_in_ticket';?>
<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'enableSorting'=>true,
	'sortableAttributes'=>array(
			'order_in_ticket'=>'По порядку',
	),
	'itemView'=>'/ticketMessage/_view',
)); ?>
