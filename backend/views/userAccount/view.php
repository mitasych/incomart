<?php
$this->breadcrumbs=array(
	Yii::t('main','User Accounts')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create User Account'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update User Account'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete User Account'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Users Accounts'),'url'=>array('index')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','View User Account: "'); echo $model->id.'"'; ?>
	</h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'create_time',
		'currency_id',
		'balance',
	),
)); ?>
