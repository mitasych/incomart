<?php
$this->breadcrumbs=array(
	Yii::t('main','User Accounts')=>array('index'),
	Yii::t('main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Manage Users Accounts'),'url'=>array('index')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Create User Account');?>
	</h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>