<?php
$this->breadcrumbs=array(
	Yii::t('main','User Accounts')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create User Account'),'url'=>array('create')),
	array('label'=>Yii::t('main','View User Account'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Users Accounts'),'url'=>array('index')),
);
?>

<div class="page-header">
	<h1>
	<?php echo Yii::t('main','Update User Account: "'); echo $model->id.'"'; ?>
	</h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>