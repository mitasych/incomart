<?php
$this->breadcrumbs=array(
	Yii::t('main','Banner')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Banner'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Banner'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Banner'),'url'=>'#','htmlOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Banners'),'url'=>array('index')),
);
?>

<br/>
<div class="page-header">
<h1><?php echo Yii::t('main','View Banner: "'); echo $model->name.'"'; ?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
// 		'id',
		'name',
// 		'size_id',
		array(
		'name'=>'size_id',
		'type'=>'raw',
		'value'=>$model->size->name,
		),
// 		'content',
		array(
		'name'=>'content',
		'type'=>'raw',
		'value'=>$model->content,
		),
		'create_time',
		'start_time',
		'end_time',
// 		'create_user_id',
		array(
		'name'=>'create_user_id',
		'type'=>'raw',
		'value'=>CHtml::link($model->createUser->username,array('/user/'.$model->create_user_id)),
),
	),
)); ?>
