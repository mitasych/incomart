<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'banner-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="main span8">
		<div class="well">
			<div class="fleft span7">
				<?php echo $form->textFieldRow($model,'name',array('class'=>'unit-fluid','maxlength'=>45)); ?>
			
				<?php echo $form->textAreaRow($model,'content',array('rows'=>6, 'cols'=>50, 'class'=>'unit-fluid','maxlength'=>1000)); ?>
			
				<?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>
				
				<?php //echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div><!-- end main -->
	<div class="side span4">
		<div class="form-actions" style="margin-top: 0;">
			
			<?php echo $form->dropDownListRow($model, 'size_id', BannerSizes::listItems(),array('class'=>'unit-fluid')); ?>	
			
			<?php //echo $form->textFieldRow($model,'start_time',array('class'=>'span5')); ?>
			<?php echo $form->labelEx($model,'start_time'); ?>
			<?php Yii::import('common.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
		   		$this->widget('CJuiDateTimePicker',array(
		       					'model'=>$model, //Model object
		        				'attribute'=>'start_time', //attribute name
		            		    'mode'=>'datetime', //use "time","date" or "datetime" (default)
								'htmlOptions'=>array(
										'value'=>$model->start_time = $model->isNewRecord ?
												date('Y-m-d H:i:s') :
												$model->start_time,
										'class'=>'unit-fluid',
								),
		      					'options'=>array(
											"dateFormat"=>'yy-mm-dd',
											"timeFormat"=>'hh:mm:ss',
											"timeText"=>Yii::t('main', "Time"), //Время
											"hourText"=>Yii::t('main', "Hours"), //Часы
											"minuteText"=>Yii::t('main', "Minutes"), //Минуты
											), // jquery plugin options
								'language' => 'ru',
								) // jquery plugin options
							);
			?>
			
		
			<?php //echo $form->textFieldRow($model,'end_time',array('class'=>'span5')); ?>
			<?php echo $form->labelEx($model,'end_time'); ?>
			<?php
		   		$this->widget('CJuiDateTimePicker',array(
		       					'model'=>$model, //Model object
		        				'attribute'=>'end_time', //attribute name
		            		    'mode'=>'datetime', //use "time","date" or "datetime" (default)
								'htmlOptions'=>array(
								'value'=>$model->isNewRecord ?
										$model->end_time = date('Y-m-d H:i:s', mktime(date("H"),date("i"),date("s"),date("m"),date("d")+1,date("Y")))
										: $model->end_time,
										'class'=>'unit-fluid',
								),
		      					'options'=>array(
											"dateFormat"=>'yy-mm-dd',
											"timeFormat"=>'hh:mm:ss',
											"timeText"=>Yii::t('main', "Time"), //Время
											"hourText"=>Yii::t('main', "Hours"), //Часы
											"minuteText"=>Yii::t('main', "Minutes"), //Минуты
											), // jquery plugin options
								'language' => 'ru',
								) // jquery plugin options
							);
			?>
			<?php //echo $form->textFieldRow($model,'size_id',array('class'=>'span5')); ?>
	
		</div>
	</div>
	<div class="clear"></div>
	<div class="span12">
		<div class="form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'),
			)); ?>
		</div>
		<p class="note"><?php echo Yii::t('main','Fields with ') ?>
		<span class="required">*</span><?php echo Yii::t('main',' are required.') ?></p>
	</div>
</div>
<?php $this->endWidget(); ?>
