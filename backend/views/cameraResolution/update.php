<?php
$this->breadcrumbs=array(
	Yii::t('main','Camera Resolutions')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Camera Resolution'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Camera Resolution'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Camera Resolutions'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update Camera Resolution: "'); echo $model->name.'"'; ?></h1>
</div>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>