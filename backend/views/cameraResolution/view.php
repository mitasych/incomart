<?php
$this->breadcrumbs=array(
	Yii::t('main','Camera Resolutions')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Camera Resolution'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Camera Resolution'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Camera Resolution'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Camera Resolutions'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Camera Resolution: "'); echo $model->name.'"'; ?></h1>
</div>

<?php 
	$ownAttribs = array(
		'id',
		'name',
		'height_dimension',
		'width_dimension',
	);
	$attribs = array_merge($ownAttribs, $model->relTarifs); 
?>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>$attribs
)); ?>
