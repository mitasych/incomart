<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'camera-resolution-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="main span8">
		<div class="well">
			<div class="fleft span7">
				<?php echo $form->textFieldRow($model,'name',array('class'=>'unit-fluid','maxlength'=>100)); ?>

				<?php echo $form->textFieldRow($model,'height_dimension',array('class'=>'unit-fluid')); ?>
			
				<?php echo $form->textFieldRow($model,'width_dimension',array('class'=>'unit-fluid')); ?>
			</div>
			<div class="clear"></div>
				<?php foreach($model->tarifPrice as $key => $tarVal): ?>
					<?php echo CHtml::label($tarVal['name'], 'tar_'.$tarVal['id'])?>
					<?php echo $form->textField($model, 'tarifPrice['.$tarVal['id'].']', array('id'=>'tar_'.$tarVal['id'], 'value'=>$tarVal['val']));?>
									
				<?php endforeach;?>
		</div>
	</div><!-- end main -->
	<div class="side span4 side_none">
		<div class="form-actions" style="margin-top: 0;">
		</div>
	</div>
	<div class="clear"></div>
	<div class="span12">
		<div class="form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'),
			)); ?>
		</div>
		<p class="note"><?php echo Yii::t('main','Fields with ') ?>
		<span class="required">*</span><?php echo Yii::t('main',' are required.') ?></p>
	</div>
</div>
<?php $this->endWidget(); ?>
