<?php
$this->breadcrumbs=array(
	Yii::t('main','Camera Resolutions')=>array('index'),
	Yii::t('main','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Manage Camera Resolutions'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Create Camera Resolution');?></h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>