<?php
$this->breadcrumbs=array(
	Yii::t('main','Banner Sizes')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Banner Sizes'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Banner Sizes'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Banner Sizes'),'url'=>array('index')),
);
?>

<br/>
<div class="page-header">
<h1><?php echo Yii::t('main','Update Banner Sizes: "'); echo $model->name.'"'; ?></h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>