<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'banner-sizes-form',
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="main span8">
		<div class="well">
			<div class="fleft span7">
				<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>100)); ?>
			
				<?php echo $form->textFieldRow($model,'width',array('class'=>'span5')); ?>
			
				<?php echo $form->textFieldRow($model,'height',array('class'=>'span5')); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div><!-- end main -->
		<div class="side span4 side_none">
		<div class="form-actions" style="margin-top: 0;">
		</div>
	</div>
	<div class="clear"></div>
	<div class="span12">
		<div class="form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'),
			)); ?>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
