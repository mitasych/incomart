<?php
$this->breadcrumbs=array(
	Yii::t('main','Banner Sizes')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Banner Sizes'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Banner Sizes'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Banner Sizes'),'url'=>'#','htmlOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Banner Sizes'),'url'=>array('index')),
);
?>

<br/>
<div class="page-header">
<h1><?php echo Yii::t('main','View Banner Sizes: "'); echo $model->name.'"'; ?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'width',
		'height',
	),
)); ?>
