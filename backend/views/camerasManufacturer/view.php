<?php
$this->breadcrumbs=array(
	Yii::t('main','Cameras Manufacturers')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Cameras Manufacturer'),'url'=>array('create')),
	array('label'=>Yii::t('main','Update Cameras Manufacturer'),'url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('main','Delete Cameras Manufacturer'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('main','Manage Cameras Manufacturers'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','View Cameras Manufacturer: "'); echo $model->name.'"'; ?></h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
