<?php
$this->breadcrumbs=array(
	Yii::t('main','Cameras Manufacturers')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('main','Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main','Create Cameras Manufacturer'),'url'=>array('create')),
	array('label'=>Yii::t('main','View Cameras Manufacturer'),'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('main','Manage Cameras Manufacturers'),'url'=>array('index')),
);
?>

<div class="page-header">
<h1><?php echo Yii::t('main','Update Cameras Manufacturer: "'); echo $model->name.'"'; ?></h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>