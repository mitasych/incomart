<?php
/**
 * 
 * @class CameraResolutionForm 
 *
 * @author Mitasov Aleksandr (mitasych)
 * @copyright Copyright (c) NikoSmart (http://nikosmart.com) 2013
 *
 */
class CameraResolutionForm extends CameraResolution
{

	public function rules()
	{
		return array_merge(parent::rules(), array(
					array('tarifPrice', 'type', 'type'=>'array'),
					array('tarifPrice', 'itemFloat'),
					array('tarifPrice', 'safe'),
				));
	}
	
	public function itemFloat($attribute, $params)
	{
		foreach($this->$attribute as $k => $item){
			if(!preg_match('/^[-+]?([0-9]*\.)?[0-9]+([eE][-+]?[0-9]+)?$/',trim($item['val']))){
				$this->addError('tarif_price','Значение в полях определения цен могут быть только формата <i>1.00</i>');
					return;
			}
		}
		return true;
	}
	
	public function afterSave()
	{
		foreach ($this->tarifPrice as $tarif => $val){
			if (!is_null(Tarif::model()->findByPk($val['id']))) {
					
				$relTarFr = CameraResolutionRelTarif::model()->findByPk(array('tarif_id'=>$val['id'], 'resolution_id'=>$this->id));
					
				if (is_null($relTarFr)) {
					$relTarFr = new CameraResolutionRelTarif;
					$relTarFr->tarif_id = $val['id'];
					$relTarFr->resolution_id = $this->id;
				}
				$relTarFr->cost_per_day = number_format(round($val['val'], 2), 2);
	
				$relTarFr->save();
			}
		}
		return parent::afterSave();
	}
	
	public function getTarifPrice()
	{
		return $this->isRelTarifs;
	}
	
	public function setTarifPrice($array)
	{
		$temp = $this->tarifPrice;
		foreach($temp as $key => &$tarif){
			$tarif['val'] = $array[$tarif['id']];
		}
		return $this->tarifPrice = $temp;
	}
	
}
